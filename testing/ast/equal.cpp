#include "types.hpp"
#include "exprs.hpp"

#include <beaker/sema/translator.hpp>

#include <cassert>
#include <iostream>


using namespace beaker;
using void_type = lang_void::void_type;
using bool_type = lang_bool::bool_type;
using nat_type = lang_int::nat_type;
using int_type = lang_int::int_type;
using ref_type = lang_store::ref_type;
using fn_type = lang_fn::fn_type;
using array_type = lang_array::array_type;

using int_expr = lang_int::int_expr;
using const_expr = lang_array::const_expr;

int main()
{
  symbol_table syms;
  context cxt(syms);
  translator sema(cxt);

  type* v = cxt.get_void_type();
  assert(equal(v, v));
  
  type* b = cxt.get_bool_type();
  assert(equal(b, b));

  type* n = cxt.get_nat_type();
  assert(equal(n, n));

  type* z = cxt.get_int_type();
  assert(equal(z, z));

  ref_type r1(b); // bool&
  ref_type r2(b); // bool&
  ref_type r3(n); // nat&
  assert(equal(&r1, &r2));
  assert(!equal(&r1, &r3));
  std::cout << pretty(&r1) << '\n';
  std::cout << pretty(&r3) << '\n';

  fn_type f1(cxt, {}, b); // () -> bool
  fn_type f2(cxt, {}, b); // () -> bool
  fn_type f3(cxt, {b}, b); // (bool) -> bool
  fn_type f4(cxt, {z, z}, b); // (int, int) -> bool
  assert(equal(&f1, &f2));
  assert(!equal(&f1, &f3));
  assert(!equal(&f3, &f4));
  std::cout << pretty(&f1) << '\n';
  std::cout << pretty(&f3) << '\n';
  std::cout << pretty(&f4) << '\n';


  expr* int_4 = sema.on_constant_expression(new int_expr(z, 4));
  expr* int_5 = sema.on_constant_expression(new int_expr(z, 5));

  array_type a1(cxt, z, {int_4});
  array_type a2(cxt, z, {int_4});
  array_type a3(cxt, z, {int_5});
  array_type a4(cxt, b, {int_4});
  assert(equal(&a1, &a2));
  assert(!equal(&a1, &a3));
  assert(!equal(&a1, &a4));
  std::cout << pretty(&a1) << '\n';
  std::cout << pretty(&a2) << '\n';
  std::cout << pretty(&a3) << '\n';
  std::cout << pretty(&a4) << '\n';
}
