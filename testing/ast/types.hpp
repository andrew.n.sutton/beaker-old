#pragma once

#include <beaker/ast/context.hpp>
#include <beaker/ast/print.hpp>

#include <beaker/ast/lang.void/types_void.hpp>
#include <beaker/ast/lang.bool/types_bool.hpp>
#include <beaker/ast/lang.int/types_int.hpp>
#include <beaker/ast/lang.store/types_store.hpp>
#include <beaker/ast/lang.fn/types_fn.hpp>
#include <beaker/ast/lang.array/types_array.hpp>

