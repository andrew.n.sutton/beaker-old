#include <beaker/util/input.hpp>
#include <beaker/lex/lexer.hpp>
#include <beaker/syn/parser.hpp>
#include <beaker/ast/context.hpp>
#include <beaker/ast/decls.hpp>
#include <beaker/ast/evaluate.hpp>
#include <beaker/ast/print.hpp>

#include <beaker/ast/lang.tu/decls_tu.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>
#include <beaker/ast/lang.fn/exprs_fn.hpp>

#include <fstream>
#include <iterator>
#include <iostream>


using namespace beaker;

int 
main(int argc, char* argv[])
{
  /// Defines the language's keywords.
  keyword_table keywords;

  /// Stores the language's symbols.
  symbol_table symbols;

  // Stores language facilities.
  context cxt(symbols);

  // Stores the inputs for translation.
  input_list inputs;

  if (argc < 2) {
    std::cerr << "error: no input files given\n";
    return 1;
  }

  // Read the input into a string.
  std::ifstream ifs(argv[1]);
  std::string input{std::istreambuf_iterator<char>(ifs),
                    std::istreambuf_iterator<char>()};

  parser p(keywords, symbols, cxt, input);
  decl* ast = p.translation();

  auto* tu = static_cast<lang_tu::tu_decl*>(ast);

  // Find the entry point "main".
  //
  // FIXME: This should really be in beaker-interpret, which doesn't
  // actually exist.
  lang_fn::fn_decl* entry = nullptr;
  for (decl* d  : tu->get_declarations()) {
    if (named_decl* nd = dynamic_cast<named_decl*>(d)) {
      if (nd->get_name() == symbols.insert("main")) {
        entry = dynamic_cast<lang_fn::fn_decl*>(nd);
        break;
      }
    }
  }

  // std::cout << "--- PROGRAM ----\n";
  // std::cout << verbose(tu);
  // std::cout << "----------------\n\n\n";

  // Build a call to the entry function and evaluate it.
  if (entry) {
    // FIXME: Assert that entry has the right type (()->int). Or better yet,
    // pass actual command line arguments!
    
    translator& sema = p.get_translator();
    expr* ref = sema.on_id_expression(entry);
    expr* call = sema.on_call_expression(ref, {});

    value v = evaluate(cxt, call);
    std::cout << v << '\n';

    // FIXME: Return the integer value of v from main.
  }
  else {
    std::cerr << "program does not define 'main'\n";
  }

  return 0;
}
