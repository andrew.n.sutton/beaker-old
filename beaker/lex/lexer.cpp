#include "lexer.hpp"

#include <cassert>
#include <cctype>
#include <iostream>
#include <sstream>


namespace beaker {

/// Returns true if c is a (decimal) digit.
static inline bool
is_digit(char c)
{
  return std::isdigit(c);
}

/// Returns true if c is a letter or underscore.
static inline bool
is_letter(char c)
{
  return std::isalpha(c) || c == '_';
}

/// Returns true if c is a letter or digit.
static inline bool
is_letter_or_digit(char c)
{
  return is_letter(c) || is_digit(c);
}


lexer::lexer(const keyword_table& kt, symbol_table& st, const char* f, const char* l)
  : kws(&kt), syms(&st), first(f), limit(l)
{ }

lexer::lexer(const keyword_table& kt, symbol_table& st, const std::string& str)
  : lexer(kt, st, &str[0], &str[0] + str.size())
{ }

lexer::~lexer()
{
  for (token* tok : toks)
    delete tok;
}

bool
lexer::eof() const
{
  return first == limit;
}

char
lexer::lookahead() const
{
  return eof() ? 0 : *first;
}

char
lexer::lookahead(int n) const
{
  if (limit - first <= n)
    return 0;
  else
    return *(first + n);
}

char
lexer::consume()
{
  assert(!eof());
  char c = *first++;
  buf.push_back(c);
  return c;
}

void
lexer::ignore()
{
  ++first;
}

char
lexer::match_after(char c)
{
  if (lookahead(1) == c)
    return consume();
  else
    return 0;
}

token*
lexer::lex()
{
  // Quick exit if we're the end of file.
  if (eof())
    return nullptr;

  buf.clear(); // Reset the character buffer.
  while (!eof()) {
    space(); // Consume preceding whitespace.
    switch (lookahead()) {
      case 0:
        // Consuming trailing whitespace at the end of a file means we can
        // actually get the null character here. No problem, return the null
        // token.
        return nullptr;
      case '{':
        return puncop1(lbrace_tok);
      case '}':
        return puncop1(rbrace_tok);
      case '(':
        return puncop1(lparen_tok);
      case ')':
        return puncop1(rparen_tok);
      case '[':
        return puncop1(lbrack_tok);
      case ']':
        return puncop1(rbrack_tok);
      case ',':
        return puncop1(comma_tok);
      case ';':
        return puncop1(semicolon_tok);
      case ':':
        return puncop1(colon_tok);
      case '?':
        return puncop1(question_tok);
      case '+':
        return puncop1(plus_tok);
      case '-':
        if (match_after('>'))
          return puncop1(arrow_tok);
        else
          return puncop1(minus_tok);
      case '*':
        return puncop1(star_tok);
      case '/':
        // Remove comments token stream.
        if (lookahead(1) == '/') {
          comment();
          continue;
        }
        return puncop1(slash_tok);
      case '%':
        return puncop1(percent_tok);
      case '&':
        if (match_after('&'))
          return puncop1(amp_amp_tok);
        else
          return puncop1(amp_tok);
      case '|':
        if (match_after('|'))
          return puncop1(bar_bar_tok);
        else
          return puncop1(bar_tok);
      case '^':
        return puncop1(caret_tok);
      case '!':
        if (match_after('='))
          return puncop1(bang_eq_tok);
        else
          return puncop1(bang_tok);
      case '=':
        if (match_after('='))
          return puncop1(eq_eq_tok);
        else
          return puncop1(eq_tok);
      case '<':
        if (match_after('='))
          return puncop1(le_tok);
        else
          return puncop1(lt_tok);
      case '>':
        if (match_after('='))
          return puncop1(ge_tok);
        else
          return puncop1(gt_tok);
      case '\'':
        return character();
      case '"':
        return string();
      default:
        if (is_digit(lookahead()))
          return integer();
        if (is_letter(lookahead()))
          return word();
        else {
          std::stringstream ss;
          ss << "invalid character '" << (int)lookahead() << "'";
          throw std::runtime_error(ss.str());
        }
    }
  }
  return nullptr;
}


void 
lexer::space()
{
  while (!eof() && std::isspace(lookahead()))
    ignore();
}

/// \todo Buffer comments for future use.
void
lexer::comment()
{
  while (!eof() && lookahead() != '\n')
    ignore();
}

bool
lexer::escape_character(char c)
{
  if (lookahead() == c || lookahead() == '\\' || lookahead() == '\n')
    return false;

  if (lookahead() == '\\')
    consume();
  if (std::isprint(lookahead())) {
    consume();
    return true;
  }
  return false;
}

token*
lexer::puncop0(token_kind k)
{
  token* tok = new token(k);
  toks.push_back(tok);
  return tok;
}

token*
lexer::puncop1(token_kind k)
{
  ignore();
  return puncop0(k);
}

token*
lexer::word()
{
  assert(is_letter(lookahead()));
  
  // Match id characters.
  consume();
  while (first != limit && is_letter_or_digit(lookahead()))
    consume();

  // Did we lex a keyword? If so, return a new token.
  auto iter = kws->find(buf);
  if (iter != kws->end()) {
    token *tok = new token(iter->second);
    toks.push_back(tok);
    return tok;
  }

  // Otherwise, this is an identifier.
  symbol* sym = syms->insert(buf);
  token* tok = new id_token(sym);
  toks.push_back(tok);
  return tok;
}

token*
lexer::integer()
{
  assert(is_digit(lookahead()));

  // Match digits.
  consume();
  while (first != limit && is_digit(lookahead()))
    consume();

  // Create the integer token.
  token* tok = new int_token(std::stoi(buf));
  toks.push_back(tok);
  return tok;
}

/// \todo Support escape chaacters.
token*
lexer::character()
{
  assert(lookahead() == '\'');
  ignore(); // '
  if (!escape_character('\''))
    throw std::runtime_error("invalid character literal");
  if (lookahead() != '\'')
    throw std::runtime_error("multi-character literal");
  ignore(); // '

  /// Returns a new character encoding.
  return new char_token(basic_enc, std::move(buf));
}

token*
lexer::string()
{
  assert(lookahead() == '"');
  ignore(); // "
  while (escape_character('"'))
    ;
  if (lookahead() != '"')
    throw std::runtime_error("unterminated string literal");
  ignore(); // "

  return new str_token(basic_enc, std::move(buf));
}

} // namespace beaker
