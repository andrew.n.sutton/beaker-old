#include "tokens.hpp"

#include <cassert>
#include <iostream>
#include <sstream>


namespace beaker {

keyword_table::keyword_table()
{
#define def_token(K, S)
#define def_keyword(K, S) insert({S, K ## _kw});
#include "tokens.def"
#undef def_keyword
#undef def_token

  insert({"bool", bool_kw});
  insert({"def", def_kw});
  insert({"false", false_kw});
  insert({"int", int_kw});
  insert({"nat", nat_kw});
  insert({"return", return_kw});
  insert({"true", true_kw});
  insert({"var", var_kw});
}

const char*
token::get_name(token_kind kind)
{
  switch (kind) {
  default: break;
#define def_token(K, S) case K ## _tok: return #K;
#define def_keyword(K, S) case K ## _kw: return #K;
#include "tokens.def"
#undef def_keyword
#undef def_token
  }
  assert(false && "invalid token kind");
}

const char*
token::str(token_kind kind)
{
  switch (kind) {
  default: break;
#define def_token(K, S) case K ## _tok: return S;
#define def_keyword(K, S) case K ## _kw: return S;
#include "tokens.def"
#undef def_keyword
#undef def_token
  }
  assert(false && "invalid token kind");
}

std::string
int_token::str() const
{
  std::stringstream ss;
  ss << value;
  return ss.str();
}

std::ostream& 
operator<<(std::ostream& os, const token& tok)
{
  os << '<' << tok.get_name();
  if (const int_token* num = dynamic_cast<const int_token*>(&tok))
    os << ',' << num->value;
  else if (const id_token* id = dynamic_cast<const id_token*>(&tok))
    os << ',' << id->str();
  os << '>';
  return os;
}

std::ostream& operator<<(std::ostream& os, const token* ptr)
{
  if (!ptr)
    return os << "<null>";
  else
    return os << *ptr;
}

} // namespace beaker
