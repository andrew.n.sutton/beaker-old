#pragma once

#include <beaker/lex/tokens.hpp>

#include <vector>


namespace beaker {

/// The lexer is responsible for transforming input characters into tokens
/// according to the following rules.
///
/// Punctuators and operators are tokens and are the following characters:
///
///   ( ) ; : ? + - * / % =
///
/// Keywords are tokens and are the following reserved words.
///
///   true false var
///
/// Literals are values denoted by tokens.
///
///   literal -> boolean-literal | integer-literal
///
///   boolean-literal -> 'true' | 'false'
///
///   integer-literals -> digit digit*
///
///   digit -> [0-9]
///
///   character-literal -> ''' escape-character '''
///
///   string-literal -> '"' escape-character* '"'
///
///   escape-character -> letter | '\' letter | ' '
///
///   letter -> [a-zA-Z]
///
/// Identifiers are tokens and are defined by the following pattern:
///
///   identifier -> (letter | '_') (letter | digit | '_')*
///
/// Comments begin with the characters '//' and continue until the end of
/// the line. These are removed from the input source (i.e., treated as white
/// space).
///
/// The lexer operates over a range of characters, represented by a pair of
/// pointers. Tokens created by the lexer are destroyed along with the
/// lexer.
///
///
/// \todo Implement hex and binary integer literals.
///
/// \todo Implement hex escape characters. 
///
/// \todo Implement hex escape characters.
///
/// \todo Implement string and character prefixes. Or should those be 
/// user-defined suffixes?
struct lexer
{
  lexer(const keyword_table&, symbol_table&, const char*, const char*);
  lexer(const keyword_table&, symbol_table&, const std::string&);
  ~lexer();

  token* lex();

private:
  /// Returns true if the lexer is at the end of input.
  bool eof() const;

  /// Returns the current lookahead character.
  char lookahead() const;

  /// Returns the nth character past the current lookahead character.
  char lookahead(int) const;
  
  /// Consume the current character and return it. This advances to the next
  /// input character.
  char consume();

  /// Ignore the current character. This advances to the next input character.
  void ignore();

  /// Assuming the current character matches, match `c` against `lookahead(1)`.
  /// If that matches, consume the current character (but not `c`) and return
  /// it. Otherwise, return the null character.
  char match_after(char c);

  /// Consume whitespace.
  void space();

  /// Consume a line comment.
  void comment();

  /// Consume an escape character. Returns true if one or more characters were
  /// consumed and false otherwise. Immediately returns false if lookahead
  /// is a newline, a '\', or the character `c`. Note that `c` is either
  /// a single or double quotation mark.
  bool escape_character(char c);

  /// Match a punctuator or operator.
  token* puncop0(token_kind);

  /// Match a punctuator or operator, and ignore one character.
  token* puncop1(token_kind);

  /// Match a word (identifier or keyword).
  token* word();

  /// Match an integer literal.
  token* integer();

  /// Match a character literal.
  token* character();

  /// Match a string literal.
  token* string();

  /// The keyword table associates reserved words with token names.
  const keyword_table* kws;

  /// A symbol table that stores identifiers.
  symbol_table* syms;

  /// The input characters.
  const char* first;
  const char* limit;

  /// Stores characters that form the current token.
  std::string buf;

  /// Stores each token created by the lexer. These are deleted when the
  /// lexer is destroyed.
  std::vector<token*> toks;
};

} // namespace beaker
