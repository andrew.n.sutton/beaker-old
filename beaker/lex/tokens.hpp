#pragma once

#include <beaker/common/symbols.hpp>

#include <iosfwd>
#include <string>
#include <unordered_map>


namespace beaker {

/// Represents the names of tokens.
enum token_kind {
#define def_token(K, S) K ## _tok,
#define def_keyword(K, S) K ## _kw,
#include "tokens.def"
#undef def_keyword
#undef def_token
};


/// The keyword table associates reserved words with their corresponding
/// token names.
struct keyword_table : std::unordered_map<std::string, token_kind>
{
  keyword_table();
};


/// Represents individual symbols in the input source. Derived classes provide
/// additional attributes for a token.
struct token
{
  virtual ~token() = default;

  token(token_kind k) : kind(k) { }

  /// Returns the token kind (it's encoded name).
  int get_kind() const { return kind; }

  /// Returns the name of the token kind.
  static const char* get_name(token_kind);

  /// Returns the token name.
  const char* get_name() const { return get_name(kind); }

  /// Returns the spelling of the token kind.
  static const char* str(token_kind);

  /// Returns the spelling of the tokens. This can be overriden for tokens
  /// with custom attributes.
  virtual std::string str() const { return str(kind); }

  token_kind kind;
};


/// Integer tokens contain their integer value.
struct int_token : token
{
  int_token(int n, int r = 10) : token(int_tok), value(n), radix(r) { }
  
  /// Returns the integer value of the token.
  int get_value() const { return value; }

  /// Returns the radix of the token.
  int get_radix() const { return radix; }

  /// Returns the string representation of the token.
  std::string str() const override;

  int value;
  int radix;
};


/// Determines the character encoding for character and string literals.
///
/// \todo This probably needs to move into common so that we can use this
/// in the AST. We'll also need other tools to support that.
enum encoding {
  basic_enc,
  utf8_enc,
  utf16_enc,
  utf32_enc,
};


/// The base class of character and string tokens. This provides storage
/// for the unelaborated sequence of characters in the literal.
///
/// \todo Should we store strings in the symbol table? It will cut down on
/// storage requirements.
struct text_token : token
{
  text_token(token_kind k, encoding e, std::string&& s) : token(k), code(e), chars(std::move(s)) { }

  /// Returns the integer value of the token.
  const std::string& get_value() const { return chars; }

  /// Returns the character encoding of the token.
  encoding get_encoding() const { return code; }

  /// Returns the sring representation of the token.
  std::string str() const override { return chars; }

  std::string chars;
  encoding code;
};


/// Character tokens refer to their character value. Their character value
/// is represented as a 32-bit value in order to support UTF16 and UTF32
/// character literals.
struct char_token : text_token
{
  char_token(encoding e, std::string&& s) : text_token(char_tok, e, std::move(s)) { }
};


/// String tokens refer to their written string.
struct str_token : text_token
{
  str_token(encoding e, std::string&& s) : text_token(str_tok, e, std::move(s)) { }
};


/// Identifiers contain their symbol.
///
/// \todo This should be a pointer to the symbol stored in the symbol table.
struct id_token : token
{
  id_token(symbol* s) : token(id_tok), sym(s) { }

  /// Returns the string representation of the identifier.
  std::string str() const override { return *sym; }
  
  /// The symbol spelling the identifier.
  symbol* sym;
};


/// Print the token to an output stream.
std::ostream& operator<<(std::ostream&, const token&);
std::ostream& operator<<(std::ostream&, const token*);

} // namespace beaker
