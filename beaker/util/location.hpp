#pragma once

#include <cstdint>


namespace beaker {

/// Represents a source code location. This is an index into an input set,
/// which can be resolved, as needed, into a source location.
struct location
{
  unsigned index;
};


// A fully resolved location within a text object.
struct source_location
{
  source_location() : path(), line(-1), col(-1) { }
  source_location(const char* p, int l, int c) : path(p), line(l), col(c) { }

  /// Returns true if the source code location is invalid.
  bool is_invalid() const { return line == -1; }

  // Returns the name of the file.
  const char* get_path() const { return path; }

  // Returns the 0-based line number of the location.
  int get_line() const { return line; }

  // Returns the 0-based column number of the location.
  int get_column() const { return col; }

  const char* path;
  int line;
  int col;
};

} // namespace beaker

