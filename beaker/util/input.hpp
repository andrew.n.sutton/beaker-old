#pragma once

#include <beaker/util/location.hpp>

#include <string>
#include <vector>


namespace beaker {

/// A line map stores the beginning character offsets of each line in an
/// input buffer. This supports reasonably efficient (log N) lookup from a 
/// character offset to its corresponding line.
struct line_map
{
  line_map(const std::string& buf);

  /// Returns the length of the longest line in the file.
  int get_width() const { return width; }

  /// Returns the offset of the nth line in the file.
  int operator[](int n) const { return offsets[n]; }

  /// Returns the number of lines.
  std::size_t size() const { return offsets.size(); }

  /// Returns true if there are no lines.
  bool empty() const { return offsets.empty(); }

  auto begin() { return offsets.begin(); }
  auto end() { return offsets.end(); }

  auto begin() const { return offsets.begin(); }
  auto end() const { return offsets.end(); }

  /// Stores the starting offset of each line in the file.
  std::vector<int> offsets; 

  /// Stores the column widths of each line in the file.
  std::vector<int> cols;

  /// Cached maximum column width.
  int width;
};


// The input class represents input text containing fragments of a program.
// When created, the contents of the buffer are copied into the object
// (i.e., not shared), and analyzed to determine the line structure of
// of the text.
//
// TODO: It might be nice to generalize the notion of textual "origin":
// whether the text originates in a file, an input stream, or simply a
// in-memory buffer.
struct input
{
  input(const char* p, const std::string& b, unsigned n);

  /// Returns the underlying string data of the text.
  const std::string& data() const { return buf; }

  /// Returns the base offset used to compute opaque source code locations.
  unsigned get_base_offset() const { return base; }

  /// Returns the number of characters in the text buffer.
  std::size_t size() const { return buf.size(); }

  /// Returns the textual information about the file.
  line_map const& get_lines() const { return lines; }

  /// Returns the text of the nth line, starting at 0.
  std::string get_line(int) const;

  /// Returns the number of lines in the text.
  int get_num_lines() const { return lines.size(); }

  /// Returns the size of the widest column in the text.
  int get_num_columns() const { return lines.get_width(); }

  /// Returns the fully resolved source code location at offset n. Note
  /// that line and column are 0-based values.
  source_location resolve(location) const;

  char const* begin() const { return buf.data(); }
  char const* end() const { return buf.data() + buf.size(); }

  std::string path;
  std::string buf;
  line_map lines;
  unsigned base;
};


/// Represents the set of inputs to compilation. This provides a single address
/// space for characters, allowing all text locations to be represented
/// as a single integer value.
///
/// \todo Think of a better name for this. It could be extended to support
/// synthesized input, not just files and text.
struct input_list : std::vector<input>
{
  input_list() : offset() { }

  /// Add the file with path `p` as input. Returns the analyzed input text.
  const input& add_file(const char* p);

  /// Add the (finite amount of) text in `is` as input. Returns the analyzed 
  /// input text.
  const input& add_input(std::istream& is);
  
  /// Generate a full source location from the encoded location.
  source_location resolve(location loc) const;

  unsigned offset; // The current base offset
};

} // namespace beaker
