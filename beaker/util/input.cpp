#include "input.hpp"

#include <cassert>
#include <algorithm>
#include <fstream>
#include <iterator>


namespace beaker {

/// Construct a line map of the given input buffer.
line_map::line_map(const std::string& buf)
  : width(0)
{
  if (buf.empty())
    return;  

  // First pass: count the number of newlines in order to eliminate 
  // reallocations during the second pass.
  int lines = std::count(buf.begin(), buf.end(), '\n') + 1;
  offsets.reserve(lines);
  cols.reserve(lines);

  // Second pass: find the offset of each new line in the file.
  //
  // TODO: Use resize() above and count then use assignment to
  // set the offsets. This will eliminate all of the branches used
  // when calling push_back().
  //
  // TODO: Rewrite in terms of std::find().
  auto first = buf.begin(), // first character
       iter = first,        // current character
       last = first,        // first character of the current line
       limit = buf.end();   // past the end
  while (iter != limit) {
    if (*iter == '\n') {
      // Save the offset
      offsets.push_back(last - first);

      // Save the width and update the max width.
      int const w = iter - last;
      cols.push_back(w);
      if (w > width)
        width = w;

      last = iter + 1;
    }
    ++iter;
  }
  offsets.push_back(last - first);
}


input::input(const char* p, const std::string& b, unsigned n)
  : path(p), buf(b), lines(b), base(n)
{ }

// Returns the text for a given line.
std::string
input::get_line(int n) const
{
  assert(n < lines.size());
  if (n + 1 == lines.size())
    return buf.substr(lines[n]);
  else
    return buf.substr(lines[n], lines[n + 1] - lines[n] - 1);
}


// The line in which the offset resides is before the lower bound. The column 
// is found by subtracting the offset from that of the actual line.
//
// The location shall denote a character in this text buffer.
source_location
input::resolve(location loc) const
{
  int n = loc.index - base;
  if (n == 0)
    return {path.c_str(), 0, 0};
  auto iter = --std::lower_bound(lines.begin(), lines.end(), n);
  int line = iter - lines.begin();
  int col = n - *iter;
  return {path.c_str(), line, col};
}


const input&
input_list::add_file(char const* p)
{
  std::ifstream ifs(p);
  auto first = std::istreambuf_iterator<char>(ifs);
  auto limit = std::istreambuf_iterator<char>();
  std::string buf(first, limit);
  emplace_back(p, buf, offset);
  offset += back().size();
  return back();
}

const input&
input_list::add_input(std::istream& is)
{
  auto first = std::istreambuf_iterator<char>(is);
  auto limit = std::istreambuf_iterator<char>();
  std::string buf(first, limit);
  emplace_back(nullptr, buf, offset);
  offset += back().size();
  return back();
}

} // namespace beaker
