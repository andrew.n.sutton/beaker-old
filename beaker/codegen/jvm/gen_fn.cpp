
#include "generate.hpp"
#include "gen_fn.hpp"

#include <beaker/ast/print.hpp>

#include <cassert>
#include <iostream>
#include <sstream>


namespace beaker {
namespace jvm {

using namespace lang_fn;

value
generate_expr(codegen& gen, const call_expr* e)
{
  value fn = generate(gen, e->get_function());
  assert(fn.get_declaration() && "invalid callee");

  // Generate each argument in turn. These are pushed onto the stack.
  for (const expr* arg: e->get_arguments())
    generate(gen, arg);

  const decl* d = fn.get_declaration();
  emit_invoke_static(gen, d);
  return {};
}

value
generate_expr(codegen& gen, const eq_expr* e)
{
  throw std::runtime_error("function comparison not supported by architecture");
}

value
generate_expr(codegen& gen, const ne_expr* e)
{
  throw std::runtime_error("function comparison not supported by architecture");
}

void
generate_decl(codegen& gen, const fn_decl* d)
{
  // Use a new function generator.
  method mgen(get_file_codegen(gen), d);

  // Rewrite "main" to "__main" to avoid conflicts with the global loader.
  std::string name = *d->get_name();
  if (name == "main")
    name = "__main";

  // Generate the signature.
  mgen.outs() << ".method public static " 
              << name 
              << describe(gen, d->get_type()) 
              << '\n';

  // Bind local variable slots to each in d.
  for (const decl* p : d->get_parameters())
    mgen.declare_local(p);

  // Generate the definition into a new block called the entry block.
  block bgen(mgen, "entry");
  if (const stmt* s = d->get_body())
    generate(bgen, s);

  // Generate observed limits.
  mgen.outs() << "  .limit stack " << mgen.max_stack_size() << '\n';
  mgen.outs() << "  .limit locals " << mgen.locals.size() << '\n';
  
  // Generate the body.
  mgen.outs() << bgen.outs().rdbuf();
  mgen.outs() << ".end method\n\n";

  // Merge the function into parent context.
  gen.outs() << mgen.outs().rdbuf();
}

void
generate_decl(codegen& gen, const parm_decl* d)
{
  throw std::logic_error("unreachable");
}

void
generate_decl(codegen& gen, const ret_decl* d)
{
  throw std::logic_error("unreachable");
}

void
generate_decl(codegen& gen, const var_decl* d)
{  
  // Create a binding for the declaration in the current method.
  method& m = get_method_codegen(gen);
  int n = m.declare_local(d);

  // Generate the initialization sequence.
  generate(gen, d->get_initializer());

  // FIXME: Handle object vs. reference initialization?
  const type* t = d->get_type();
  if (is_bool(t) || is_integral(t))
    emit_istore(gen, n);
  else
    throw std::runtime_error("invalid variable type");  
}

void
generate_stmt(codegen& gen, const block_stmt* s)
{
  for (const stmt* sub : s->get_statements())
    generate(gen, sub);
}

void
generate_stmt(codegen& gen, const if_stmt* s)
{
  method& m = get_method_codegen(gen);
  std::string then = m.make_label("if/then");
  std::string end = m.make_label("if/end");
  generate(gen, s->get_condition());
  emit_ipush(gen, 1);
  emit_if_icmpeq(gen, then); // Branch if true, fall through if false
  generate(gen, s->get_false_branch());
  emit_goto(gen, end);
  emit_label(gen, then);
  generate(gen, s->get_true_branch());
  emit_label(gen, end);
}

/// \todo Support loops.
void
generate_stmt(codegen& gen, const while_stmt* s)
{
  method& m = get_method_codegen(gen);
  std::string top = m.make_label("while/top");
  std::string bot = m.make_label("while/bot");
  generating_loop loop(m, top, bot);
  emit_label(gen, top);
  generate(gen, s->get_condition());
  emit_ipush(gen, 0);
  emit_if_icmpeq(gen, bot); // Branch if false, fall through if true
  generate(gen, s->get_body());
  emit_goto(gen, top);
  emit_label(gen, bot);
}

void
generate_stmt(codegen& gen, const break_stmt* s)
{
  method& m = get_method_codegen(gen);
  emit_goto(gen, m.get_break_label());
}

void
generate_stmt(codegen& gen, const cont_stmt* s)
{
  method& m = get_method_codegen(gen);
  emit_goto(gen, m.get_continue_label());
}

void
generate_stmt(codegen& gen, const ret_stmt* s)
{
  const expr* e = s->get_return_value();
  generate(gen, e);

  const type* t = e->get_type();
  if (is_void(t))
    emit_return(gen);
  else if (is_bool(t) || is_integral(t))
    emit_ireturn(gen);
  else
    throw std::runtime_error("invalid return type");
}

void
generate_stmt(codegen& gen, const decl_stmt* s)
{
  generate(gen, s->get_declaration());
}

void
generate_stmt(codegen& gen, const expr_stmt* s)
{
  generate(gen, s->get_expression());
}


} // namespace jvm
} // namespace beaker
