#include "gen_store.hpp"
#include "generate.hpp"

#include <beaker/ast/decls.hpp>
#include <beaker/ast/print.hpp>

#include <cassert>
#include <iostream>


namespace beaker {
namespace jvm {

using namespace lang_store;

/// Generation depends on the declaration referenced. For stored references,
/// just return the declaration -- we're going to load it or store to it
/// in a subsequent expression. For functions, return the reference so we
/// can call it. For anything else, generate its value.
value
generate_expr(codegen& gen, const ref_expr* e)
{
  const decl* d = e->get_declaration();
  if (is_function(d) || is_variable(d))
    return {d};
  else
    throw std::runtime_error("invalid reference");
}

/// Load the referenced operand onto the stack. Since this computes a value,
/// it does not return a reference.
value
generate_expr(codegen& gen, const value_expr* e)
{
  method& m = get_method_codegen(gen);

  // Evaluate the operand. It must be a reference.
  value val = generate(gen, e->get_operand());
  
  const decl* d = val.get_declaration();
  if (has_automatic_storage(d)) {
    // The the local storage slot.
    int n = m.lookup_local(d);

    // Generate a load based on the reference expression's type.
    const type* t = get_object_type(e->get_type());
    if (is_bool(t) || is_integral(t))
      emit_iload(gen, n);
    else
      throw std::runtime_error("load an object of unknown type");
  }
  else {
    // FIXME: The JVM doesn't actually have static storage.
    throw std::runtime_error("load from static storage not implemented");
  }
  return {};
}

/// Assign the RHS to the object referred to by the LHS. Returns the object
/// referred to by the LHS.
value
generate_expr(codegen& gen, const assign_expr* e)
{
  method& m = get_method_codegen(gen);

  // Evaluate operands. The LHS must be a reference.
  value val = generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  
  // FIXME: This all depends on the kind of reference computed by the
  // LHS (i.e., the "path" to the object).
  const decl* d = val.get_declaration();
  if (has_automatic_storage(d)) {
    // Get the slot for the local variable.
    int n = m.lookup_local(d);

    // Generate a store based on the stored object's declared type.
    const type* t = d->get_type();
    if (is_bool(t) || is_integral(t))
      emit_istore(gen, n);
    else
      throw std::runtime_error("load an object of unknown type");
  }
  else {
    // FIXME: The JVM doesn't actually have static storage.
    throw std::runtime_error("load from static storage not implemented");
  }

  return val;
}

value
generate_expr(codegen& gen, const nop_init* e)
{
  // Do nothing.
  return {};
}

value
generate_expr(codegen& gen, const zero_init* e)
{
  // FIXME: What should we do here?
  return {};
}

value
generate_expr(codegen& gen, const copy_init* e)
{
  generate(gen, e->get_value());
  return {};
}

value
generate_expr(codegen& gen, const bind_init* e)
{
  // FIXME: Is this right?
  generate(gen, e->get_value());
  return {};
}


} // namespace jvm
} // namespace beaker
