#pragma once

#include "generate.hpp"

#include <beaker/ast/lang.store/types.hpp>
#include <beaker/ast/lang.store/exprs.hpp>

namespace beaker {
namespace jvm {

value generate_expr(codegen& gen, const lang_store::ref_expr* e);
value generate_expr(codegen& gen, const lang_store::value_expr* e);
value generate_expr(codegen& gen, const lang_store::assign_expr* e);

value generate_expr(codegen& gen, const lang_store::nop_init* e);
value generate_expr(codegen& gen, const lang_store::zero_init* e);
value generate_expr(codegen& gen, const lang_store::copy_init* e);
value generate_expr(codegen& gen, const lang_store::bind_init* e);

} // namespace jvm
} // namespace beaker
