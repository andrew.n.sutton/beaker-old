#pragma once

#include "generate.hpp"

#include <beaker/ast/lang.io/stmts.hpp>

namespace beaker {
namespace jvm {

void generate_stmt(codegen& gen, const lang_io::print_stmt* s);
void generate_stmt(codegen& gen, const lang_io::scan_stmt* s);

} // namespace jvm
} // namespace beaker
