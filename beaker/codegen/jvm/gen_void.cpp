#include "gen_void.hpp"
#include "generate.hpp"

#include <iostream>


namespace beaker {
namespace jvm {

using namespace lang_void;

/// This does not actually emit a nop instruction. It seems like there is
/// no good reason to do so.
value
generate_expr(codegen& gen, const nop_expr* e)
{
  return {};
}

/// Evaluate the operand and discard the result. 
value
generate_expr(codegen& gen, const void_expr* e)
{
  const expr* val = e->get_operand();
  generate(gen, e->get_operand());

  // Only emit the operand computed a value. If it computed a reference, we
  // may already have an empty stack.
  const type* ty = e->get_type();
  if (is_object(ty))
    emit_pop(gen);
  
  return {};
}

/// Allocate and throw an internal error.
value
generate_expr(codegen& gen, const trap_expr* e)
{
  const std::string cls = "java/lang/InternalError";
  emit_new(gen, cls);
  emit_dup(gen);
  emit_invoke_special(gen, "java/lang/InternalError/<init>()V");
  emit_throw(gen);
  return {};
}

} // namespace jvm
} // namespace beaker
