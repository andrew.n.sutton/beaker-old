#pragma once

#include <iosfwd>
#include <sstream>
#include <string>
#include <stack>
#include <vector>
#include <unordered_map>


namespace beaker {

struct context;
struct type;
struct expr;
struct decl;
struct stmt;

namespace jvm {

struct generator;
struct file;
struct method;
struct block;

/// The base class of all code generation contexts.
struct codegen
{
  codegen(context& cxt, codegen& parent, std::ostream& os);
protected:
  codegen(codegen& cg) : cxt(cg.cxt), parent(cg.parent), os(cg.os) { }
public:

  virtual ~codegen() { }

  /// Returns the parent code generation context. Note that the root context
  /// has itself as a parent.
  const codegen& get_parent() const { return *parent; }
  codegen& get_parent() { return *parent; }

  /// Returns the output stream.
  std::ostream& outs() { return *os; }

  /// Returns true if this is a root codegen context.
  virtual bool is_generator() const { return false; }

  /// Returns true if this is a file context.
  virtual bool is_file() const { return false; }

  /// Returns true if this is a method context.
  virtual bool is_method() const { return false; }

  /// Returns true if this is a block context.
  virtual bool is_block() const { return false; }

  /// Return this context as the root codegen context.
  generator& as_generator();

  /// Returns this context as a file context.
  file& as_file();

  /// Returns this context as a method context.
  method& as_method();

  /// Returns this context as a block context.
  block& as_block();

  context* cxt;
  codegen* parent;
  std::ostream* os;
};

inline
codegen::codegen(context& cxt, codegen& parent, std::ostream& os) 
  : cxt(&cxt), parent(&parent), os(&os) 
{ }


/// The top-level generator for a module.
struct generator : codegen
{
  generator(context& cxt);

  /// Returns true.
  bool is_generator() const override { return true; }

  /// Returns this as the parent.
  const generator& parent() const { return *this; }
  generator& parent() { return *this; }
};


/// A code generator context for class files. This ultimately becomes.
///
/// \todo The file should have a class name.
struct file : codegen
{
  file(codegen& gen) : codegen(gen) { }

  /// Returns true.
  bool is_file() const override { return true; }

  /// Returns the parent codegen context.
  const generator& get_parent() const { return parent->as_generator(); }
  generator& get_parent() { return parent->as_generator(); }
};


/// Stores the innermost labels for break and continue statements. This stores
/// references to the strings containing the labels.
struct break_continue
{
  const std::string* break_label;
  const std::string* continue_label;
};


/// A code generator for an individual method. A method encapsulates its own
/// output stream. This allows methods different methods to be generated
/// simultaneously and then added to their context later.
struct method : codegen
{
  /// \todo In full generality, this methods should take class file contexts.
  method(file& gen, const decl* fn);

  /// Returns true.
  bool is_method() const override { return true; }

  /// Returns the parent file context.
  const file& get_parent() const { return parent->as_file(); }
  file& get_parent() { return parent->as_file(); }

  /// Returns the function declaration being generated.
  const decl* get_function() const { return fn; }

  // Locals

  /// Declare `d` to be a local variable. This associates the declaration
  /// with a slot in the local store. Returns the associated slot.
  int declare_local(const decl* d);
  
  /// Returns the slot number for the bound local `d`.
  int lookup_local(const decl* d);

  // Labels

  /// Returns a unique label with `s` as the given root.
  std::string make_label(const std::string& s);

  // Break and continue

  /// Push a new pair of break/continue labels.
  void push_break_continue(const std::string& b, const std::string& c) { bc.push({&b, &c}); }

  /// Pop the innermost pair of break/continue labels.
  void pop_break_continue() { bc.pop(); }

  /// Returns the current break label.
  const std::string& get_break_label() const { return *bc.top().break_label; }
  
  /// Returns the current continue label.
  const std::string& get_continue_label() const { return *bc.top().continue_label; }

  // Stack size

  /// Returns the current stack size.
  int stack_size() const { return stack; }

  /// Returns the maximum stack size needed to execute the function.
  int max_stack_size() const { return mstack; }

  /// Indicate that an operand has pushed to the stack. Remember the
  /// largest possible stack.
  void push(int n) { stack += n; mstack = std::max(stack, mstack); }

  /// Indicate that an operand has popped from the stack.
  void pop(int n) { stack -= n; }

  /// Sets the stack size to 0.
  void clear() { stack = 0; }

  std::stringstream ss;
  const decl* fn;
  int stack; // The current stack size.
  int mstack; // The maximum observed stack size.
  std::unordered_map<const decl*, int> locals;
  std::unordered_map<std::string, int> labels;
  std::stack<break_continue> bc;
};


/// A code generation context for a block. This is primarily used to store
/// instructions separately from the function in which they will be emitted.
struct block : codegen
{
  block(method& gen, const std::string& str);

  /// Returns true.
  bool is_block() const override { return true; }

  /// Returns the parent method context.
  const method& get_parent() const { return parent->as_method(); }
  method& get_parent() { return parent->as_method(); }

  std::string label;
  std::stringstream ss;
};


/// Returns the current method code generation context.
generator& get_generator(codegen&);

/// Returns the current file code generation context.
file& get_file_codegen(codegen&);

/// Returns the current method code generation context.
method& get_method_codegen(codegen&);

/// Returns the current block code generation context.
block& get_block_codegen(codegen&);


/// Generating an expression can either generate an instruction or it can yield 
/// a value, specifically a reference to a declaration. This structure is used 
/// to communicate information to calling contexts.
///
/// \todo This really represents a reference value. This doesn't work in full
/// generality since references can be computed (e.g., reference parameters,
/// pointer dereference, array subscript, etc.). Not an easy problem to solve.
struct value
{
  value() : d() { }
  value(const decl* d) : d(d) { }

  const decl* get_declaration() const { return d; }

  const decl* d;
};

/// Emit the descriptor for the type `t`.
std::string describe(codegen& gen, const type* t);

/// Emit the code for a the given expression.
value generate(codegen& gen, const expr* e);

/// Emit the code for a the given declaration.
void generate(codegen& gen, const decl* d);

/// Emit the code for a the given statement.
void generate(codegen& gen, const stmt* s);

// Code generation dispatch.
value generate_expr(codegen& gen, const expr* e);
void generate_decl(codegen& gen, const decl* d);
void generate_stmt(codegen& gen, const stmt* s);


// Instruction emission
void emit_goto(codegen& gen, const std::string&);
void emit_label(codegen& gen, const std::string&);

void emit_pop(codegen& gen);
void emit_dup(codegen& gen);

void emit_ipush(codegen& gen, int n); // Intrinsic
void emit_iadd(codegen& gen);
void emit_isub(codegen& gen);
void emit_imul(codegen& gen);
void emit_idiv(codegen& gen);
void emit_irem(codegen& gen);
void emit_ineg(codegen& gen);
void emit_iand(codegen& gen);
void emit_ior(codegen& gen);
void emit_ixor(codegen& gen);

void emit_ieq(codegen& gen); // Intrinsic
void emit_ine(codegen& gen);
void emit_ilt(codegen& gen);
void emit_igt(codegen& gen);
void emit_ile(codegen& gen);
void emit_ige(codegen& gen);

void emit_if_icmpeq(codegen& gen, const std::string&);
void emit_if_icmpne(codegen& gen, const std::string&);
void emit_if_icmplt(codegen& gen, const std::string&);
void emit_if_icmpgt(codegen& gen, const std::string&);
void emit_if_icmple(codegen& gen, const std::string&);
void emit_if_icmpge(codegen& gen, const std::string&);

void emit_ireturn(codegen& gen);
void emit_return(codegen& gen);

void emit_istore(codegen& gen, int n); // Intrinsic
void emit_iload(codegen& gen, int n); // Intrinsic

void emit_new(codegen& gen, const std::string&);

void emit_invoke_static(codegen& gen, const decl* d);
void emit_invoke_special(codegen& gen, const std::string&);

void emit_throw(codegen& gen);


// -------------------------------------------------------------------------- //
// Helper classes

/// An RAII class that controls the registration of break/continue labels
/// for a loop.
struct generating_loop
{
  generating_loop(method& m, const std::string& b, const std::string& c)
    : cg(m)
  {
    cg.push_break_continue(b, c);
  }

  ~generating_loop()
  {
    cg.pop_break_continue();
  }

  method& cg;
};

} // namespace jvm
} // namespace beaker
