#include "gen_io.hpp"
#include "generate.hpp"

#include <iostream>


namespace beaker {
namespace jvm {

using namespace lang_io;

void
generate_stmt(codegen& gen, const print_stmt* s)
{
  throw std::runtime_error("print statements not implemented");
}

void
generate_stmt(codegen& gen, const scan_stmt* s)
{
  throw std::runtime_error("scan statements not implemented");
}

} // namespace jvm
} // namespace beaker
