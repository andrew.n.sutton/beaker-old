#pragma once

#include "generate.hpp"

#include <beaker/ast/lang.void/types.hpp>
#include <beaker/ast/lang.void/exprs.hpp>

namespace beaker {
namespace jvm {

value generate_expr(codegen& gen, const lang_void::nop_expr* e);
value generate_expr(codegen& gen, const lang_void::void_expr* e);
value generate_expr(codegen& gen, const lang_void::trap_expr* e);

} // namespace jvm
} // namespace beaker
