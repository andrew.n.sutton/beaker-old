#include "gen_bool.hpp"
#include "generate.hpp"

#include <iostream>


namespace beaker {
namespace jvm {

using namespace lang_bool;

value
generate_expr(codegen& gen, const bool_expr* e)
{
  emit_ipush(gen, e->get_value());
  return {};
}

value
generate_expr(codegen& gen, const and_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_iand(gen);
  return {};
}

value
generate_expr(codegen& gen, const or_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ior(gen);
  return {};
}

value
generate_expr(codegen& gen, const xor_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ixor(gen);
  return {};
}

value
generate_expr(codegen& gen, const imp_expr* e)
{
  throw std::logic_error("not implemented");
  return {};
}

value
generate_expr(codegen& gen, const eq_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ieq(gen);
  return {};
}

/// The expression `!e` is equivalent to `e ^ true`.
value
generate_expr(codegen& gen, const not_expr* e)
{
  generate(gen, e->get_operand());
  emit_ipush(gen, 1);
  emit_ixor(gen);
  return {};
}

/// JVM does not support generalized reference expressions. That is, we
/// cannot write:
///
///   var int x;
///   var int y;
///   (b ? x : y) = 4;
///
/// To do so, we would have to push the address of x and y onto the stack.
/// Unfortunately, the JVM does not support a load instruction that accepts
/// an arbitrary address.
value
generate_expr(codegen& gen, const if_expr* e)
{
  if (is_object(e->get_type()))
    throw std::runtime_error("reference expressions not supported by architecture");
  
  method& m = get_method_codegen(gen);
  std::string then = m.make_label("if/then");
  std::string end = m.make_label("if/end");
  generate(gen, e->get_condition());
  emit_ipush(gen, 1);
  emit_if_icmpeq(gen, then); // Branch if true, fall through if false
  generate(gen, e->get_false_value());
  emit_goto(gen, end);
  emit_label(gen, then);
  generate(gen, e->get_true_value());
  emit_label(gen, end);
  return {};
}

value
generate_expr(codegen& gen, const and_if_expr* e)
{
  method& m = get_method_codegen(gen);
  std::string skip = m.make_label("and/skip");
  generate(gen, e->get_lhs());
  emit_ipush(gen, 0);
  emit_if_icmpeq(gen, skip); // If false, skip the rhs
  generate(gen, e->get_rhs());
  emit_label(gen, skip);
  return {};
}

value
generate_expr(codegen& gen, const or_if_expr* e)
{
  method& m = get_method_codegen(gen);
  std::string skip = m.make_label("and/skip");
  generate(gen, e->get_lhs());
  emit_ipush(gen, 1);
  emit_if_icmpeq(gen, skip); // If true, skip the rhs
  generate(gen, e->get_rhs());
  emit_label(gen, skip);
  return {};
}

/// \todo Create a new string containing the pretty-printed expression
/// being thrown.
value
generate_expr(codegen& gen, const assert_expr* e)
{
  method& m = get_method_codegen(gen);
  std::string skip = m.make_label("and/skip");
  generate(gen, e->get_operand());
  emit_ipush(gen, 1);
  emit_if_icmpeq(gen, skip); // If true, skip ahead.
  
  // Generate a throw of an assertion error.
  const std::string cls = "java/lang/AssertionError";
  emit_new(gen, cls);
  emit_dup(gen);
  emit_invoke_special(gen, "java/lang/AssertionError/<init>()V");
  emit_throw(gen);

  emit_label(gen, skip);
  return {};
}


} // namespace jvm
} // namespace beaker
