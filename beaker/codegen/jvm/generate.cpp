#include "generate.hpp"

#include "gen_tu.hpp"
#include "gen_void.hpp"
#include "gen_bool.hpp"
#include "gen_int.hpp"
#include "gen_store.hpp"
#include "gen_fn.hpp"
#include "gen_io.hpp"

// FIXME: Needed to compile, but these can't be easily generated.
#include <beaker/ast/lang.array/types.hpp>
#include <beaker/ast/lang.array/exprs.hpp>

#include <cassert>
#include <iostream>


namespace beaker {
namespace jvm {

generator&
codegen::as_generator()
{
  if (is_generator())
    return static_cast<generator&>(*this);
  throw std::logic_error("not a generator");
}

file&
codegen::as_file()
{
  if (is_file())
    return static_cast<file&>(*this);
  throw std::logic_error("not a file");
}

method&
codegen::as_method()
{
  if (is_method())
    return static_cast<method&>(*this);
  throw std::logic_error("not a method");
}

block&
codegen::as_block()
{
  if (is_block())
    return static_cast<block&>(*this);
  throw std::logic_error("not a block");
}


/// Initialize the generator so that it writes to standard output.
///
/// Note that the parent is itself.
generator::generator(context& cxt) 
  : codegen(cxt, *this, std::cout) 
{ }


method::method(file& gen, const decl* fn)
  : codegen(*gen.cxt, gen, ss), ss(), fn(fn), stack(0), mstack(0)
{ }

int
method::declare_local(const decl* d) 
{
  assert(locals.count(d) == 0 && "declaration already bound");
  auto result = locals.emplace(d, locals.size());
  return result.first->second;
}

int
method::lookup_local(const decl* d)
{
  assert(locals.count(d) == 1 && "declaration not locally bound");
  return locals.find(d)->second;
}

std::string 
method::make_label(const std::string& s)
{
  auto result = labels.emplace(s, 0);
  auto iter = result.first;
  
  // Increment the label count. This guarantees that we always start with 1.
  ++iter->second;
  
  // Generate the label string.
  std::stringstream ss;
  ss << s << '/' << iter->second;
  return ss.str();
}

block::block(method& gen, const std::string& n)
  : codegen(*gen.cxt, gen, ss), ss(), label(get_method_codegen(gen).make_label(n))
{ }


// FIXME: Can we make these functions any cleaner?

block&
get_block_codegen(codegen& gen)
{
  if (block* bg = dynamic_cast<block*>(&gen))
    return *bg;
  throw std::runtime_error("not in a block\n");
}

method&
get_method_codegen(codegen& gen)
{
  if (method* m = dynamic_cast<method*>(&gen))
    return *m;
  if (file* f = dynamic_cast<file*>(&gen))
    throw std::runtime_error("not in a file context\n");
  if (generator* g = dynamic_cast<generator*>(&gen))
    throw std::runtime_error("not in a file context\n");
  return get_method_codegen(gen.get_parent());
}

file&
get_file_codegen(codegen& gen)
{
  if (file* f = dynamic_cast<file*>(&gen))
    return *f;
  if (generator* g = dynamic_cast<generator*>(&gen))
    throw std::runtime_error("not in a file context\n");
  return get_file_codegen(gen.get_parent());
}

generator&
get_generator(codegen& gen)
{
  codegen& p = get_file_codegen(gen);
  return dynamic_cast<generator&>(gen);
}


/// Generates a descriptor for a function type.
static std::string
describe_fn(codegen& gen, const lang_fn::fn_type* t)
{
  std::stringstream ss;
  ss << '(';
  for (const type* p : t->get_parameter_types())
    ss << describe(gen, p);
  ss << ')' << describe(gen, t->get_return_type());
  return ss.str();
}

/// Generates a type descriptor.
///
/// \todo How should we handle reference types. We might be able treat them as
/// arrays --- maybe.
std::string
describe(codegen& gen, const type* t)
{
  struct fn {
    std::string operator()(const type* t) const { throw std::logic_error("unreachable"); }
    std::string operator()(const lang_void::void_type*) const { return "V"; }
    std::string operator()(const lang_bool::bool_type*) const { return "Z"; }
    std::string operator()(const lang_int::int_type*) const { return "I"; }
    std::string operator()(const lang_int::nat_type*) const { return "I"; }
    std::string operator()(const lang_fn::fn_type* t) const { return describe_fn(gen, t); }

    codegen& gen;
  };
  return apply(t, fn{gen});
}


/// Dispatches to `generate_expr(gen, e)`.
value
generate(codegen& gen, const expr* e)
{
  struct fn {
    value operator()(const expr*) { throw std::logic_error("unreachable"); }
#define def_init(I, NS)
#define def_expr(E, NS) \
    value operator()(const NS::E ## _expr* e) { return generate_expr(gen, e); }
#include <beaker/ast/exprs.def>
#undef def_expr
#undef def_init

    codegen& gen;
  };
  return apply(e, fn{gen});
}

/// Dispatches to `generate_decl(gen, e)`.
void
generate(codegen& gen, const decl* d)
{
  struct fn {
    void operator()(const decl*) { throw std::logic_error("unreachable"); }
#define def_decl(T, NS) \
    void operator()(const NS::T ## _decl* d) { return generate_decl(gen, d); }
#include <beaker/ast/decls.def>
#undef def_decl

    codegen& gen;
  };
  apply(d, fn{gen});
}

void
generate(codegen& gen, const stmt* s)
{
  struct fn {
    void operator()(const stmt*) { throw std::logic_error("unreachable"); }
#define def_stmt(T, NS) \
    void operator()(const NS::T ## _stmt* s) { return generate_stmt(gen, s); }
#include <beaker/ast/stmts.def>
#undef def_stmt

    codegen& gen;
  };
  apply(s, fn{gen});
}

value
generate_expr(codegen& gen, const expr* e)
{
  throw std::logic_error("undefined codegen method");
}

void
generate_decl(codegen& gen, const decl* d)
{
  throw std::logic_error("undefined codegen method");
}

void
generate_stmt(codegen& gen, const stmt* s)
{
  throw std::logic_error("undefined codegen method");
}


// ---------------------------------------------------------------------------//
// Instructions

void
emit_label(codegen& gen, const std::string& label)
{
  gen.outs() << "  " << label << ":\n";
}

void
emit_goto(codegen& gen, const std::string& label)
{
  gen.outs() << "  goto " << label << '\n';
}

void
emit_pop(codegen& gen)
{
  gen.outs() << "  pop\n";
  method& m = get_method_codegen(gen);
  m.pop(1);
}

void
emit_dup(codegen& gen)
{
  gen.outs() << "  dup\n";
  method& m = get_method_codegen(gen);
  m.push(1);
}

/// Emit an instruction code that pushes the constant value `n`. This reduces
/// to `iconst_n`, `Tipush`, or a load from the constant pool depending on
/// the size of the operand.
void
emit_ipush(codegen& gen, int n)
{  
  // Select an instruction.
  switch (n) {
    case -1:
      gen.outs() << "  iconst_m1\n";
      break;
    case 0:
      gen.outs() << "  iconst_0\n";
      break;
    case 1:
      gen.outs() << "  iconst_1\n";
      break;
    case 2:
      gen.outs() << "  iconst_2\n";
      break;
    case 3:
      gen.outs() << "  iconst_3\n";
      break;
    case 4:
      gen.outs() << "  iconst_4\n";
      break;
    case 5:
      gen.outs() << "  iconst_5\n";
      break;
    default:
      if (-127 < n && n < 127)
        gen.outs() << "  bipush " << n << '\n';
      else if (-32767 < n && n < 32767)
        gen.outs() << "  sipush " << n << '\n';
      else
        throw std::runtime_error("integer constant overflow");
      break;
  }

  // This pushes one operand.
  method& m = get_method_codegen(gen);
  m.push(1);
}

// Update the stack for a unary operation. This pops an operand and pushes
// the result.
static void
stack_unary_op(codegen& gen)
{
  method& m = get_method_codegen(gen);
  m.pop(1);
  m.push(1);
}

// Update the stack for a binary operation. This pops two operands and pushes
// the result.
static void
stack_binary_op(codegen& gen)
{
  method& m = get_method_codegen(gen);
  m.pop(2);
  m.push(1);
}

void
emit_iadd(codegen& gen)
{
  gen.outs() << "  iadd\n";
  stack_binary_op(gen);
}

void
emit_isub(codegen& gen)
{
  gen.outs() << "  isub\n";
  stack_binary_op(gen);
}

void
emit_imul(codegen& gen)
{
  gen.outs() << "  imul\n";
  stack_binary_op(gen);
}

void
emit_idiv(codegen& gen)
{
  gen.outs() << "  idiv\n";
  stack_binary_op(gen);
}

void
emit_irem(codegen& gen)
{
  gen.outs() << "  irem\n";
  stack_binary_op(gen);
}

void
emit_ineg(codegen& gen)
{
  gen.outs() << "  ineg\n";
  stack_unary_op(gen);
}

void
emit_iand(codegen& gen)
{
  gen.outs() << "  iand\n";
  stack_binary_op(gen);
}

void
emit_ior(codegen& gen)
{
  gen.outs() << "  ior\n";
  stack_binary_op(gen);
}

void
emit_ixor(codegen& gen)
{
  gen.outs() << "  ixor\n";
  stack_binary_op(gen);
}

/// A helper function for all of the integer comparison functions. This
/// is parameterized by the comparison function that evaluates when two
/// operands are true. 
///
/// This pushes 1 if the emit_if comparison is true and 0 otherwise.
static void
emit_icmp(codegen& gen, void (*emit_if)(codegen&, const std::string&))
{
  method& m = get_method_codegen(gen);
  std::string then = m.make_label("if/then");
  std::string end = m.make_label("if/end");
  emit_if(gen, then);
  emit_ipush(gen, 0);
  emit_goto(gen, end);     
  emit_label(gen, then);
  emit_ipush(gen, 1);
  emit_label(gen, end);
}

void
emit_ieq(codegen& gen)
{
  emit_icmp(gen, emit_if_icmpeq);
}

void
emit_ine(codegen& gen)
{
  emit_icmp(gen, emit_if_icmpne);
}

void
emit_ilt(codegen& gen)
{
  emit_icmp(gen, emit_if_icmplt);
}

void
emit_igt(codegen& gen)
{
  emit_icmp(gen, emit_if_icmpgt);
}

void
emit_ile(codegen& gen)
{
  emit_icmp(gen, emit_if_icmple);
}

void
emit_ige(codegen& gen)
{
  emit_icmp(gen, emit_if_icmpge);
}

static void
emit_if_icmp(codegen& gen, const char* cmp, const std::string& label)
{
  gen.outs() << "  if_icmp" << cmp << ' ' << label << '\n';
  method& m = get_method_codegen(gen);
  m.pop(2);
}

void
emit_if_icmpeq(codegen& gen, const std::string& label)
{
  emit_if_icmp(gen, "eq", label);
}

void
emit_if_icmpne(codegen& gen, const std::string& label)
{
  emit_if_icmp(gen, "ne", label);
}

void
emit_if_icmplt(codegen& gen, const std::string& label)
{
  emit_if_icmp(gen, "lt", label);
}

void
emit_if_icmpgt(codegen& gen, const std::string& label)
{
  emit_if_icmp(gen, "gt", label);
}

void
emit_if_icmple(codegen& gen, const std::string& label)
{
  emit_if_icmp(gen, "le", label);
}

void
emit_if_icmpge(codegen& gen, const std::string& label)
{
  emit_if_icmp(gen, "ge", label);
}

/// Emits a return of an integer value.
void
emit_ireturn(codegen& gen)
{
  gen.outs() << "  ireturn\n";
  method& m = get_method_codegen(gen);
  m.clear();
}

/// Emits a return for a void function. This does not pop values.
void
emit_return(codegen& gen)
{
  gen.outs() << "  return\n";
  method& m = get_method_codegen(gen);
  m.clear();
}

/// This is an intrinsic. It reduces to one of the istore instructions.
void
emit_istore(codegen& gen, int n)
{
  switch (n) {
    case 0:
      gen.outs() << "  istore_0\n";
      break;
    case 1:
      gen.outs() << "  istore_1\n";
      break;
    case 2:
      gen.outs() << "  istore_2\n";
      break;
    case 3:
      gen.outs() << "  istore_3\n";
      break;
    default:
      gen.outs() << "  istore " << n << '\n';
      break;
  }
  method& m = get_method_codegen(gen);
  m.pop(1);
}

/// This is an intrinsic. It reduces to one of the iload instructions.
void
emit_iload(codegen& gen, int n)
{
  switch (n) {
    case 0:
      gen.outs() << "  iload_0\n";
      break;
    case 1:
      gen.outs() << "  iload_1\n";
      break;
    case 2:
      gen.outs() << "  iload_2\n";
      break;
    case 3:
      gen.outs() << "  iload_3\n";
      break;
    default:
      gen.outs() << "  iload " << n << '\n';
      break;
  }
  method& m = get_method_codegen(gen);
  m.push(1);
}

void
emit_new(codegen& gen, const std::string& cls)
{
  gen.outs() << "  new " << cls << '\n';
  method& m = get_method_codegen(gen);
  m.push(1);
}

void
emit_invoke_static(codegen& gen, const decl* d)
{
  const auto* fn = static_cast<const lang_fn::fn_decl*>(d);

  // FIXME: Correctly compute the qualification on the function name.
  gen.outs() << "  invokestatic " 
             << "Main/" << *fn->get_name() 
             << describe(gen, fn->get_type()) << '\n';

  // Update the stack.  
  method& m = get_method_codegen(gen);
  m.pop(fn->get_parameters().size());
  m.push(1);
}

/// Emit a call to the special function with name `s`. This is generally used 
/// to invoke runtime functions. Note that `s` must be fully qualified and
/// contain the type descriptor.
void
emit_invoke_special(codegen& gen, const std::string& s)
{
  gen.outs() << "  invokespecial " << s << '\n';
}


void
emit_throw(codegen& gen)
{
  gen.outs() << "  athrow\n";
  method& m = get_method_codegen(gen);
  m.clear();
  m.push(1);
}

} // namespace jvm
} // namespace beaker
