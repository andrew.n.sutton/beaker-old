#pragma once

#include <beaker/codegen/jvm/generate.hpp>
#include <beaker/ast/lang.tu/decls.hpp>


namespace beaker {
namespace jvm {

void generate_decl(codegen& gen, const lang_tu::tu_decl* d);

} // namespace jvm
} // namespace beaker
