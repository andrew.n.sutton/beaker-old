#include "gen_int.hpp"
#include "generate.hpp"

#include <iostream>


namespace beaker {
namespace jvm {

using namespace lang_int;

// Note that none of these expressions take references or yield a reference.
// Therefore, we can ignore the return values from all generate functions.

value
generate_expr(codegen& gen, const int_expr* e)
{
  emit_ipush(gen, e->get_signed());
  return {};
}

value
generate_expr(codegen& gen, const add_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_iadd(gen);
  return {};
}

/// \todo Are the operands pushed in the right order?
value
generate_expr(codegen& gen, const sub_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_isub(gen);
  return {};
}

value
generate_expr(codegen& gen, const mul_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_imul(gen);
  return {};
}

/// \todo Are the operands pushed in the right order?
value
generate_expr(codegen& gen, const div_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_idiv(gen);
  return {};
}

/// \todo Are the operands pushed in the right order?
value
generate_expr(codegen& gen, const rem_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_irem(gen);
  return {};
}

value
generate_expr(codegen& gen, const neg_expr* e)
{
  generate(gen, e->get_operand());
  emit_ineg(gen);
  return {};
}

// FIXME: All of these require branches which is totally stupid.

value
generate_expr(codegen& gen, const eq_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ieq(gen);
  return {};
}

value
generate_expr(codegen& gen, const ne_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ine(gen);
  return {};
}

value
generate_expr(codegen& gen, const lt_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ilt(gen);
  return {};
}

value
generate_expr(codegen& gen, const gt_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_igt(gen);
  return {};
}

value
generate_expr(codegen& gen, const le_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ile(gen);
  return {};
}

value
generate_expr(codegen& gen, const ge_expr* e)
{
  generate(gen, e->get_lhs());
  generate(gen, e->get_rhs());
  emit_ige(gen);
  return {};
}

} // namespace jvm
} // namespace beaker
