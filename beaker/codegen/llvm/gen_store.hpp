#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.store/types_store.hpp>
#include <beaker/ast/lang.store/exprs_store.hpp>


namespace beaker {
namespace llvm {

type generate_type(codegen& gen, const lang_store::ref_type* t);

value generate_expr(codegen& gen, const lang_store::ref_expr* e);
value generate_expr(codegen& gen, const lang_store::value_expr* e);
value generate_expr(codegen& gen, const lang_store::assign_expr* e);

value generate_init(codegen& gen, object& obj, const lang_store::nop_init* e);
value generate_init(codegen& gen, object& obj, const lang_store::zero_init* e);
value generate_init(codegen& gen, object& obj, const lang_store::copy_init* e);
value generate_init(codegen& gen, object& obj, const lang_store::bind_init* e);

} // namespace llvm
} // namespace beaker
