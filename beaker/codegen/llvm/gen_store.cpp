#include "gen_store.hpp"

#include <beaker/ast/decls.hpp>
#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {
namespace llvm {

using namespace lang_store;

type
generate_type(codegen& gen, const ref_type* t)
{
  type obj = generate(gen, t->get_object_type());
  return emit_pointer_type(gen, obj);
}

value
generate_expr(codegen& gen, const ref_expr* e)
{
  function& fgen = get_function_codegen(gen);
  module& mgen = fgen.get_parent();

  named_decl *d = e->get_declaration();

  // Functions are stored globally.
  if (is_function(d))
    return mgen.lookup_global(d);

  // Otherwise, resolve the reference locally.
  //
  // FIXME: Check the storage duration of d. It could be global.
  return fgen.lookup_local(d);
}

value
generate_expr(codegen& gen, const value_expr* e)
{
  value ptr = generate(gen, e->get_operand());
  return emit_load(gen, ptr);
}

value
generate_expr(codegen& gen, const assign_expr* e)
{
  value ptr = generate(gen, e->get_lhs());
  value val = generate(gen, e->get_rhs());
  return emit_store(gen, ptr, val);
}

/// Returns an uninhabited value.
///
/// Note that void returns are trivially initialized. In that case, we don't 
/// need an object for initialization.
///
/// \todo Under what circumstances should we install a trap representation?
value
generate_init(codegen& gen, object& obj, const nop_init* e)
{
  if (is_void(e->get_type()))
    return {};

  assert(obj && "trivial initialization without object");
  return {};
}

/// Returns an uninhabited value.
value
generate_init(codegen& gen, object& obj, const zero_init* e)
{
  assert(obj && "zero initialization without object");
  type t = generate(gen, e->get_type());
  value v = emit_null(gen, t);
  emit_store(gen, obj.get_pointer(), v);
  return {};
}

/// Evaluate the initializer, and if an object is provided store the value.
/// Returns the computed initializer value.
value
generate_init(codegen& gen, object& obj, const copy_init* e)
{
  value init = generate(gen, e->get_value());
  if (obj)
    emit_store(gen, obj.get_pointer(), init);
  return init;
}

/// Just return the computed reference of the initializer. Binding is handled
/// by the caller.
value
generate_init(codegen& gen, object& obj, const bind_init* e)
{
  assert(!obj && "direct binding with a stored object");
  return generate(gen, e->get_value());
}

} // namespace llvm
} // namespace beaker
