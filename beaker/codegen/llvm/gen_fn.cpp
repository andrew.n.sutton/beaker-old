#include "gen_fn.hpp"

#include <beaker/ast/print.hpp>

#include <llvm/IR/Function.h>

#include <cassert>
#include <iostream>


namespace beaker {
namespace llvm {

using namespace lang_fn;

/// \todo Handle indirect return and parameter types.
type
generate_type(codegen& gen, const fn_type* t)
{
  std::vector<type> parms;

  // FIXME: Handle indirect return types. Note that if the return type
  // is indirect, then it becomes an implicit parameter and the return
  // type is void.
  type ret = generate_value_type(gen, t->get_return_type());

  for (const auto* p : t->get_parameter_types()) {
    // FIXME: Handle indirect parameter types.
    type parm = generate_value_type(gen, p);
    parms.push_back(parm);
  }
  
  return emit_function_type(gen, ret, parms);
}


value
generate_expr(codegen& gen, const call_expr* e)
{
  value fn = generate(gen, e->get_function());

  // FIXME: If the function's return type is passed indirectly, materialize
  // an extra argument.

  std::vector<value> args;
  for (const expr* a : e->get_arguments()) {
    // Initialize each argument. Don't store the result because that's done
    // on the other side of the call.
    //
    // FIXME: Arguments passed indirectly need a materialized temporary.
    object obj;
    value arg = initialize(gen, obj, a);
    args.push_back(arg);
  }
  return emit_call(gen, fn, args);
}

value
generate_expr(codegen& gen, const eq_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_ieq(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const ne_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_ine(gen, lhs, rhs);
}


/// \todo There may be attributes that we can set for the function.
void
generate_decl(codegen& gen, const fn_decl* d)
{
  module& mgen = gen.as_module();
  function fgen(mgen, d);

  // Declare the function.
  llir::Function* llfn = fgen.get_llvm_function();
  mgen.bind_global(d, llfn);

  const decl* ret = d->get_return();
  const decl_seq& parms = d->get_parameters();

  auto arg_iter = llfn->arg_begin();
  auto parm_iter = parms.begin();

  // FIXME: If the return type is indirect, then arg_iter points to the
  // output parameter. Set return value properties for that argument.

  // Assign a name to each parameter.
  while (parm_iter != parms.end() && arg_iter != llfn->arg_end()) {
    const decl* parm = *parm_iter;
    auto* arg = &*arg_iter;

    arg->setName(*parm->get_name());

    // FIXME: If the parameter type is passed indirectly, then mark it
    // non-null. Similarly if it's a reference parameter.

    ++parm_iter;
    ++arg_iter;
  }

  // Generate the body if needed.
  if (const stmt* s = d->get_body()) {

    fgen.start_definition();

    arg_iter = llfn->arg_begin();
    parm_iter = parms.begin();

    // For non-void returns, allocate the return value. Note that we don't
    // add an explicit declaration binding for the return value.
    //
    // FIXME: If the return type is indirect, then we don't need to emit
    // a local definition, we can just set the return value as the argument,
    // and bump the argument iterator.
    if (!is_void(ret->get_type())) {
      type ty = generate(gen, ret->get_type());
      value ptr = emit_alloca(fgen, ty, "ret.val");
      fgen.set_return_value(ptr);
    }

    // Generate and initialize a local for each parameter.
    while (parm_iter != parms.end() && arg_iter != llfn->arg_end()) {
      const decl* parm = *parm_iter;
      auto* arg = &*arg_iter;

      // Allocate a local variable for the parameter.
      object obj = emit_local(fgen, parm, "parm." + *parm->get_name());

      // If we allocated the object, store the argument. Also set the the 
      // value for the binding.
      value val;
      if (obj) {
        val = obj.get_pointer();
        emit_store(fgen, val, arg);
      }
      else {
        val = arg;
      }

      // If we haven't bound the variable to its initializer, do so now.
      if (!obj)
        fgen.bind_local(parm, val);

      ++parm_iter;
      ++arg_iter;
    }

    // Generate the statements of the function.
    generate(fgen, s);

    // Finish up the definition.
    fgen.finish_definition();
  }
}

void
generate_decl(codegen& gen, const let_decl* d)
{
  function& fgen = get_function_codegen(gen);

  // Evaluate the initializer without an object and bind it directly to
  // it's declaration.
  object obj;
  value init = initialize(gen, obj, d->get_initializer());
  fgen.bind_local(d, init);
}

void
generate_decl(codegen& gen, const var_decl* d)
{
  function& fgen = get_function_codegen(gen);
  
  // Allocate and bind the local variable. If d is not an object, then no
  // object is allocated, and the value is unbound.
  //
  // FIXME: If the initializer of a reference refers to its declaration, this
  // will crash. We should not allow an initializer to refer to itself.
  object obj = emit_local(fgen, d);

  // Initialize the object.
  value val = initialize(gen, obj, d->get_initializer());

  // If we didn't allocate storage, then bind the declaration now.
  if (!obj)
    fgen.bind_local(d, val);
}

/// Generate the sequence of statements into the current context.
///
/// \todo We'll need cleanup handlers for destructors at some point.
void
generate_stmt(codegen& gen, const block_stmt* s)
{
  for (const stmt* ss : s->get_statements())
    generate(gen, ss);
}

void
generate_stmt(codegen& gen, const if_stmt* s)
{
  block then_block(gen, "if.then");
  block else_block(gen, "if.else");
  block end_block(gen, "if.end");

  value cond = generate(gen, s->get_condition());
  emit_branch(gen, cond, then_block, else_block);

  // Emit the then block.
  emit_block(gen, then_block);
  generate(gen, s->get_true_branch());
  then_block = get_current_block(gen);
  if (!then_block.has_terminator())
    emit_branch(then_block, end_block);

  // Emit the else block.
  emit_block(gen, else_block);
  generate(gen, s->get_false_branch());
  else_block = get_current_block(gen);
  if (!else_block.has_terminator())
    emit_branch(else_block, end_block);

  // Emit the end block.
  emit_block(gen, end_block);
}

void
generate_stmt(codegen& gen, const while_stmt* s)
{
  function& fgen = get_function_codegen(gen);

  block top_block(gen, "while.top");
  block body_block(gen, "while.body");
  block bot_block(gen, "while.bot");

  fgen.enter_loop(top_block, bot_block);

  // Emit the top of the loop and the condition test.
  emit_block(gen, top_block);
  value cond = generate(gen, s->get_condition());
  emit_branch(gen, cond, body_block, bot_block);

  // Emit the loop body and the branch to the top.
  emit_block(gen, body_block);
  generate(gen, s->get_body());
  emit_branch(gen, top_block);

  // Emit the bottom of the loop.
  emit_block(gen, bot_block);

  fgen.leave_loop();
}

void
generate_stmt(codegen& gen, const break_stmt* s)
{
  function& fgen = get_function_codegen(gen);
  emit_branch(gen, fgen.get_loop_bottom());
}

void
generate_stmt(codegen& gen, const cont_stmt* s)
{
  function& fgen = get_function_codegen(gen);
  emit_branch(gen, fgen.get_loop_top());
}

void
generate_stmt(codegen& gen, const ret_stmt* s)
{
  function& fgen = get_function_codegen(gen);

  // Reconstruct the (possibly) allocated return value.
  object obj(fgen.get_return_value());

  // Initialize the return value.
  //
  // FIXME: Save the value in case we need to return it directly.
  initialize(gen, obj, s->get_return_value());

  // Branch to the exit block.
  //
  // FIXME: If we didn't initialize a return value object, then we can
  // return the computed value. If we did initialize an object, then we
  // either return void or it's address (depending on the return type).
  emit_branch(gen, fgen.get_exit_block());
}

void
generate_stmt(codegen& gen, const decl_stmt* s)
{
  generate(gen, s->get_declaration());
}

void
generate_stmt(codegen& gen, const expr_stmt* s)
{
  generate(gen, s->get_expression());
}


} // namespace llvm
} // namespace beaker
