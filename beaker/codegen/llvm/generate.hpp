#pragma once

#include <beaker/common/symbols.hpp>

#include <stack>
#include <vector>


namespace llvm { 

class LLVMContext;
class Module;
class Function;
class Type;
class Value;
class BasicBlock;

using Context = LLVMContext;

} // namespace llvm

// Rename the llvm namespace to help avoid ambiguities later.
namespace llir = llvm;


namespace beaker {

struct context;
struct type;
struct expr;
struct decl;
struct stmt;

namespace llvm {

struct codegen;
struct generator;
struct module;
struct function;
struct type;
struct value;
struct block;

// Associates Beaker types with their corresponding LLVM types.
using type_map = std::unordered_map<const beaker::type*, llir::Type*>;

/// Associates Beaker declarations with their corresponding LLVM values.
using decl_map = std::unordered_map<const decl*, llir::Value*>;



/// Represents a generated type.
struct type
{
  type() : ty() { }
  type(llir::Type* t) : ty(t) { }

  /// Returns true if the type is inhabited.
  explicit operator bool() const { return ty; }

  /// Allow use of the underlying type through indirection.
  llir::Type* operator->() const { return ty; }

  /// Allow implicit conversion to the underlying type.
  operator llir::Type*() const { return ty; }

  /// Returns the underlying LLVM type.
  llir::Type* get_llvm_type() const { return ty; }

  /// Dump the type to an output stream.
  void dump() const;

  llir::Type* ty;
};


/// Represents a generated value.
struct value
{
  value() : val() { }
  value(llir::Value *v) : val(v) { }
  
  /// Returns true if the value is inhabited.
  explicit operator bool() const { return val; }

  /// Allow use of the underlying value through indirection.
  llir::Value* operator->() const { return val; }

  /// Allow implicit conversion to the underlying value.
  operator llir::Value*() const { return val; }

  /// Returns the underlying LLVM value.
  llir::Value* get_llvm_value() const { return val; }

  /// Dump the value to an output stream.
  void dump() const;

  llir::Value* val;
};


/// Represents a labeled basic block. Note that basic blocks are always 
/// constructed independently from the function in which they are ultimately
/// emitted. Use `emit_block()` to insert the block into a function.
struct block
{
  block() : bb() { }
  block(llir::BasicBlock* bb) : bb(bb) { }
  block(codegen& gen, const char* label);

  /// Returns true if the block is inhabited.
  explicit operator bool() const { return bb; }

  /// Allow use of the underlying block through indirection.
  llir::BasicBlock* operator->() const { return bb; }

  /// Allow implicit conversion to the underlying block.
  operator llir::BasicBlock*() const { return bb; }

  /// Returns the LLVM block.
  llir::BasicBlock* get_llvm_block() const { return bb; }

  /// Returns true if the block has a terminator.
  bool has_terminator() const;

  llir::BasicBlock* bb;
};


/// The base class of all code generation contexts. This provides access to the
/// shared Beaker context, the parent codegen context, and an interface for 
/// determining the current contextg.
struct codegen
{
protected:
  codegen(codegen* p) : parent(p) { }
public:

  virtual ~codegen() = default;

  /// Returns the parent code generation context. Note that the root context
  /// has itself as a parent.
  const codegen& get_parent() const { return *parent; }
  codegen& get_parent() { return *parent; }

  /// Returns true if this is a root codegen context.
  virtual bool is_generator() const { return false; }

  /// Returns true if this is a module context.
  virtual bool is_module() const { return false; }

  /// Returns true if this is a function context.
  virtual bool is_function() const { return false; }

  /// Return this context as the root codegen context.
  generator& as_generator();

  /// Returns this context as a module context.
  module& as_module();

  /// Returns this context as a function context.
  function& as_function();

  codegen* parent;
};



/// The top-level generator for a module. This holds references to both
/// the Beaker and LLVM contexts.
struct generator : codegen
{
  generator(context& cxt);
  ~generator();

  /// Returns true.
  bool is_generator() const override { return true; }

  /// Returns this as the parent.
  const generator& parent() const { return *this; }
  generator& parent() { return *this; }

  /// Returns the Beaker context.
  const context* get_context() const { return src; }

  /// Returns the LLVM context.
  const llir::Context* get_llvm_context() const { return dst; }
  llir::Context* get_llvm_context() { return dst; }

  // Types
  
  /// Returns the generated type corresponding to `t`.
  type lookup_type(const beaker::type* t);

  /// Register a correspondence between `b` and `ll`.
  type bind_type(const beaker::type* b, llir::Type* ll);

  context* src;
  llir::Context* dst;
  type_map types;
};


/// The code generation context for modules.
struct module : codegen
{
  module(generator& gen, const decl* d);

  /// Returns true if the module is inhabited.
  explicit operator bool() const { return tgt; }

  /// Allow use of the underlying module through indirection.
  llir::Module* operator->() const { return tgt; }

  /// Allow implicit conversion to the underlying module.
  operator llir::Module*() const { return tgt; }

  /// Returns true.
  bool is_module() const override { return true; }

  /// Returns the parent codegen context.
  const generator& get_parent() const { return static_cast<const generator&>(*parent); }
  generator& get_parent() { return static_cast<generator&>(*parent); }

  /// Returns the source translation unit.
  const decl* get_translation_unit() const { return src; }

  /// Returns the LLVM module.
  const llir::Module* get_llvm_module() const { return tgt; }
  llir::Module* get_llvm_module() { return tgt; }

  /// Dump the module IR to an ouput stream.
  void dump() const;

  // Gloal variables

  /// Returns the address of the local variable `d`.
  value lookup_global(const decl* d);

  /// Binds the local variable `d` to the address `v`.
  value bind_global(const decl* d, llir::Value* v);

  const decl* src;
  llir::Module* tgt;
  decl_map globals;
};


/// A pair of break/continue labels.
struct bc
{
  block top;
  block bottom;
};

using bc_stack = std::stack<bc>;


/// A code generator for a single function. A function encapsulates its own
/// output stream. This allows functions different functions to be generated
/// simultaneously and then added to their context later.
struct function : codegen
{
  function(module& gen, const decl* fn);

  /// Returns true if the function is inhabited.
  explicit operator bool() const { return tgt; }

  /// Allow use of the underlying function through indirection.
  llir::Function* operator->() const { return tgt; }

  /// Allow implicit conversion to the underlying function.
  operator llir::Function*() const { return tgt; }

  /// Returns true.
  bool is_function() const override { return true; }

  /// Returns the parent function context.
  const module& get_parent() const { return static_cast<const module&>(*parent); }
  module& get_parent() { return static_cast<module&>(*parent); }

  /// Returns the function declaration being generated.
  const decl* get_function() const { return src; }

  /// Returns the LLVM function.
  const llir::Function* get_llvm_function() const { return tgt; }
  llir::Function* get_llvm_function() { return tgt; }

  /// Prepare a function to be defined. This allocates the entry and exit
  /// blocks.
  void start_definition();

  /// Finish the definition of a function. This emits the exit block and 
  /// returns the function's value as needed.
  void finish_definition();

  // Returns the current block.
  block get_current_block(); 

  // Gets the entry block.
  block get_entry_block();

  // Gets the exit block.
  block get_exit_block();

  // Gets the address of the return value.
  value get_return_value() const { return ret; }

  // Sets the address of the return value.
  void set_return_value(value v) { ret = v; }

  // Local variables

  /// Returns the address of the local variable `d`.
  value lookup_local(const decl* d);

  /// Binds the local variable `d` to the address `v`.
  value bind_local(const decl* d, llir::Value* v);

  // Break and continue

  /// Indicate that we have entered a loop whose top and bottom labels
  /// are given by `t` and `b`.
  void enter_loop(block t, block b) { bcs.push({t, b}); }

  /// Indicate that we have finished translating a loop.
  void leave_loop() { bcs.pop(); }

  /// Returns the block at the top of the current loop.
  block get_loop_top() { return bcs.top().top; }

  /// Returns the block at the bottom of the current loop.
  block get_loop_bottom() { return bcs.top().bottom; }

  const decl* src;
  llir::Function* tgt;
  llir::BasicBlock* entry;
  llir::BasicBlock* exit;
  llir::Value* ret;
  decl_map locals;
  bc_stack bcs;
};



// -------------------------------------------------------------------------- //
// Operations

/// Returns the root code generator.
generator& get_generator(codegen& gen);

/// Returns the current module code generation context for `gen`.
module& get_module_codegen(codegen& gen);

/// Returns the current function code generation context for `gen`.
function& get_function_codegen(codegen& gen);

//// Returns the current block.
block& get_block_codegen(codegen& gen);

/// Returns the most recently emitted block of the current function.
inline block
get_current_block(codegen& gen)
{
  return get_function_codegen(gen).get_current_block();
}

/// Returns the entry block of the current function.
inline block
get_entry_block(codegen& gen)
{
  return get_function_codegen(gen).get_entry_block();
}

/// Returns thee xit 
inline block
get_exit_block(codegen& gen)
{
  return get_function_codegen(gen).get_exit_block();
}


/// Returns the LLVM IR context.
inline llir::Context* 
get_llvm_context(codegen& gen)
{
  return get_generator(gen).get_llvm_context();
}

/// Returns the current LLVM module for `gen`.
inline llir::Module* 
get_llvm_module(codegen& gen)
{
  return get_module_codegen(gen).get_llvm_module();
}

/// Returns the current LLVM function for `gen.`
inline llir::Function*
get_llvm_function(codegen& gen)
{
  return get_function_codegen(gen).get_llvm_function();
}


// -------------------------------------------------------------------------- //
// Code generation

/// Generate a mangled declaration name `n`.
std::string generate(codegen& gen, const symbol* n);

/// Generate an LLVM type for `t`.
type generate(codegen& gen, const beaker::type* t);

/// Generate an LLVM type for the value type `t` such that the resulting
/// type can be used as a function parameter, a global variable, or the
/// argument of an alloca instruction. This typically means constructing
/// a pointer type of some kind.
///
/// Some Beaker types have multiple interpretations depending on their use. 
/// In particular function types represent both the types of functions and
/// the values of their addresses. To generate parameters and variables of
/// function type, we need to generate function pointers.
type generate_value_type(codegen& gen, const beaker::type* t);

/// Generate an LLVM value computation `e`.
value generate(codegen& gen, const expr* e);

/// Generate a corresponding declaration for `d` in the current context.
void generate(codegen& gen, const decl* d);

/// Generate a computation for the sequence `s`.
void generate(codegen& gen, const stmt* s);

/// Represents the (possible) allocation of a declared value. This may or
/// may not correspond to storage.
struct object
{
  object() : val() { }
  object(value val) : val(val) { }

  explicit operator bool() const { return (bool)val; }

  /// Returns the pointer to the allocated object.
  value get_pointer() const { return val; }

  value val;
};

/// Generate the initialization of an object by an expression `e`.
value initialize(codegen& gen, object& obj, const expr* e);

// -------------------------------------------------------------------------- //
// Dispatch

/// This function throws an exception. It is intended to be overloaded by
/// any type for which a translation is defined.
type generate_type(codegen& gen, const beaker::type* t);

/// This function throws an exception. It is intended to be overloaded by
/// any expression for which a translation is defined.
value generate_expr(codegen& gen, const expr* e);

/// This function throws an exception. It is intended to be overloaded by
/// any declaration for which a translation is defined.
void generate_decl(codegen& gen, const decl* d);

/// This function throws an exception. It is intended to be overloaded by
/// any statement for which a translation is defined.
void generate_stmt(codegen& gen, const stmt* s);

/// This function throws an exception.
value generate_init(codegen& gen, object& obj, const expr*);


// -------------------------------------------------------------------------- //
// Types

/// Returns the void type.
type emit_void_type(codegen& gen);

/// Returns an integer type of the corresponding bit width.
type emit_int1_type(codegen& gen);
type emit_int8_type(codegen& gen);
type emit_int16_type(codegen& gen);
type emit_int32_type(codegen& gen);
type emit_int64_type(codegen& gen);

/// Returns the type `t*`.
type emit_pointer_type(codegen& gen, type t);

/// Returns the function type `t()`.
type emit_function_type(codegen& gen, type t);

/// Returns the function type `t(t1, t2, ..., tn).
type emit_function_type(codegen& gen, type t, std::vector<type> ts);

/// Emits the type `[t x n]`.
type emit_array_type(codegen& gen, type t, int n);


// -------------------------------------------------------------------------- //
// Constants

/// Emit the bool value `b`. This implicitly has type `i1`.
value emit_bool(codegen& gen, bool b);

inline value
emit_true(codegen& gen)
{
  return emit_bool(gen, true);
}

inline value
emit_false(codegen& gen)
{
  return emit_bool(gen, false);
}

/// Emit the integer value `n` with type `t`.
value emit_sint(codegen& gen, type t, std::intmax_t n);
value emit_uint(codegen& gen, type t, std::uintmax_t n);

/// Emits a null value for the given type. This is used for zero initialization
/// of scalars and aggregates.
value emit_null(codegen& gen, type t);


// -------------------------------------------------------------------------- //
// Instructions
//
// The following functions create new LLVM instructions in the context given.
// The context must either be a block, in which case the instruction appended
// to the block, a function, in which case the instruction is appended to
// the function's current block.

// Signed integer arithmetic
// These operations have signed integer overflow.

value emit_sadd(block bb, value lhs, value rhs);
value emit_ssub(block bb, value lhs, value rhs);
value emit_smul(block bb, value lhs, value rhs);
value emit_sdiv(block bb, value lhs, value rhs);
value emit_srem(block bb, value lhs, value rhs);

inline value
emit_sadd(function& fn, value lhs, value rhs)
{
  return emit_sadd(fn.get_current_block(), lhs, rhs);
}

inline value
emit_sadd(codegen& gen, value lhs, value rhs)
{
  return emit_sadd(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_ssub(function& fn, value lhs, value rhs)
{
  return emit_ssub(fn.get_current_block(), lhs, rhs);
}

inline value
emit_ssub(codegen& gen, value lhs, value rhs)
{
  return emit_ssub(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_smul(function& fn, value lhs, value rhs)
{
  return emit_smul(fn.get_current_block(), lhs, rhs);
}

inline value
emit_smul(codegen& gen, value lhs, value rhs)
{
  return emit_smul(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_sdiv(function& fn, value lhs, value rhs)
{
  return emit_sdiv(fn.get_current_block(), lhs, rhs);
}

inline value
emit_sdiv(codegen& gen, value lhs, value rhs)
{
  return emit_sdiv(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_srem(function& fn, value lhs, value rhs)
{
  return emit_srem(fn.get_current_block(), lhs, rhs);
}

inline value
emit_srem(codegen& gen, value lhs, value rhs)
{
  return emit_srem(get_function_codegen(gen), lhs, rhs);
}

// Unsigned integer arithmetic
// These operations have unsigned integer overflow.

value emit_uadd(block bb, value lhs, value rhs);
value emit_usub(block bb, value lhs, value rhs);
value emit_umul(block bb, value lhs, value rhs);
value emit_udiv(block bb, value lhs, value rhs);
value emit_urem(block bb, value lhs, value rhs);

inline value
emit_uadd(function& fn, value lhs, value rhs)
{
  return emit_uadd(fn.get_current_block(), lhs, rhs);
}

inline value
emit_uadd(codegen& gen, value lhs, value rhs)
{
  return emit_uadd(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_usub(function& fn, value lhs, value rhs)
{
  return emit_usub(fn.get_current_block(), lhs, rhs);
}

inline value
emit_usub(codegen& gen, value lhs, value rhs)
{
  return emit_usub(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_umul(function& fn, value lhs, value rhs)
{
  return emit_umul(fn.get_current_block(), lhs, rhs);
}

inline value
emit_umul(codegen& gen, value lhs, value rhs)
{
  return emit_umul(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_udiv(function& fn, value lhs, value rhs)
{
  return emit_udiv(fn.get_current_block(), lhs, rhs);
}

inline value
emit_udiv(codegen& gen, value lhs, value rhs)
{
  return emit_udiv(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_urem(function& fn, value lhs, value rhs)
{
  return emit_urem(fn.get_current_block(), lhs, rhs);
}

inline value
emit_urem(codegen& gen, value lhs, value rhs)
{
  return emit_urem(get_function_codegen(gen), lhs, rhs);
}

// Bitwise operations

value emit_and(block bb, value lhs, value rhs);
value emit_or(block bb, value lhs, value rhs);
value emit_xor(block bb, value lhs, value rhs);
value emit_lsh(block bb, value lhs, value rhs);
value emit_lrsh(block bb, value lhs, value rhs);
value emit_arsh(block bb, value lhs, value rhs);

inline value
emit_and(function& fn, value lhs, value rhs)
{
  return emit_and(fn.get_current_block(), lhs, rhs);
}

inline value
emit_and(codegen& gen, value lhs, value rhs)
{
  return emit_and(get_function_codegen(gen), lhs, rhs);
}


inline value
emit_or(function& fn, value lhs, value rhs)
{
  return emit_or(fn.get_current_block(), lhs, rhs);
}

inline value
emit_or(codegen& gen, value lhs, value rhs)
{
  return emit_or(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_xor(function& fn, value lhs, value rhs)
{
  return emit_xor(fn.get_current_block(), lhs, rhs);
}

inline value
emit_xor(codegen& gen, value lhs, value rhs)
{
  return emit_xor(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_lsh(function& fn, value lhs, value rhs)
{
  return emit_lsh(fn.get_current_block(), lhs, rhs);
}

inline value
emit_lsh(codegen& gen, value lhs, value rhs)
{
  return emit_lsh(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_lrsh(function& fn, value lhs, value rhs)
{
  return emit_lrsh(fn.get_current_block(), lhs, rhs);
}

inline value
emit_lrsh(codegen& gen, value lhs, value rhs)
{
  return emit_lrsh(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_arsh(function& fn, value lhs, value rhs)
{
  return emit_arsh(fn.get_current_block(), lhs, rhs);
}

inline value
emit_arsh(codegen& gen, value lhs, value rhs)
{
  return emit_arsh(get_function_codegen(gen), lhs, rhs);
}


// Integer equality

value emit_ieq(block bb, value lhs, value rhs);
value emit_ine(block bb, value lhs, value rhs);

inline value
emit_ieq(function& fn, value lhs, value rhs)
{
  return emit_ieq(fn.get_current_block(), lhs, rhs);
}

inline value
emit_ieq(codegen& gen, value lhs, value rhs)
{
  return emit_ieq(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_ine(function& fn, value lhs, value rhs)
{
  return emit_ine(fn.get_current_block(), lhs, rhs);
}

inline value
emit_ine(codegen& gen, value lhs, value rhs)
{
  return emit_ine(get_function_codegen(gen), lhs, rhs);
}

// Signed integer ordering

value emit_slt(block bb, value lhs, value rhs);
value emit_sgt(block bb, value lhs, value rhs);
value emit_sle(block bb, value lhs, value rhs);
value emit_sge(block bb, value lhs, value rhs);

inline value
emit_slt(function& fn, value lhs, value rhs)
{
  return emit_slt(fn.get_current_block(), lhs, rhs);
}

inline value
emit_slt(codegen& gen, value lhs, value rhs)
{
  return emit_slt(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_sgt(function& fn, value lhs, value rhs)
{
  return emit_sgt(fn.get_current_block(), lhs, rhs);
}

inline value
emit_sgt(codegen& gen, value lhs, value rhs)
{
  return emit_sgt(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_sle(function& fn, value lhs, value rhs)
{
  return emit_sle(fn.get_current_block(), lhs, rhs);
}

inline value
emit_sle(codegen& gen, value lhs, value rhs)
{
  return emit_sle(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_sge(function& fn, value lhs, value rhs)
{
  return emit_sge(fn.get_current_block(), lhs, rhs);
}

inline value
emit_sge(codegen& gen, value lhs, value rhs)
{
  return emit_sge(get_function_codegen(gen), lhs, rhs);
}

// Unsigned integer ordering

value emit_ult(block bb, value lhs, value rhs);
value emit_ugt(block bb, value lhs, value rhs);
value emit_ule(block bb, value lhs, value rhs);
value emit_uge(block bb, value lhs, value rhs);

inline value
emit_ult(function& fn, value lhs, value rhs)
{
  return emit_ult(fn.get_current_block(), lhs, rhs);
}

inline value
emit_ult(codegen& gen, value lhs, value rhs)
{
  return emit_ult(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_ugt(function& fn, value lhs, value rhs)
{
  return emit_ugt(fn.get_current_block(), lhs, rhs);
}

inline value
emit_ugt(codegen& gen, value lhs, value rhs)
{
  return emit_ugt(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_ule(function& fn, value lhs, value rhs)
{
  return emit_ule(fn.get_current_block(), lhs, rhs);
}

inline value
emit_ule(codegen& gen, value lhs, value rhs)
{
  return emit_ule(get_function_codegen(gen), lhs, rhs);
}

inline value
emit_uge(function& fn, value lhs, value rhs)
{
  return emit_uge(fn.get_current_block(), lhs, rhs);
}

inline value
emit_uge(codegen& gen, value lhs, value rhs)
{
  return emit_uge(get_function_codegen(gen), lhs, rhs);
}

/// Emits a call to the nullary function `tgt` in `bb`.
value emit_call(block bb, value tgt);

/// Emits a call of `tgt` with the arguments `args` in `bb`.
value emit_call(block bb, value tgt, const std::vector<value>& args);

/// Emits a call to the nullary function `tgt` in the current block of `fn`.
inline value
emit_call(function& fn, value tgt)
{
  return emit_call(fn.get_current_block(), tgt);
}

/// Emits a call to the nullary function `tgt` in the current block.
inline value
emit_call(codegen& gen, value tgt)
{
  return emit_call(get_function_codegen(gen), tgt);
}

/// Emits a call to `tgt` in the current block of `fn`.
inline value
emit_call(function& fn, value tgt, const std::vector<value>& args)
{
  return emit_call(fn.get_current_block(), tgt, args);
}

/// Emits a call to `tgt` in the current block.
inline value
emit_call(codegen& gen, value tgt, const std::vector<value>& args)
{
  return emit_call(get_function_codegen(gen), tgt, args);
}

// Storage

/// Appends a load of the `ptr` to the block `bb`.
value emit_load(block bb, value ptr);

/// Appends a load of the `ptr` to the current block.
inline value
emit_load(function& fn, value ptr)
{
  return emit_load(fn.get_current_block(), ptr);
}

/// Appends a load of the `ptr` to the current block.
inline value
emit_load(codegen& gen, value ptr)
{
  return emit_load(get_function_codegen(gen), ptr);
}

/// Appends of store of `val` to `ptr` to the block `bb`.
value emit_store(block bb, value ptr, value val);

/// Appends of store of `val` to `ptr` to the current block.
inline value
emit_store(function& fn, value ptr, value val)
{
  return emit_store(fn.get_current_block(), ptr, val);
}

/// Appends of store of `val` to `ptr` to the current block.
inline value
emit_store(codegen& gen, value ptr, value val)
{
  return emit_store(get_function_codegen(gen), ptr, val);
}

/// Appends an alloca for one object of type `t` to the block `bb`.
value emit_alloca(block bb, type t);
value emit_alloca(block bb, type t, const char*);

/// Appends an alloca for one object of type `t` to the current block.
inline value
emit_alloca(function& fn, type t)
{
  return emit_alloca(fn.get_current_block(), t);
}

/// Appends an alloca for one object of type `t` to the current block.
inline value
emit_alloca(codegen& gen, type t)
{
  return emit_alloca(get_function_codegen(gen), t);
}

/// Appends an alloca for one object of type `t` to the current block. The
/// name of the value is `n`.
inline value
emit_alloca(function& fn, type t, const char* n)
{
  return emit_alloca(fn.get_current_block(), t, n);
}

/// Appends an alloca for one object of type `t` to the current block.
inline value
emit_alloca(codegen& gen, type t, const char* n)
{
  return emit_alloca(get_function_codegen(gen), t, n);
}

// Control flow

/// Emits a conditional branch in `bb`.
value emit_branch(block bb, value c, block t, block f);

/// Emits an unconditional branch in `bb`.
value emit_branch(block bb, block b);

/// Emits a conditional branch in the current block.
inline value
emit_branch(function& fn, value c, block t, block f)
{
  return emit_branch(fn.get_current_block(), c, t, f);
}

inline value
emit_branch(codegen& gen, value c, block t, block f)
{
  return emit_branch(get_function_codegen(gen), c, t, f);
}

/// Emits an unconditional branch in the current block.
inline value
emit_branch(function& fn, block b)
{
  return emit_branch(fn.get_current_block(), b);
}

inline value
emit_branch(codegen& gen, block b)
{
  return emit_branch(get_function_codegen(gen), b);
}

/// Represents an incoming edge to a phi node.
using incoming = std::pair<value, block>;

/// Emits a phi node in `bb`.
value emit_phi(block bb, type t, std::initializer_list<incoming> ins);

inline value
emit_phi(function& fn, type t, std::initializer_list<incoming> ins)
{
  return emit_phi(fn.get_current_block(), t, ins);
}

inline value
emit_phi(codegen& gen, type t, std::initializer_list<incoming> ins)
{
  return emit_phi(get_function_codegen(gen), t, ins);
}

/// Emits a return with no value in `bb`.
value emit_ret(block bb);

/// Emits a return of `val` in `bb`.
value emit_ret(block bb, value val);

/// Emits a return with no value in the current block.
inline value
emit_ret(function& fn)
{
  return emit_ret(fn.get_current_block());
}

inline value
emit_ret(codegen& gen)
{
  return emit_ret(get_function_codegen(gen));
}

/// Emits a return of `val` in the current blokc.
inline value
emit_ret(function& fn, value val)
{
  return emit_ret(fn.get_current_block(), val);
}

inline value
emit_ret(codegen& gen, value val)
{
  return emit_ret(get_function_codegen(gen), val);
}

/// Returns a new unreachable teminator.
value emit_unreachable(block bb);

inline value
emit_unreachable(function& fn)
{
  return emit_unreachable(fn.get_current_block());
}

inline value
emit_unreachable(codegen& gen)
{
  return emit_unreachable(get_function_codegen(gen));
}

// Basic blocks

/// Emit a block into the function `fn`. This makes `b` the current block.
void emit_block(function& fn, block b);

/// Emit a block into the current codegen context.
inline void 
emit_block(codegen& gen, block b)
{
  emit_block(get_function_codegen(gen), b);
}

// Local variables

/// Emit the allocation of a local variable.
object emit_local(function& fn, const decl* d, const char* n);
object emit_local(function& fn, const decl* d, const std::string& n);
object emit_local(function& fn, const decl* d);


// Intrinsics

/// Returns the trap intrinsic. If `debug` is true, then this returns the
/// debug trap.
value get_trap_intrinsic(module& gen, bool debug);

inline value 
get_trap_intrinsic(codegen& gen, bool debug)
{
  return get_trap_intrinsic(get_module_codegen(gen), debug);
}


} // namespace llvm
} // namespace beaker
