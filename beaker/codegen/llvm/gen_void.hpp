#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.void/types_void.hpp>
#include <beaker/ast/lang.void/exprs_void.hpp>


namespace beaker {
namespace llvm {

type generate_type(codegen& gen, const lang_void::void_type* t);

value generate_expr(codegen& gen, const lang_void::nop_expr* e);
value generate_expr(codegen& gen, const lang_void::void_expr* e);
value generate_expr(codegen& gen, const lang_void::trap_expr* e);

} // namespace llvm
} // namespace beaker
