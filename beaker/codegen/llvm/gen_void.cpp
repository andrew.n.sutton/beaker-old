#include "gen_void.hpp"


namespace beaker {
namespace llvm {

using namespace lang_void;

type
generate_type(codegen& gen, const void_type* t)
{
  return emit_void_type(gen);
}


value 
generate_expr(codegen& gen, const lang_void::nop_expr* e)
{
  return {};
}

value 
generate_expr(codegen& gen, const lang_void::void_expr* e)
{
  generate(gen, e->get_operand());
  return {};
}

/// \todo Emit an intrinsic trap instruction.
value 
generate_expr(codegen& gen, const lang_void::trap_expr* e)
{
  throw std::logic_error("trap not implemented");
}


} // namespace llvm
} // namespace beaker
