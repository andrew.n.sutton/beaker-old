#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.fn/types_fn.hpp>
#include <beaker/ast/lang.fn/exprs_fn.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>
#include <beaker/ast/lang.fn/stmts_fn.hpp>


namespace beaker {
namespace llvm {

type generate_type(codegen& gen, const lang_fn::fn_type* t);

value generate_expr(codegen& gen, const lang_fn::call_expr* e);
value generate_expr(codegen& gen, const lang_fn::eq_expr* e);
value generate_expr(codegen& gen, const lang_fn::ne_expr* e);

void generate_decl(codegen& gen, const lang_fn::fn_decl* d);
void generate_decl(codegen& gen, const lang_fn::var_decl* d);
void generate_decl(codegen& gen, const lang_fn::let_decl* d);

void generate_stmt(codegen& gen, const lang_fn::block_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::if_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::while_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::break_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::cont_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::ret_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::decl_stmt* s);
void generate_stmt(codegen& gen, const lang_fn::expr_stmt* s);

} // nmamespace llvm
} // namespace beaker
