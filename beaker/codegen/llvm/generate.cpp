#include "generate.hpp"
#include "gen_tu.hpp"
#include "gen_void.hpp"
#include "gen_bool.hpp"
#include "gen_int.hpp"
#include "gen_store.hpp"
#include "gen_fn.hpp"
#include "gen_io.hpp"
#include "gen_array.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>
#include <sstream>

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Intrinsics.h>
#include <llvm/IR/IRBuilder.h>


namespace beaker {
namespace llvm {

generator&
codegen::as_generator()
{
  return dynamic_cast<generator&>(*this);
}

module&
codegen::as_module()
{
  return dynamic_cast<module&>(*this);
}

function&
codegen::as_function()
{
  return dynamic_cast<function&>(*this);
}

void
type::dump() const
{
  ty->dump();
}

void
value::dump() const
{
  val->dump();
}

block::block(codegen& gen, const char* label)
  : bb(llir::BasicBlock::Create(*get_llvm_context(gen), label))
{ }

bool
block::has_terminator() const
{
  return bb->getTerminator();
}

generator::generator(context& cxt)
  : codegen(this), src(&cxt), dst(new llir::Context())
{ }

generator::~generator()
{
  delete dst;
}

type
generator::lookup_type(const beaker::type* t)
{
  auto iter = types.find(t);
  if (iter != types.end())
    return iter->second;
  else
    return {};
}

type 
generator::bind_type(const beaker::type* b, llir::Type* ll)
{
  assert(types.count(b) == 0);
  return types.emplace(b, ll).first->second;
}

/// \todo Actually give this module a meaningful name.
module::module(generator& gen, const decl* d) 
  : codegen(&gen), src(d), tgt(new llir::Module("a.bkm", *get_llvm_context(gen)))
{ }

void
module::dump() const
{
  tgt->dump();
}

value
module::lookup_global(const decl* d)
{
  assert(globals.count(d) == 1 && "global name not declared");
  return globals.find(d)->second;
}

value 
module::bind_global(const decl* d, llir::Value* v)
{
  assert(globals.count(d) == 0);
  return globals.emplace(d, v).first->second;
}

static llir::Function*
make_extern_function(module& gen, type t, const std::string& n)
{
  llir::Module* mod = gen.get_llvm_module();
  llir::FunctionType* ty = static_cast<llir::FunctionType*>(t.get_llvm_type());
  llir::Function::LinkageTypes link = llir::Function::ExternalLinkage;
  return llir::Function::Create(ty, link, n, mod);
}

static llir::Function*
make_extern_function(module& gen, const decl* d)
{
  type t = generate(gen, d->get_type());
  std::string n = generate(gen, d->get_name());
  return make_extern_function(gen, t, n);
}

/// \todo What if we want a non-external function? 
function::function(module& gen, const decl* d)
  : codegen(&gen), src(d), tgt(make_extern_function(gen, d)), entry(), exit(), ret()
{ }

void
function::start_definition()
{
  llir::Context* cxt = get_llvm_context(*this);
  entry = llir::BasicBlock::Create(*cxt, "entry", tgt);
  exit = llir::BasicBlock::Create(*cxt, "exit");
}

void
function::finish_definition()
{
  block bb = get_exit_block();

  // Emit the exit block.
  emit_block(*this, bb);

  if (ret) {
    value ptr = emit_load(bb, ret);
    emit_ret(bb, ptr);
  }
  else {
    emit_ret(bb);
  }
}

block
function::get_current_block()
{
  return block(&tgt->back());
}

block
function::get_entry_block()
{
  return block(entry);
}

block
function::get_exit_block()
{
  return block(exit);
}

value
function::lookup_local(const decl* d)
{
  assert(locals.count(d) == 1 && "local not declared");
  return locals.find(d)->second;
}

value
function::bind_local(const decl* d, llir::Value* v)
{
  assert(locals.count(d) == 0 && "local already declared");
  return locals.emplace(d, v).first->second;
}

// -------------------------------------------------------------------------- //
// Operations

/// Returns the root code generator.
generator& 
get_generator(codegen& gen)
{
  if (gen.is_generator())
    return gen.as_generator();
  else 
    return get_module_codegen(gen).get_parent();
}

/// Returns the current module code generation context for `gen`.
module& 
get_module_codegen(codegen& gen)
{
  if (auto* mod = dynamic_cast<module*>(&gen))
    return *mod;
  else
    return get_function_codegen(gen).get_parent();
}

function& 
get_function_codegen(codegen& gen)
{
  if (auto* fn = dynamic_cast<function*>(&gen))
    return *fn;
  throw std::logic_error("invalid codegen context\n");
}


// -------------------------------------------------------------------------- //
// Code generation

/// \todo Mangle the symbol according to the ABI.
std::string
generate(codegen& gen, const symbol* sym)
{
  return *sym;
}

/// Dispatches to generate_type(gen, t).
type
generate(codegen& gen, const beaker::type* t)
{
  struct fn {
    type operator()(const beaker::type*) { throw std::logic_error("unreachable"); }
#define def_type(T, NS) \
    llvm::type operator()(const beaker::NS::T ## _type* t) { return generate_type(gen, t); }
#include <beaker/ast/types.def>
#undef def_type

    codegen& gen;
  };

  generator& root = get_generator(gen);

  // If we've already seen the type, return that. Otherwise, compute and 
  // bind the type.
  if (type ret = root.lookup_type(t))
    return ret;
  type ret = apply(t, fn{gen});
    // return root.bind_type(t, apply(t, fn{gen}));
  return ret;
}

type
generate_value_type(codegen& gen, const beaker::type* t)
{
  type ty = generate(gen, t);
  if (ty->isFunctionTy())
    ty = ty->getPointerTo();
  return ty;
}

/// Dispatches to generate_expr(cg, e).
value
generate(codegen& gen, const expr* e)
{
  struct fn {
    value operator()(const expr*) { throw std::logic_error("unreachable"); }
#define def_init(E, NS)
#define def_expr(E, NS) \
    value operator()(const NS::E ## _expr* e) { return generate_expr(gen, e); }
#include <beaker/ast/exprs.def>
#undef def_expr
#undef def_init

    codegen& gen;
  };
  return apply(e, fn{gen});
}

/// Dispatches to generate_decl(gen, d).
void
generate(codegen& gen, const decl* d)
{
  struct fn {
    void operator()(const decl*) { throw std::logic_error("unreachable"); }
#define def_decl(T, NS) \
    void operator()(const NS::T ## _decl* d) { generate_decl(gen, d); }
#include <beaker/ast/decls.def>
#undef def_decl

    codegen& gen;
  };
  apply(d, fn{gen});
}

void
generate(codegen& gen, const stmt* s)
{
  // Dispatches to generate_stmt(gen, s).
  struct fn {
    void operator()(const stmt*) { throw std::logic_error("unreachable"); }
#define def_stmt(T, NS) \
    void operator()(const NS::T ## _stmt* s) { return generate_stmt(gen, s); }
#include <beaker/ast/stmts.def>
#undef def_stmt

    codegen& gen;
  };
  apply(s, fn{gen});
}

type
generate_type(codegen&, const beaker::type*)
{
  throw std::logic_error("undefined codegen behavior");
}

value
generate_expr(codegen&, const expr* e)
{
  throw std::logic_error("undefined codegen behavior");
}

void
generate_decl(codegen&, const decl*)
{
  throw std::logic_error("undefined codegen behavior");
}

void
generate_stmt(codegen&, const stmt* s)
{
  throw std::logic_error("undefined codegen behavior");
}

/// Dispatches to generate_init(gen, e).
value
initialize(codegen& gen, object& obj, const expr* e)
{
  struct fn {
    value operator()(const expr*) { throw std::logic_error("unreachable"); }
#define def_expr(E, NS)
#define def_init(I, NS) \
    value operator()(const NS::I ## _init* e) { return generate_init(gen, obj, e); }
#include <beaker/ast/exprs.def>
#undef def_init
#undef def_expr

    codegen& gen;
    object& obj;
  };
  return apply(e, fn{gen, obj});
}

value
generate_init(codegen& gen, object& obj, const expr* e)
{
  throw std::logic_error("undefined codegen behavior");
}

// -------------------------------------------------------------------------- //
// Types

type 
emit_void_type(codegen& gen)
{
  return llir::Type::getVoidTy(*get_llvm_context(gen));
}

type 
emit_int1_type(codegen& gen)
{
  return llir::Type::getInt1Ty(*get_llvm_context(gen));
}

type 
emit_int8_type(codegen& gen)
{
  return llir::Type::getInt8Ty(*get_llvm_context(gen));
}

type 
emit_int16_type(codegen& gen)
{
  return llir::Type::getInt16Ty(*get_llvm_context(gen));
}

type 
emit_int32_type(codegen& gen)
{
  return llir::Type::getInt32Ty(*get_llvm_context(gen));
}

type 
emit_int64_type(codegen& gen)
{
  return llir::Type::getInt64Ty(*get_llvm_context(gen));
}

type 
emit_pointer_type(codegen& gen, type t)
{
  return t->getPointerTo();
}

type
emit_function_type(codegen& gen, type t)
{
  return llir::FunctionType::get(t, false);
}

type 
emit_function_type(codegen& gen, type t, std::vector<type> ts)
{
  std::vector<llir::Type*> parms(ts.size());
  std::transform(ts.begin(), ts.end(), parms.begin(), [](type t) { 
    return t.get_llvm_type();
  });
  return llir::FunctionType::get(t, parms, false);
}

type
emit_array_type(codegen& gen, type t, int n)
{
  return llir::ArrayType::get(t, n);
}


// -------------------------------------------------------------------------- //
// Constants

value 
emit_bool(codegen& gen, bool b)
{
  return llir::ConstantInt::get(emit_int1_type(gen), b);
}

value 
emit_sint(codegen& gen, type t, std::intmax_t n)
{
  return llir::ConstantInt::getSigned(t, n);
}

value 
emit_uint(codegen& gen, type t, std::uintmax_t n)
{
  return llir::ConstantInt::get(t, n);
}

value
emit_null(codegen& gen, type t)
{
  return llir::Constant::getNullValue(t);
}


// -------------------------------------------------------------------------- //
// Instructions

/// Returns an LLVM IR builder for the given generation context. The insertion
/// point is the end of the block.
///
/// Also, potentially adjust the current block so that we don't add 
/// instructions past a terminator. 
static llir::IRBuilder<>
get_builder(block& bb)
{
  if (bb->getTerminator()) {
    // Insert a new basic block into the function and update the input block.
    //
    // FIXME: WE could conceivably reposition the new block between existing
    // blocks if we were careful. For now, it's not an issue.
    llir::Context& cxt = bb->getContext();
    llir::Function* fn = bb->getParent();
    assert(bb.bb == &fn->back() && "splitting a block with a successor");
    llir::BasicBlock* newb = llir::BasicBlock::Create(cxt, "", fn);
    bb.bb = newb;
  }
  return llir::IRBuilder<>(bb);
}

value
emit_sadd(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNSWAdd(lhs, rhs);
}

value
emit_ssub(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNSWSub(lhs, rhs);
}

value
emit_smul(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNSWMul(lhs, rhs);
}

value
emit_sdiv(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNSWAdd(lhs, rhs);
}

value
emit_srem(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateSRem(lhs, rhs);
}

value
emit_uadd(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNUWAdd(lhs, rhs);
}

value
emit_usub(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNUWSub(lhs, rhs);
}

value
emit_umul(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateNUWMul(lhs, rhs);
}

value
emit_udiv(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateUDiv(lhs, rhs);
}

value
emit_urem(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateURem(lhs, rhs);
}

value
emit_and(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateAnd(lhs, rhs);
}

value
emit_or(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateOr(lhs, rhs);
}

value
emit_xor(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateXor(lhs, rhs);
}

value
emit_lsh(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateShl(lhs, rhs);
}

value
emit_lrsh(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateLShr(lhs, rhs);
}

value
emit_arsh(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateAShr(lhs, rhs);
}

value
emit_ieq(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpEQ(lhs, rhs);
}

value
emit_ine(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpNE(lhs, rhs);
}

value
emit_slt(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpSLT(lhs, rhs);
}

value
emit_sgt(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpSGT(lhs, rhs);
}

value
emit_sle(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpSLE(lhs, rhs);
}

value
emit_sge(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpSGE(lhs, rhs);
}

value
emit_ult(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpULT(lhs, rhs);
}

value
emit_ugt(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpUGT(lhs, rhs);
}

value
emit_ule(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpULE(lhs, rhs);
}

value
emit_uge(block bb, value lhs, value rhs)
{
  auto ir = get_builder(bb);
  return ir.CreateICmpUGE(lhs, rhs);
}

value
emit_load(block bb, value ptr)
{
  auto ir = get_builder(bb);
  return ir.CreateLoad(ptr);
}

value
emit_store(block bb, value ptr, value val)
{
  auto ir = get_builder(bb);
  return ir.CreateStore(val, ptr);
}

value
emit_alloca(block bb, type t)
{
  assert(!t->isVoidTy() && "allocation of void type");
  assert(!t->isFunctionTy() && "allocation of function type");
  auto ir = get_builder(bb);
  return ir.CreateAlloca(t);
}

value
emit_alloca(block bb, type t, const char* n)
{
  auto ir = get_builder(bb);
  return ir.CreateAlloca(t, nullptr, n);
}

value
emit_branch(block bb, value c, block t, block f)
{
  auto ir = get_builder(bb);
  return ir.CreateCondBr(c, t, f);
}

value
emit_branch(block bb, block b)
{
  auto ir = get_builder(bb);
  return ir.CreateBr(b);
}

value
emit_phi(block bb, type t, std::initializer_list<incoming> ins)
{
  auto ir = get_builder(bb);
  llir::PHINode* phi = ir.CreatePHI(t, ins.size());
  for (const incoming& in : ins)
    phi->addIncoming(in.first, in.second);
  return phi;
}

value
emit_ret(block bb)
{
  auto ir = get_builder(bb);
  return ir.CreateRetVoid();
}

value
emit_ret(block bb, value val)
{
  auto ir = get_builder(bb);
  return ir.CreateRet(val);
}

value
emit_call(block bb, value fn)
{
  auto ir = get_builder(bb);
  return ir.CreateCall(fn);
}

value
emit_call(block bb, value fn, const std::vector<value>& args)
{
  std::vector<llir::Value*> vals(args.size());
  std::transform(args.begin(), args.end(), vals.begin(), [](value v) {
    return v.get_llvm_value();
  });
  auto ir = get_builder(bb);
  return ir.CreateCall(fn, vals);
}

value
emit_unreachable(block bb)
{
  auto ir = get_builder(bb);
  return ir.CreateUnreachable();
}

/// Add the given block to the function. This guarantees that the previous
/// block flows (correctly) into this block, if needed.
void 
emit_block(function& gen, block b)
{
  // If the last block did not have a terminator, emit a jump to the block
  // being inserted.
  block last = gen.get_current_block();
  if (!last->getTerminator())
    emit_branch(last, b);

  // Make this the current block.
  b->insertInto(gen);
}

/// Emit the allocation of a local variable of object type `t`. This inserts 
/// the alloca instruction at the front of the entry block. Effectively,
/// variables are allocated in the reverse order of their appearance.
///
/// If `t` is not an object type, then this returns an uninhabited object.
///
/// The name of the value associated with the variable is `n`. This is 
/// primarily used to improve readability of generated code.
///
/// If a name `n` is given, then it is the name of the generated value. 
/// This is primarily used to improve readability of generated code. If not,
/// the name is computed from the declaration.
object
emit_local(function& fn, const decl* d, const char* n)
{
  assert(n != nullptr && "null value name");

  if (!is_object(d->get_type()))
    // Don't allocate memory for non-objects.
    return object();

  // Get a value type for the declaration. 
  type ty = generate_value_type(fn, d->get_type());

  // Generate the allocation at the front of the entry block.
  llir::BasicBlock* bb = &fn->getEntryBlock();
  llir::IRBuilder<> ir(bb, bb->begin());
  llir::Value* ptr = ir.CreateAlloca(ty.get_llvm_type(), nullptr, n);

  // Bind the declaration to its address.
  fn.bind_local(d, ptr);
  
  return object(ptr);
}

object
emit_local(function& fn, const decl* d, const std::string& n)
{
  return emit_local(fn, d, n.c_str());
}

object
emit_local(function& fn, const decl* d)
{
  assert(has_no_linkage(d) && "generating a local with external linkage");
  if (const symbol* n = d->get_name())
    return emit_local(fn, d, n->c_str());
  else
    return emit_local(fn, d, "");
}

value
get_trap_intrinsic(module& mod, bool debug)
{
  auto id = debug ? llir::Intrinsic::debugtrap : llir::Intrinsic::trap;
  return llir::Intrinsic::getDeclaration(mod.get_llvm_module(), id);
}

} // namespace llvm
} // namespace beaker
