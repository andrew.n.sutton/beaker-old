#include "gen_array.hpp"


namespace beaker {
namespace llvm {

using namespace lang_array;

/// Generates the corresponding array type.
type
generate_type(codegen& gen, const array_type* t)
{
  type elem = generate(gen, t->get_element_type());
  return emit_array_type(gen, elem, t->get_extent(0));
}

/// Generates a buffer type for the array.
type
generate_type(codegen& gen, const buf_type* t)
{
  type elem = generate(gen, t->get_element_type());
  return emit_pointer_type(gen, elem);
}

value 
generate_expr(codegen& gen, const lang_array::index_expr* e)
{
  throw std::logic_error("array index not implemented");
}

} // namespace llvm
} // namespace beaker
