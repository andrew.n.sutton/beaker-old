#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.int/types_int.hpp>
#include <beaker/ast/lang.int/exprs_int.hpp>


namespace beaker {
namespace llvm {

type generate_type(codegen& gen, const lang_int::int_type* t);

value generate_expr(codegen& gen, const lang_int::int_expr* e);
value generate_expr(codegen& gen, const lang_int::add_expr* e);
value generate_expr(codegen& gen, const lang_int::sub_expr* e);
value generate_expr(codegen& gen, const lang_int::mul_expr* e);
value generate_expr(codegen& gen, const lang_int::div_expr* e);
value generate_expr(codegen& gen, const lang_int::rem_expr* e);
value generate_expr(codegen& gen, const lang_int::neg_expr* e);
value generate_expr(codegen& gen, const lang_int::eq_expr* e);
value generate_expr(codegen& gen, const lang_int::ne_expr* e);
value generate_expr(codegen& gen, const lang_int::lt_expr* e);
value generate_expr(codegen& gen, const lang_int::gt_expr* e);
value generate_expr(codegen& gen, const lang_int::le_expr* e);
value generate_expr(codegen& gen, const lang_int::ge_expr* e);

} // namespace llvm
} // namespace beaker
