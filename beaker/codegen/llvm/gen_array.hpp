#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.array/types_array.hpp>
#include <beaker/ast/lang.array/exprs_array.hpp>


namespace beaker {
namespace llvm {

type generate_type(codegen& gen, const lang_array::array_type* t);
type generate_type(codegen& gen, const lang_array::buf_type* t);

value generate_expr(codegen& gen, const lang_array::index_expr* e);

} // namespace llvm
} // namespace beaker
