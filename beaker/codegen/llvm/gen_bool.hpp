#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.bool/types_bool.hpp>
#include <beaker/ast/lang.bool/exprs_bool.hpp>


namespace beaker {
namespace llvm {

type generate_type(codegen& gen, const lang_bool::bool_type* t);

value generate_expr(codegen& gen, const lang_bool::bool_expr* e);
value generate_expr(codegen& gen, const lang_bool::and_expr* e);
value generate_expr(codegen& gen, const lang_bool::or_expr* e);
value generate_expr(codegen& gen, const lang_bool::xor_expr* e);
value generate_expr(codegen& gen, const lang_bool::imp_expr* e);
value generate_expr(codegen& gen, const lang_bool::eq_expr* e);
value generate_expr(codegen& gen, const lang_bool::not_expr* e);
value generate_expr(codegen& gen, const lang_bool::if_expr* e);
value generate_expr(codegen& gen, const lang_bool::and_if_expr* e);
value generate_expr(codegen& gen, const lang_bool::or_if_expr* e);
value generate_expr(codegen& gen, const lang_bool::assert_expr* e);

} // namespace llvm
} // namespace beaker
