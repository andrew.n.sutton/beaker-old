#pragma once

#include <beaker/codegen/llvm/generate.hpp>

#include <beaker/ast/lang.tu/decls_tu.hpp>


namespace beaker {
namespace llvm {

void generate_decl(codegen& gen, const lang_tu::tu_decl* d);

} // namespace llvm
} // namespace beaker
