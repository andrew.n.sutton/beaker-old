#include "gen_bool.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {
namespace llvm {

using namespace lang_bool;

type
generate_type(codegen& gen, const bool_type* t)
{
  return emit_int1_type(gen);
}

value 
generate_expr(codegen& gen, const lang_bool::bool_expr* e)
{
  return emit_bool(gen, e->get_value());
}

value
generate_expr(codegen& gen, const lang_bool::and_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_and(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_bool::or_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_or(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_bool::xor_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_xor(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_bool::imp_expr* e)
{
  throw std::logic_error("implication not implemented");
}

value
generate_expr(codegen& gen, const lang_bool::eq_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_ieq(gen, lhs, rhs);
}

/// Generates the expression `e ^ true`.
value
generate_expr(codegen& gen, const lang_bool::not_expr* e)
{
  value lhs = generate(gen, e->get_operand());
  value rhs = emit_true(gen);
  return emit_xor(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_bool::if_expr* e)
{
  block then_block(gen, "if.then");
  block else_block(gen, "if.else");
  block end_block(gen, "if.end");

  // Emit the condition.
  value cond = generate(gen, e->get_first());
  emit_branch(gen, cond, then_block, else_block);

  // Emit the then block.
  emit_block(gen, then_block);
  value then_value = generate(gen, e->get_true_value());
  then_block = get_current_block(gen); // NOTE: Value computed here.
  emit_branch(gen, end_block);

  // Emit the else block.
  emit_block(gen, else_block);
  value else_value = generate(gen, e->get_false_value());
  else_block = get_current_block(gen); // NOTE: Value computed here.
  emit_branch(gen, end_block);
  
  emit_block(gen, end_block);
  type ty = generate(gen, e->get_type());
  return emit_phi(gen, ty, {
    {then_value, then_block}, 
    {else_value, else_block}
  });
}

value
generate_expr(codegen& gen, const lang_bool::and_if_expr* e)
{
  block then_block(gen, "and.then");
  block end_block(gen, "and.end");

  // Emit the LHS and the branch.
  value lhs = generate(gen, e->get_lhs());
  block and_block = get_current_block(gen); // NOTE: value computed here.
  emit_branch(gen, lhs, then_block, end_block);

  // Emit the evaluation of the RHS and a jump to the end.
  emit_block(gen, then_block);
  value rhs = generate(gen, e->get_rhs());
  emit_branch(gen, end_block);

  emit_block(gen, end_block);
  type ty = generate(gen, e->get_type());
  return emit_phi(gen, ty, {
    {lhs, and_block}, 
    {rhs, then_block}
  });
}

value
generate_expr(codegen& gen, const lang_bool::or_if_expr* e)
{
  block else_block(gen, "or.else");
  block end_block(gen, "or.end");

  // Emit the LHS.
  value lhs = generate(gen, e->get_lhs());
  block or_block = get_current_block(gen); // NOTE: value computed here
  emit_branch(gen, lhs, end_block, else_block);

  // Emit the evaluation of the RHS.
  emit_block(gen, else_block);
  value rhs = generate(gen, e->get_rhs());
  emit_branch(gen, end_block);

  emit_block(gen, end_block);
  type ty = generate(gen, e->get_type());
  return emit_phi(gen, ty, {
    {lhs, or_block}, 
    {rhs, else_block}
  });
}

/// \todo Generate debug traps based on translation options.
value
generate_expr(codegen& gen, const lang_bool::assert_expr* e)
{
  block fail_block(gen, "assert.fail");
  block ok_block(gen, "assert.ok");

  // Emit the condition.
  value cond = generate(gen, e->get_operand());
  block cond_block = get_current_block(gen); // NOTE: value computed here
  emit_branch(gen, cond, ok_block, fail_block);

  // Emit the assertion trap.
  emit_block(gen, fail_block);
  value trap = get_trap_intrinsic(gen, false);
  emit_call(gen, trap);
  emit_unreachable(gen);

  emit_block(gen, ok_block);
  type ty = generate(gen, e->get_type());
  return emit_phi(gen, ty, {
    {cond, cond_block}
  });
}

} // namespace llvm
} // namespace beaker
