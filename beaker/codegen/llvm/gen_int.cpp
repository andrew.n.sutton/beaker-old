#include "gen_int.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {
namespace llvm {

using namespace lang_int;

/// \todo The integer type is determined by its bit size.
type
generate_type(codegen& gen, const int_type* t)
{
  return emit_int32_type(gen);
}

value
generate_expr(codegen& gen, const lang_int::int_expr* e)
{
  type t = generate(gen, e->get_type());
  if (is_signed(e->get_type()))
    return emit_sint(gen, t, e->get_signed());
  else
    return emit_uint(gen, t, e->get_unsigned());
}

// FIXME: Handle modular integer arithmetic.

value
generate_expr(codegen& gen, const lang_int::add_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(e->get_type()))
    return emit_sadd(gen, lhs, rhs);
  else
    return emit_uadd(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::sub_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(e->get_type()))
    return emit_ssub(gen, lhs, rhs);
  else
    return emit_uadd(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::mul_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(e->get_type()))
    return emit_smul(gen, lhs, rhs);
  else
    return emit_umul(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::div_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(e->get_type()))
    return emit_sdiv(gen, lhs, rhs);
  else
    return emit_udiv(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::rem_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(e->get_type()))
    return emit_srem(gen, lhs, rhs);
  else
    return emit_urem(gen, lhs, rhs);
}

/// Generates the instruction `0 - e`.
value
generate_expr(codegen& gen, const lang_int::neg_expr* e)
{
  type ty = generate(gen, e->get_type());
  value rhs = generate(gen, e->get_operand());
  if (is_signed(e->get_type())) {
    value lhs = emit_sint(gen, ty, 0);
    return emit_ssub(gen, lhs, rhs);
  }
  else {
    // NOTE: This operation should be ill-formed for natural numbers.
    value lhs = emit_uint(gen, ty, 0);
    return emit_usub(gen, lhs, rhs);
  }
}

value
generate_expr(codegen& gen, const lang_int::eq_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_ieq(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::ne_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  return emit_ine(gen, lhs, rhs);
}

/// Returns the type of the operands of a binary expression.
static const beaker::type*
get_operand_type(const binary_expr* e) 
{
  return e->get_lhs()->get_type();
}

value
generate_expr(codegen& gen, const lang_int::lt_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(get_operand_type(e)))
    return emit_slt(gen, lhs, rhs);
  else
    return emit_ult(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::gt_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(get_operand_type(e)))
    return emit_sgt(gen, lhs, rhs);
  else
    return emit_ugt(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::le_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(get_operand_type(e)))
    return emit_sle(gen, lhs, rhs);
  else
    return emit_ule(gen, lhs, rhs);
}

value
generate_expr(codegen& gen, const lang_int::ge_expr* e)
{
  value lhs = generate(gen, e->get_lhs());
  value rhs = generate(gen, e->get_rhs());
  if (is_signed(get_operand_type(e)))
    return emit_sge(gen, lhs, rhs);
  else
    return emit_uge(gen, lhs, rhs);
}

} // namespace llvm
} // namespace beaker
