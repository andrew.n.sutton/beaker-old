#include "gen_tu.hpp"

#include <iostream>


namespace beaker {
namespace llvm {

using namespace lang_tu;

void
generate_decl(codegen& gen, const tu_decl* d)
{
  // FIXME: Give the module a name.
  //
  // FIXME: I wonder if we should save generated modules in the root
  // generator for subsequent serialization (probably).
  module mod(gen.as_generator(), d);

  // Generate each declaration in turn.
  for (const decl* tld : d->get_declarations())
    generate(mod, tld);

  // FIXME: This isn't right...
  mod.dump();
}

} // namespace llvm
} // namespace beaker
