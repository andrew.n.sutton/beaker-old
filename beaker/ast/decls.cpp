#include "decls.hpp"
#include "types.hpp"
#include "exprs.hpp"
#include "print.hpp"

#include "lang.tu/decls_tu.hpp"
#include "lang.fn/decls_fn.hpp"

#include <iostream>


namespace beaker {

bool
decl::encloses(const decl* d) const
{
  const decl* p = d->get_context();
  while (p) {
    if (this == p)
      return true;
    p = p->get_context();
  }
  return false;
}

void
decl::print() const
{
  std::cerr << pretty(this) << '\n';
}

void
decl::print_verbose() const
{
  std::cerr << verbose(this) << '\n';
}

bool
typed_decl::has_type(const type* t) const
{
  return equal(get_type(), t);
}

bool
typed_decl::has_type_of(const expr* e) const
{
  return equal(get_type(), e->get_type());
}

// Update a parameter so it links to the function and update its index.
template<typename T>
static void
do_update(mapping_decl* map, T* p, int n)
{
  assert(!p->get_context() && "parameter already has an owner");
  p->cxt = dc(map);
  p->depth = 0;
  p->index = n;
}

static void
update_parameter(mapping_decl* map, decl* p, int n)
{
  struct fn {
    mapping_decl* map;
    int n;
    void operator()(decl*) { throw std::logic_error("invalid parameter"); }
    void operator()(lang_fn::in_parm* p) { do_update(map, p, n); }
    void operator()(lang_fn::out_parm* p) { do_update(map, p, n); }
    void operator()(lang_fn::var_parm* p) { do_update(map, p, n); }
    void operator()(lang_fn::ret_parm* p) { do_update(map, p, n); }
  };
  return apply(p, fn{map, n});
}

// Update the parameters and return so they link to the function.
static void
update_signature(mapping_decl* map)
{
  // Set the owners and indexes of parameters.
  std::size_t i = 0;
  for (decl* p : map->get_parameters())
    update_parameter(map, p, i++);

  // The return value has index -1.
  //
  // FIXME: Should we make the last parameter?
  update_parameter(map, map->get_return(), -1);
}

mapping_decl::mapping_decl(uid id, dc cxt, symbol* n, type* t, const decl_seq& ps, decl* r)
  : typed_decl(id, cxt, n, t), parms(ps), ret(r), body() 
{
  update_signature(this);
}

mapping_decl::mapping_decl(uid id, dc cxt, symbol* n, type* t, decl_seq&& ps, decl* r)
  : typed_decl(id, cxt, n, t), parms(std::move(ps)), ret(r), body() 
{
  update_signature(this);
}

mapping_decl::mapping_decl(uid id, dc cxt, symbol* n, type* t, const decl_seq& ps, decl* r, stmt* s)
  : typed_decl(id, cxt, n, t), parms(ps), ret(r), body(s) 
{
  update_signature(this);
}

mapping_decl::mapping_decl(uid id, dc cxt, symbol* n, type* t, decl_seq&& ps, decl* r, stmt* s)
  : typed_decl(id, cxt, n, t), parms(std::move(ps)), ret(r), body(s) 
{
  update_signature(this);
}

/// Note that return parameters are considered constant because they do not
/// bind to memory locations.
bool
is_constant(const decl* d)
{
  struct fn {
    bool operator()(const decl* d) { return false; }
    bool operator()(const lang_fn::const_decl* d) { return true; }
    bool operator()(const lang_fn::in_parm* d) { return true; }
    bool operator()(const lang_fn::ret_parm* d) { return true; }
  };
  return apply(d, fn{});
}

/// Note that output parameters are considered variables because they bind
/// to memory locations.
bool
is_variable(const decl* d)
{
  struct fn {
    bool operator()(const decl* d) { return false; }
    bool operator()(const lang_fn::var_decl* d) { return true; }
    bool operator()(const lang_fn::tmp_decl* d) { return true; }
    bool operator()(const lang_fn::var_parm* d) { return true; }
    bool operator()(const lang_fn::out_parm* d) { return true; }
  };
  return apply(d, fn{});
}

bool
is_parameter(const decl* d)
{
  struct fn {
    bool operator()(const decl* d) { return false; }
    bool operator()(const lang_fn::in_parm* d) { return true; }
    bool operator()(const lang_fn::out_parm* d) { return true; }
    bool operator()(const lang_fn::var_parm* d) { return true; }
    bool operator()(const lang_fn::ret_parm* d) { return true; }
  };
  return apply(d, fn{});
}

bool
is_function(const decl* d)
{
  return dynamic_cast<const lang_fn::fn_decl*>(d);
}

/// \todo This will be wrong as soon as we support global variables. For now,
/// only functions have static storage.
bool
has_static_storage(const decl* d)
{
  return dynamic_cast<const lang_fn::fn_decl*>(d);
}

/// \todo This will be wrong as soon as we support global variables. For now,
/// all value declarations are automatic.
bool
has_automatic_storage(const decl* d)
{
  return dynamic_cast<const value_decl*>(d);
}

/// \todo This will be wrong as soon as we support global variables. For now,
/// only functions have external linkage.
bool
has_external_linkage(const decl* d)
{
  return dynamic_cast<const lang_fn::fn_decl*>(d);
}

/// \todo This will be wrong as soon as we support global variables. For now,
/// having automatic storage is equivalent to no linkage.
bool
has_no_linkage(const decl* d)
{
  return has_automatic_storage(d);
}

} // namespace beaker