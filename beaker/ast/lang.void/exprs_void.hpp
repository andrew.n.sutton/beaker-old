#pragma once

#include <beaker/ast/exprs.hpp>


namespace beaker {
namespace lang_void {

/// An expression that has no effect or value.
struct nop_expr : nullary_expr
{
  using nullary_expr::nullary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// An expression that computes a value and discards the result.
struct void_expr : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// An expression that causes a trap.
struct trap_expr : nullary_expr
{
  using nullary_expr::nullary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};

} // namespace lang_void
} // namespace beaker
