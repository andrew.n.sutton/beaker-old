#pragma once

#include "types_void.hpp"
#include "exprs_void.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_type(pretty_printer&, const lang_void::void_type*);

void print_expr(pretty_printer&, const lang_void::nop_expr*);
void print_expr(pretty_printer&, const lang_void::void_expr*);
void print_expr(pretty_printer&, const lang_void::trap_expr*);

} // namespace beaker

