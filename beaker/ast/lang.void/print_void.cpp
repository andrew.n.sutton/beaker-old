#include "print_void.hpp"


namespace beaker
{

void 
print_type(pretty_printer& pp, const lang_void::void_type*)
{
  print(pp, "void");
}

void 
print_expr(pretty_printer& pp, const lang_void::nop_expr*)
{
  print(pp, "nop");
}

void 
print_expr(pretty_printer& pp, const lang_void::void_expr* e)
{
  print_builtin_call_expr(pp, "void", e->get_operand());
}

void 
print_expr(pretty_printer& pp, const lang_void::trap_expr*)
{
  print(pp, "trap");
}
  
}
