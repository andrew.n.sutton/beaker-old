#pragma once

#include <beaker/ast/types.hpp>


namespace beaker {
namespace lang_void {

/// Represents the type `void`.
struct void_type : type
{
  void_type() : type() { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};

} // namespace lang_void


// -------------------------------------------------------------------------- //
// Operations

/// Void types always compare equal.
inline bool 
equal_type(const lang_void::void_type*, const lang_void::void_type*)
{
  return true;
}

/// Adds no additional information to the hash.
inline void
hash_type(hasher& h, const lang_void::void_type* t)
{ }

} // namespace beaker

