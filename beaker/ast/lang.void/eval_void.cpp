#include "eval_void.hpp"


namespace beaker {

using namespace lang_void;

value
evaluate_expr(evaluator& eval, const nop_expr* e)
{
  return void_value{};
}

value
evaluate_expr(evaluator& eval, const void_expr* e)
{
  evaluate(eval, e->get_operand());
  return void_value{};
}

/// \todo This should do something other than throw. Probably, we should
/// simply return an error value indicating  a trap.
value
evaluate_expr(evaluator& eval, const trap_expr* e)
{
  throw std::runtime_error("trap");
}


} // namespace beaker
