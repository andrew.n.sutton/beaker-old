#pragma once

#include "types_void.hpp"
#include "exprs_void.hpp"

#include <beaker/ast/evaluate.hpp>


namespace beaker {

value evaluate_expr(evaluator& eval, const lang_void::nop_expr* e);
value evaluate_expr(evaluator& eval, const lang_void::void_expr* e);
value evaluate_expr(evaluator& eval, const lang_void::trap_expr* e);

} // namespace beaker

