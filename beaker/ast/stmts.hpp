#pragma once

#include <beaker/ast/seq.hpp>


namespace beaker {

// Forward declarations
#define def_stmt(D, NS) \
  namespace NS { struct D ## _stmt; }
#include "stmts.def"
#undef def_stmt


/// The base class of all statements in the language.
struct stmt
{
  struct const_visitor;
  struct visitor;

  virtual ~stmt() = default;

  virtual void accept(const_visitor&) const = 0;
  virtual void accept(visitor&) = 0;
};

/// The constant statement visitor.
struct stmt::const_visitor
{
#define def_stmt(D, NS) \
  virtual void visit(const NS::D ## _stmt*) { }
#include "stmts.def"
#undef def_stmt
};

/// The non-constant statement visitor.
struct stmt::visitor
{
#define def_stmt(D, NS) \
  virtual void visit(NS::D ## _stmt*) { }
#include "stmts.def"
#undef def_stmt
};


// -------------------------------------------------------------------------- //
// Visitors

template<typename F, typename T>
struct stmt_visitor : stmt::visitor
{
  stmt_visitor(F f) : fn(f) { }

#define def_stmt(T, NS) \
  virtual void visit(NS::T ## _stmt* s) { ret = fn(s); }
#include "stmts.def"
#undef def_stmt

  /// Apply the visitor to s and return the accumulated value.
  T apply(stmt* s)
  {
    s->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct stmt_visitor<F, void> : stmt::visitor
{
  stmt_visitor(F f) : fn(f) { }

#define def_stmt(T, NS) \
  virtual void visit(NS::T ## _stmt* s) { fn(s); }
#include "stmts.def"
#undef def_stmt

  /// Apply the visitor to s.
  void apply(stmt* s)
  {
    s->accept(*this);
  }

  F fn;
};


/// A non-modifying visitor.
template<typename F, typename T>
struct const_stmt_visitor : stmt::const_visitor
{
  const_stmt_visitor(F f) : fn(f) { }

#define def_stmt(T, NS) \
  virtual void visit(const NS::T ## _stmt* s) { ret = fn(s); }
#include "stmts.def"
#undef def_stmt

  /// Apply the visitor to s and return the accumulated value.
  T apply(const stmt* s)
  {
    s->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct const_stmt_visitor<F, void> : stmt::const_visitor
{
  const_stmt_visitor(F f) : fn(f) { }

#define def_stmt(T, NS) \
  virtual void visit(const NS::T ## _stmt* s) { fn(s); }
#include "stmts.def"
#undef def_stmt

  /// Apply the visitor to s.
  void apply(const stmt* s)
  {
    s->accept(*this);
  }

  F fn;
};


/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(stmt* s, F fn)
{
  using T = typename std::result_of_t<F(stmt*)>;
  stmt_visitor<F, T> vis(fn);
  return vis.apply(s);
}

/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(const stmt* s, F fn)
{
  using T = typename std::result_of_t<F(const stmt*)>;
  const_stmt_visitor<F, T> vis(fn);
  return vis.apply(s);
}

} // namespace beaker
