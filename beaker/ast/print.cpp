#include "print.hpp"

#include "lang.tu/print_tu.hpp"
#include "lang.void/print_void.hpp"
#include "lang.bool/print_bool.hpp"
#include "lang.int/print_int.hpp"
#include "lang.store/print_store.hpp"
#include "lang.fn/print_fn.hpp"
#include "lang.array/print_array.hpp"
#include "lang.io/print_io.hpp"

#include <iostream>


namespace beaker {

// -------------------------------------------------------------------------- //
// Pretty printer

pretty_printer::pretty_printer(unsigned flags)
  : pretty_printer(std::cout, flags)
{ }

pretty_printer::pretty_printer(std::ostream& os, unsigned flags)
  : os(os), nesting(0), flags(flags)
{ }

/// Print a single character.
void
pretty_printer::print(char c)
{
  os << c;
}

/// Print a string.
void
pretty_printer::print(const char* str)
{
  os << str;
}

/// Print a string.
void
pretty_printer::print(const std::string& str)
{
  os << str;
}

/// Print an integer value.
void
pretty_printer::print(std::intmax_t n)
{
  os << n;
}

/// Print an unsigned integer value.
void
pretty_printer::print(std::uintmax_t n)
{
  os << n;
}

void
pretty_printer::newline()
{
  os << '\n';
  std::string tab(nesting * 2, ' ');
  os << tab;
}

void
pretty_printer::space()
{
  os << ' ';
}

// -------------------------------------------------------------------------- //
// Print dispatch

/// Print the text of a symbol.
void
print(pretty_printer& pp, const symbol* sym)
{
  print(pp, *sym);
}

/// Dispatches to print_type(pp, t).
void
print(pretty_printer& pp, const type* t)
{
  struct fn {
    void operator()(const type*) { throw std::logic_error("unreachable"); }
#define def_type(T, NS) \
    void operator()(const NS::T ## _type* t) { return print_type(pp, t); }
#include "types.def"
#undef def_type

    pretty_printer& pp;
  };
  apply(t, fn{pp});
}

/// Dispatches to print_expr(pp, e).
void
print(pretty_printer& pp, const expr* e)
{
  struct fn {
    void operator()(const expr*) { throw std::logic_error("unreachable"); }
#define pp_expr(E, X, NS) \
    void operator()(const NS::E ## X* e) { return print_expr(pp, e); }
#include "exprs.def"
#undef pp_expr

    pretty_printer& pp;
  };
  apply(e, fn{pp});
}

/// Dispatches to print_decl(pp, d).
void
print(pretty_printer& pp, const decl* d)
{
  struct fn {
    void operator()(const decl*) { throw std::logic_error("unreachable"); }
#define pp_decl(D, X, NS) \
    void operator()(const NS::D ## X* d) { print_decl(pp, d); }
#include "decls.def"
#undef pp_decl

    pretty_printer& pp;
  };
  apply(d, fn{pp});
}

/// Dispatches to print_stmt(pp, s).
void
print(pretty_printer& pp, const stmt* s)
{
  struct fn {
    void operator()(const stmt*) { throw std::logic_error("unreachable"); }
#define def_stmt(T, NS) \
    void operator()(const NS::T ## _stmt* s) { return print_stmt(pp, s); }
#include "stmts.def"
#undef def_stmt

    pretty_printer& pp;
  };
  apply(s, fn{pp});
}

void
print_type(pretty_printer& pp, const type* t)
{
  throw std::logic_error("undefined type printing behavior");
}

void
print_expr(pretty_printer& pp, const expr*)
{
  throw std::logic_error("undefined expression printing behavior");
}

void
print_decl(pretty_printer& pp, const decl* d)
{
  throw std::logic_error("undefined declaration printing behavior"); 
}

void
print_stmt(pretty_printer& pp, const stmt* s)
{
  throw std::logic_error("undefined statement print behavior"); 
}


// -------------------------------------------------------------------------- //
// Print helpers

/// Print a type enclosed by the given characters.
void
print_enclosed_type(pretty_printer& pp, const type* t, char l, char r)
{
  pp.print(l);
  print(pp, t);
  pp.print(r);
}

/// Print an empty enclosure.
void
print_enclosed_expr(pretty_printer& pp, char l, char r)
{
  print(pp, l);
  print(pp, r);
}

/// Print an expression enclosed by the given characters.
void
print_enclosed_expr(pretty_printer& pp, const expr* e, char l, char r)
{
  print(pp, l);
  print(pp, e);
  print(pp, r);
}

/// Print a pair of expressions enclosed by the given characters.
void
print_enclosed_expr(pretty_printer& pp, const expr* e1, const expr* e2, char l, char r)
{
  print(pp, l);
  print(pp, e1);
  print(pp, ',');
  print(pp, e2);
  print(pp, r);
}

/// Print an expression that resembles a call to a builtin function.
void
print_builtin_call_expr(pretty_printer& pp, const char* fn)
{
  print(pp, fn);
  print_enclosed_expr(pp, '(', ')');
}

/// Print an expression that resembles a call to a builtin function.
void
print_builtin_call_expr(pretty_printer& pp, const char* fn, const expr* e)
{
  print(pp, fn);
  print_enclosed_expr(pp, e, '(', ')');
}

/// Print an expression that resembles a call to a builtin function.
void
print_builtin_call_expr(pretty_printer& pp, const char* fn, const expr* e1, const expr* e2)
{
  print(pp, fn);
  print_enclosed_expr(pp, e1, e2, '(', ')');
}

/// Print an expression enclosed by parentheses.
///
/// \todo Don't paren-enclose terminal expressions.
void
print_grouped_expr(pretty_printer& pp, const expr* e)
{
  print_enclosed_expr(pp, e, '(', ')');

  // if (is_terminal_expression(e))
  //   print(pp, e);
  // else
  //   print_enclosed_expr(pp, e, '(', ')');
}

/// Pretty print a prefix unary operator spelled by `op`. The operand is
/// printed with enclosed parentheses.
void
print_prefix_expr(pretty_printer& pp, const unary_expr* e, const char* op)
{
  pp.print(op);
  print_grouped_expr(pp, e->get_first());
}

// Pretty print an infix binary expression.
void
print_infix_expr(pretty_printer& pp, const binary_expr* e, const char* op)
{
  print_grouped_expr(pp, e->get_lhs());
  print_binary_op(pp, op);
  print_grouped_expr(pp, e->get_rhs());
}

} // namespace beaker