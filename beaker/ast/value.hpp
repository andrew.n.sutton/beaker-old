#pragma once

#include <cassert>
#include <cstdint>
#include <cstring>
#include <iosfwd>
#include <list>
#include <vector>


namespace beaker {

struct decl;
struct mapping_decl;
struct value;


// Kinds of values.
enum value_kind 
{
  uninit_value_kind,
  void_value_kind,
  int_value_kind,
  float_value_kind,
  ref_value_kind,
  fn_value_kind,
  agg_value_kind,
};

// Represents an uninitialized value. This typically indicates an error.
struct uninit_value { };

// Represents the absence of value.
struct void_value { };

// Represents integer values.
using integer_value = std::intmax_t;

// Represents floating point values.
using float_value = double;

/// A reference value refers to a stored object or sub-object. In particular,
/// it is a triple comprised of:
///
/// - a store index; either static storage or the call frame index
/// - a reference to a complete object in storage
/// - a (possibly empty) path to a sub-object.
///
/// For example, a reference to a variable is simply that variable; no
/// sub-object was requested. An expression like `a[3]` is a reference to
/// the storage for the array `a` and the a selector for the 3rd sub-object.
///
/// \todo Can the complete object be something other than a declaration. For
/// example, can an expression denote an undeclared object in storage? 
/// Probably, yes.
struct ref_value
{
  using path_type = std::vector<int>;

  ref_value() : store(0), obj() { }
  ref_value(value* v) : store(-1), obj(v) { }
  ref_value(int s, value* v) : store(s), obj(v) { }

  /// Returns true if the value is null.
  bool is_null() const { return obj; }

  /// Returns the store indicator for the reference.
  int get_store() const { return store; }

  /// Returns true if the referenced object is in the static store.
  bool in_static_store() const { return store == -1; }

  /// Returns true if the referenced object is in the automatic store.
  bool in_automatic_store() const { return store >= 0; }

  /// Returns the index of the frame in which the value is stored.
  int get_frame() const { assert(in_automatic_store()); return store; }

  /// Returns true if the reference is to a complete object. This is the case
  /// when the path is empty.
  bool is_complete_object() const { return path.empty(); }

  /// Returns true if the reference is a path to a sub-object. This is the
  /// case when the path is non-empty.
  bool is_subobject() const { return !path.empty(); }

  /// Returns a reference to the complete object.
  const value& get_complete_object() const { return *obj; }
  value& get_complete_object() { return *obj; }

  /// Returns a reference to the designated subobject.
  const value& get_subobject() const;
  value& get_subobject();

  /// Returns the complete object or subobject designated by the reference.
  const value& get_object() const;
  value& get_object();

  /// Returns the path to the sub-object.
  const path_type& get_path() const { return path; }

  /// Returns a new reference value whose path is extended to access the
  /// nth subobject of an aggregate value.
  ref_value extend(int n);
  
  /// Returns a new reference value whose path is extended by the indexes in
  /// in the given path.
  ref_value extend(const path_type& p);

  /// Returns a new reference value whose path designates a subobject indicated
  /// by the path `p`.
  ref_value designate(path_type&& p);

  /// Returns the size of the path.
  std::size_t size() const { return path.size(); }

  /// Path iterators
  auto begin() const { return path.begin(); }
  auto end() const { return path.end(); }
  auto begin() { return path.begin(); }
  auto end() { return path.end(); }

  bool operator==(const ref_value& v) const;
  bool operator!=(const ref_value& v) const;

  int store; // The store/frame indicator.
  value* obj; // The stored value.
  path_type path; // The sub-object path.
};

/// The value of a function.
using fn_value = const mapping_decl*;


/// An aggregate value is a sequence of sub-objects (values). 
struct agg_value 
{
  agg_value(int n);
  agg_value(const agg_value& v);
  agg_value(agg_value&& v);
  ~agg_value();

  agg_value& operator=(const agg_value& v);
  agg_value& operator=(agg_value&& v);

  /// Returns true the the aggregate has no members.
  bool empty() const { return len == 0; }

  /// Returns the number of elements in the aggregate.
  int size() const { return len; }

  /// Returns the nth element of the aggregate.
  const value& operator[](int n) const;
  value& operator[](int n);

  bool operator==(const agg_value& v) const;
  bool operator!=(const agg_value& v) const;

  const value* begin() const;
  const value* end() const;

  value* begin();
  value* end();
  
  int len;
  value* elems;
};

/// Representation of values.
///
/// \todo Rewrite this as an aligned store.
union value_data
{
  value_data() : u() { }
  value_data(void_value) : v() { }
  value_data(integer_value n) : z(n) { }
  value_data(float_value n) : f(n) { }
  value_data(ref_value r) : ref(r) { }
  value_data(fn_value f) : fn(f) { }
  value_data(agg_value a) : agg(a) { }
  ~value_data() { }
  
  uninit_value u;
  void_value v;
  integer_value z;
  float_value f;
  ref_value ref;
  fn_value fn;
  agg_value agg;
};


// Represents the compile-time value of an object.
//
// Values are used to represent literals, constants, and to perform
// compile-time evaluation. A value can be one of several different 
// kinds: integers, reals, and composites.
struct value
{
  value() : kind_(uninit_value_kind), data_() { }
  value(const value&);
  value(value&&);
  ~value();
  
  /// Construct a void value.
  value(void_value v) : kind_(void_value_kind), data_(v) { }

  /// Construct and integer value.
  explicit value(int n) : kind_(int_value_kind), data_(integer_value(n)) { }
  explicit value(std::intmax_t n) : kind_(int_value_kind), data_(n) { }
  explicit value(std::uintmax_t n) : kind_(int_value_kind), data_(std::intmax_t(n)) { }

  /// Construct a floating point value.
  explicit value(double n) : kind_(float_value_kind), data_(n) { }

  /// Construct a floating point value.
  value(ref_value r) : kind_(ref_value_kind), data_(r) { }

  /// Construct a function value.
  value(fn_value f) : kind_(fn_value_kind), data_(f) { }

  /// Construct a value with the given aggregate.
  value(const agg_value& a) : kind_(agg_value_kind), data_(a) { }

  /// Construct a value with the given aggregate
  value(agg_value&& a) : kind_(agg_value_kind), data_(std::move(a)) { }

  value& operator=(const value&);
  value& operator=(value&&);
  value& operator=(integer_value v) { return *this = value(v); }
  value& operator=(float_value v) { return *this = value(v); }
  value& operator=(ref_value v) { return *this = value(v); }
  value& operator=(fn_value v) { return *this = value(v); }
  value& operator=(const agg_value& v);
  value& operator=(agg_value&& v);
  
  /// Returns the kind of value.
  value_kind get_kind() const { return kind_; }

  /// Returns true if the value is uninitialized.
  bool is_uninitialized() const { return kind_ == uninit_value_kind; }

  /// Returns true if this has no value.
  bool is_void() const { return kind_ == void_value_kind; }

  /// Returns true if this is an integer value.
  bool is_integer() const { return kind_ == int_value_kind; }

  /// Returns true if this is a floating point value.
  bool is_float() const { return kind_ == float_value_kind; }

  /// Returns true if this is a reference to a value.
  bool is_reference() const { return kind_ == ref_value_kind; }

  /// Returns true if this is a function.
  bool is_function() const { return kind_ == fn_value_kind; }

  /// Returns true if this is an aggregate.
  bool is_aggregate() const { return kind_ == agg_value_kind; }

  /// Returns the integer value.
  integer_value get_int() const;

  /// Returns the floating point value.
  float_value get_float() const;

  /// Get the reference value (i.e., a storage location).
  const ref_value& get_reference() const;
  ref_value& get_reference();

  /// Returns the object designated by a reference.
  const value& get_referenced_object() const;
  value& get_referenced_object();

  /// Get the function value.
  fn_value get_function() const;

  /// Returns the aggregate object.
  const agg_value& get_aggregate() const;
  agg_value& get_aggregate();

private:
  /// Destroys the value.
  void kill();

  /// Copy the value of v after setting the kind.
  void copy(const value& v);

  /// Moves the value of v after setting the kind.
  void move(value&& v);

  value_kind kind_;
  value_data data_;
};

inline void
value::kill()
{
  switch (kind_) {
    default:
      // Trivially destructible types do not need to be destroyed.
      break;
    case ref_value_kind:
      data_.ref.~ref_value();
      break;
    case agg_value_kind:
      data_.agg.~agg_value();
      break;
  }
}

inline void
value::copy(const value& v)
{
  switch (kind_) {
    default:
      // Trivially copyable types can be copied using memcpy.
      std::memcpy(&data_, &v.data_, sizeof(value_data));
      break;
    case ref_value_kind:
      new (&data_.ref) ref_value(v.data_.ref);
      break;
    case agg_value_kind:
      new (&data_.agg) agg_value(v.data_.agg);
      break;
  }
}

inline void
value::move(value&& v)
{
  switch (kind_) {
    default:
      // Trivially copyable types can be moved using memcpy.
      std::memcpy(&data_, &v.data_, sizeof(value_data));
      break;
    case ref_value_kind:
      new (&data_.ref) ref_value(std::move(v.data_.ref));
      break;
    case agg_value_kind:
      new (&data_.agg) agg_value(std::move(v.data_.agg));
      break;
  }
}

inline
value::~value()
{
  kill();
}

inline 
value::value(const value& v)
  : kind_(v.kind_)
{
  copy(v);
}

inline 
value::value(value&& v)
  : kind_(v.kind_)
{
  move(std::move(v));
}

inline value&
value::operator=(const value& v) 
{
  kill();
  kind_ = v.kind_;
  copy(v);
  return *this;
}

inline value&
value::operator=(value&& v) 
{
  kill();
  kind_ = v.kind_;
  move(std::move(v));
  return *this;
}

inline value& 
value::operator=(const agg_value& v)
{
  kill();
  kind_ = agg_value_kind;
  new (&data_.agg) agg_value(v);
  return *this;
}

inline value& 
value::operator=(agg_value&& v)
{
  kill();
  kind_ = agg_value_kind;
  new (&data_.agg) agg_value(std::move(v));
  return *this;
}

inline integer_value
value::get_int() const 
{ 
  assert(is_integer());
  return data_.z;
}

inline float_value
value::get_float() const 
{ 
  assert(is_float());
  return data_.f;
}

inline const ref_value&
value::get_reference() const 
{ 
  assert(is_reference());
  return data_.ref;
}

inline ref_value&
value::get_reference() 
{ 
  assert(is_reference());
  return data_.ref;
}

inline const value&
value::get_referenced_object() const
{
  return get_reference().get_object();
}

inline value&
value::get_referenced_object()
{
  return get_reference().get_object();
}

inline const mapping_decl*
value::get_function() const 
{ 
  assert(is_function());
  return data_.fn;
}

inline const agg_value&
value::get_aggregate() const
{
  assert(is_aggregate());
  return data_.agg;
}

inline agg_value&
value::get_aggregate() 
{
  assert(is_aggregate());
  return data_.agg;
}


// -------------------------------------------------------------------------- //
// Equality comparison

/// Two values are when they have the same kind and underlying value.
inline bool
operator==(const value& a, const value& b)
{
  if (a.get_kind() != b.get_kind())
    return false;
  switch (a.get_kind()) {
    case uninit_value_kind:
    case void_value_kind:
      return true;
    case int_value_kind:
      return a.get_int() == b.get_int();
    case float_value_kind:
      return a.get_float() == b.get_float();
    case ref_value_kind:
      return a.get_reference() == b.get_reference();
    case fn_value_kind:
      return a.get_function() == b.get_function();
    case agg_value_kind:
      return a.get_aggregate() == b.get_aggregate();
    default:
      break;
  }
  assert(false && "invalid value kind");
}

inline bool
operator!=(const value& a, const value& b)
{
  return !(a == b);
}


// -------------------------------------------------------------------------- //
// Hashing

struct hasher;

void hash(hasher&, const value&);


// -------------------------------------------------------------------------- //
// Streaming

std::ostream& operator<<(std::ostream&, const value&);


} // namespace beaker
