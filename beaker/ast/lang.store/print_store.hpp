#pragma once

#include "types_store.hpp"
#include "exprs_store.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_type(pretty_printer& pp, const lang_store::ref_type* t);

void print_expr(pretty_printer& pp, const lang_store::ref_expr* e);
void print_expr(pretty_printer& pp, const lang_store::value_expr* e);
void print_expr(pretty_printer& pp, const lang_store::assign_expr* e);
void print_expr(pretty_printer& pp, const lang_store::nop_init* e);
void print_expr(pretty_printer& pp, const lang_store::zero_init* e);
void print_expr(pretty_printer& pp, const lang_store::copy_init* e);
void print_expr(pretty_printer& pp, const lang_store::bind_init* e);


} // namespace beaker

