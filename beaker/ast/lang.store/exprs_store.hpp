#pragma once

#include <beaker/ast/exprs.hpp>


namespace beaker {

struct decl;
struct named_decl;

namespace lang_store {

/// An expression that refers to a named declaration.
struct ref_expr : nullary_expr
{
  ref_expr(type* t, named_decl* d) : nullary_expr(t), ref(d) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the referenced declaration.
  named_decl* get_declaration() const { return ref; }

  named_decl* ref;
};


/// Represents the expression `value(e1)`, which loads the value from the 
/// storage location computed by `e1`. This is used to "convert" references 
/// into objects by fetching their values.
struct value_expr : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 = e2`, which stores the value of `e2`
/// in the storage location computed by `e1`.
struct assign_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


// -------------------------------------------------------------------------- //
// Basic initializers

/// Represents the trivial of initialization of an object. 
struct nop_init : nullary_expr
{
  nop_init(type* t) : nullary_expr(t) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the zero-initialization of an object. This is chosen for the
/// default-initialization of scalars and arrays of scalars. For example:
///
///   var int x; // zero-initialized.
///
/// The initializer of `x` will be a `zero_init` with type `int`.
struct zero_init : nullary_expr
{
  zero_init(type* t) : nullary_expr(t) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the initialization of an object by a value. This happens, for
/// example. when declaring a non-reference, non-udt variable.
///
///   var int x = 0;
///
/// The initializer of `x` is an `copy_init` whose operand is the literal 0.
struct copy_init : unary_expr
{
  copy_init(type* t, expr* e) : unary_expr(t, e) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the expression that computes the initial value.
  const expr* get_value() const { return get_operand(); }
  expr* get_value() { return get_operand(); }
};


/// Represents the binding of a name to an expression. This happens, for
/// example, with `let` and reference declarations.
///
///   var int n = 0;
///   var int& r = n;    // x binds to the address of n.
///   let int v = 3 * n; // z binds to the computed value of 3 * n.
///
/// There is no storage in either case.
struct bind_init : unary_expr
{
  bind_init(type* t, expr* e) : unary_expr(t, e) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the expression that computes the initial value.
  const expr* get_value() const { return get_operand(); }
  expr* get_value() { return get_operand(); }
};

} // namespace lang_store
} // namespace beaker
