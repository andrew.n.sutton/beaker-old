#pragma once

#include <beaker/ast/types.hpp>


namespace beaker {
namespace lang_store {

/// Represents the type `t&`. Reference values are the storage locations of
/// objects. The type `t` is called the object type.
struct ref_type : type
{
  ref_type(type* t) : type(), obj(t) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the referenced object types.
  type* get_object_type() const { return obj; }

  type* obj;
};

} // namespace lang_store


// -------------------------------------------------------------------------- //
// Operations

/// Two reference types are equal only if their object types are equal.
inline bool 
equal_type(const lang_store::ref_type* a, const lang_store::ref_type* b)
{
  return equal(a->get_object_type(), b->get_object_type());
}

/// Add the object type to the hash.
inline void
hash_type(hasher& h, const lang_store::ref_type* t)
{
  hash(h, t->get_object_type());
}

} // namespace beaker

