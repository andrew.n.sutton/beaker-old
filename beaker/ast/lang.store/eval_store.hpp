#pragma once

#include "types_store.hpp"
#include "exprs_store.hpp"

#include <beaker/ast/evaluate.hpp>


namespace beaker {

value evaluate_expr(evaluator& eval, const lang_store::ref_expr* e);
value evaluate_expr(evaluator& eval, const lang_store::value_expr* e);
value evaluate_expr(evaluator& eval, const lang_store::assign_expr* e);

void evaluate_init(evaluator& eval, object& obj, const lang_store::nop_init* e);
void evaluate_init(evaluator& eval, object& obj, const lang_store::zero_init* e);
void evaluate_init(evaluator& eval, object& obj, const lang_store::copy_init* e);
void evaluate_init(evaluator& eval, object& obj, const lang_store::bind_init* e);


} // namespace beaker

