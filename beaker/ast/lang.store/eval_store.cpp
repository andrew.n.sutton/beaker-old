#include "eval_store.hpp"

#include <beaker/ast/context.hpp>
#include <beaker/ast/decls.hpp>
#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

using namespace lang_store;

/// Returns a reference to the declaration. References to functions or objects
/// with static storage are lazily statically initialized.
///
/// \todo If the declaration has static storage, then lookup the declaration
/// in the static store.
value
evaluate_expr(evaluator& eval, const ref_expr* e)
{
  const named_decl* d = e->get_declaration();

  // The value of a function is a function value.
  //
  // FIXME: Is there any reason we don't just directly bind these into
  // the static store? Same with other flavors of constants.
  if (is_function(d))
    return value(static_cast<const mapping_decl*>(d));

  // In all other cases (i.e., constants and variables), the value is found
  // by lookup.
  //
  // FIXME: The store depends on the declaration's storage duration.
  return eval.lookup_automatic(d);
}

/// Returns the value of a reference. Note that the value of an aggregate
/// is a location.
///
/// \todo Validate the address before returning.
value
evaluate_expr(evaluator& eval, const value_expr* e)
{
  value ref = evaluate(eval, e->get_operand());
  if (is_aggregate(e->get_type()))
    return ref;
  else
    return eval.fetch(ref);
}

/// Store the value of the right-hand side in the object referenced by the
/// left hand side.
value
evaluate_expr(evaluator& eval, const assign_expr* e)
{
  assert(!is_aggregate(e->get_rhs()->get_type()) && "copy assignment of aggregate");
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  eval.fetch(lhs) = rhs;
  return lhs;
}

/// Trivially initialize `obj. Leave the value in an uninitialized state.
///
/// \todo Actually install a trap representation for scalar values? Note that
/// void values don't get trap representations.
void
evaluate_init(evaluator& eval, object& obj, const nop_init* e)
{
  // Do nothing.
}

/// Initialize `obj` with a `0` value. This is defined only for scalar types.
/// Note that for pointer-like types, the zero value is a nullptr.
void
evaluate_init(evaluator& eval, object& obj, const zero_init* e)
{
  assert(is_scalar(e->get_type()) && "zero initialization of aggregate");
  const type* t = e->get_type();
  if (is_function(t))
    *obj = fn_value(nullptr);
  else if (is_buffer(t))
    *obj = ref_value();
  else
    *obj = integer_value(0);
}

/// Initialize `obj` with the value of the initializer. If `obj` is null
/// then no initialization is performed. Returns the value of the initializer.
void
evaluate_init(evaluator& eval, object& obj, const copy_init* e)
{
  assert(is_scalar(e->get_type()) && "copy initialization of aggregate");
  value val = evaluate(eval, e->get_value());
  *obj = val;
}

/// Simply returns the value of the initializer. Note that references do not
/// have storage.
///
/// \todo Are there any cases where a reference has storage? A member of a
/// class, perhaps?
void
evaluate_init(evaluator& eval, object& obj, const bind_init* e)
{
  assert(obj.is_direct() && "reference initialization of indirect value");
  assert(is_reference(e->get_type()) && "reference initialization of object");
  *obj = evaluate(eval, e->get_value());
}

} // namespace beaker
