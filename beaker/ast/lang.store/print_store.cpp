#include "print_store.hpp"

#include <beaker/ast/decls.hpp>


namespace beaker {

using namespace lang_store;

/// \todo Do we need to support grouping for types? Probably...
void 
print_type(pretty_printer& pp, const ref_type* t)
{
  print_enclosed_type(pp, t->get_object_type(), '(', ')');
  pp.print('&');
}

/// \todo Compute the shortest possible prefix for the name or print its
/// fully qualified version. We might actually want to manage scopes during
/// printing, to make the print back more elegant.
void
print_expr(pretty_printer& pp, const ref_expr* e)
{
  if (pp.verbose_output()) {
    print(pp, "__ref");
    print(pp, '(');
    print(pp, e->get_declaration()->get_name());
    print(pp, ')');
  }
  else
    print(pp, e->get_declaration()->get_name());
}

void
print_expr(pretty_printer& pp, const value_expr* e)
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__value", e->get_operand());
  else
    print(pp, e->get_operand());
}

void
print_expr(pretty_printer& pp, const assign_expr* e)
{
  print_infix_expr(pp, e, "=");
}

void
print_expr(pretty_printer& pp, const nop_init* e) 
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__nop");
}

void
print_expr(pretty_printer& pp, const zero_init* e) 
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__zero");
}

void
print_expr(pretty_printer& pp, const copy_init* e) 
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__copy", e->get_value());
  else
    print(pp, e->get_value());
}

void
print_expr(pretty_printer& pp, const bind_init* e)
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__bind", e->get_value());
  else
    print(pp, e->get_value());
}


} // namespace beaker

