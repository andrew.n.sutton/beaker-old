#pragma once

#include <beaker/ast/seq.hpp>

#include <type_traits>


namespace beaker
{

// Forward declarations
struct context;
struct type;
#define def_type(T, NS) \
  namespace NS { struct T ## _type; }
#include "types.def"
#undef def_type


/// Represents the set of all types.
///
/// All types have a size and alignment. The size of an object may be 0,
/// indicating that objects of that type cannot be created. If the size is
/// 0, the alignment must also be 0.
///
/// A type that is passed directly is passed via registers. This is determined
/// by the ABI.
struct type 
{
  struct const_visitor;
  struct visitor;

  /// Stores computed information about the size and layout of a type.
  ///
  /// \todo Layout of arrays and records may be interesting.
  struct layout {
    layout() : size(), align(), direct() { }
    
    layout(int s, int a, bool d) : size(s), align(a), direct(d) { }
    
    int size;
    int align;
    bool direct : 1;
  };

  type() = default;

  type(layout info) : info(info) { }

  virtual ~type() = default;

  virtual void accept(const_visitor&) const = 0;
  virtual void accept(visitor&) = 0;

  /// Returns the number of bits in objects of this type.
  int get_size_bits() const { return info.size; }

  /// Returns the number of bits in the alignment of this type.
  int get_alignment_bits() const { return info.align; }

  /// Returns true if the type is passed directly (in registers).
  bool is_direct() const { return info.direct; }

  /// Returns true if the type is passed indirectly.
  bool is_indirect() const { return !is_direct(); }

  /// Pretty print this type.
  void print() const;

  /// Pretty print this type, including internal types.
  void print_verbose() const;

  layout info;
};

/// The constant type visitor.
struct type::const_visitor
{
#define def_type(T, NS) \
  virtual void visit(const NS::T ## _type*) { }
#include "types.def"
#undef def_type
};

/// The constant type visitor.
struct type::visitor
{
#define def_type(T, NS) \
  virtual void visit(NS::T ## _type*) { }
#include "types.def"
#undef def_type
};


/// An object type is one whose values can be stored (in memory). Object types
/// have size and alignment.
///
/// When passed as a arguments to a function, objects can be passed directly 
/// (in a register) or indirectly (as a pointer). The means of determining how 
/// an object is passed is determined by the ABI of the target architecture. 
/// In general, objects greater than the size of a target's register are 
/// passed indirectly.
struct object_type : type
{
  object_type(layout info) : type(info) { }
};


/// The base class class of all fundamental object types. 
struct base_type : object_type
{
  using object_type::object_type;
};


// -------------------------------------------------------------------------- //
// Operations

/// Returns true if `a` and `b` are the same type.
bool equal(const type* a, const type* b);
bool equal_type(const type*, const type*);
bool equal_type(const base_type*, const base_type*);

/// Add the representation of `t` onto `h`.
void hash(hasher& h, const type* t);
void hash_type(hasher&, const type*);
void hash_type(hasher&, const base_type*);


// Primary type categories

/// Returns true if `t` is `void`.
bool is_void(const type* t);

/// Returns true if `t` is `bool`.
bool is_bool(const type* t);

/// Returns true if `t` is a reference type.
bool is_reference(const type* t);

/// Returns true if `t` is a function type.
bool is_function(const type* t);

/// Returns true if `t` is an array type.
bool is_array(const type* t);

/// Returns true if `t` is a buffer type.
bool is_buffer(const type* t);

/// Returns true if `t` is a tuple type.
bool is_tuple(const type* t);


// Composite type categories

/// Returns true if `t` is an integral type. The integral types are
/// `int` and `nat`.
bool is_integral(const type* t);

/// Returns true if `t` is a signed integer type.
bool is_signed(const type* t);

/// Returns true if `t` is an unsigned integer type.
bool is_unsigned(const type* t);

/// Returns true if `t` is an arithmetic type. The arithmetic types are `bool`,
/// the integral types and the floating point types.
///
/// \todo Add support for floating point types.
///
/// \todo In order for bool to be arithmetic, it must define all of the
/// arithmetic operators.
inline bool 
is_arithmetic(const type* t)
{
  return is_bool(t) || is_integral(t);
}

/// Returns true if `t` is a scalar type. The scalar types include the 
/// arithmetic types and pointer types.
///
/// \todo Should functions be considered scalars? We do treat them as such.
inline bool 
is_scalar(const type* t)
{
  return is_arithmetic(t) || is_function(t) || is_buffer(t);
}

/// Returns true if `t` is an aggregate type. The aggregate types are arrays
/// and tuples.
inline bool
is_aggregate(const type* t)
{
  return is_array(t) || is_tuple(t);
}

/// Returns true if `t` is a object type. The object types are all types except
/// `void` and reference types. Note that function types are object types.
inline bool
is_object(const type* t)
{
  return !(is_void(t) || is_reference(t));
}


// Associated types

/// If `t` is a reference type, this returns its object type, otherwise `t` is
/// an object type and the argument is returned.
const type* get_object_type(const type* t);
type* get_object_type(type* t);

/// If `t` is an array or pointer, this returns the type of object stored
/// or referred to.
const type* get_element_type(const type* t);
type* get_element_type(type* t);


// -------------------------------------------------------------------------- //
// Visitors

template<typename F, typename T>
struct type_visitor : type::visitor
{
  type_visitor(F f) : fn(f) { }

#define def_type(T, NS) \
  virtual void visit(NS::T ## _type* t) { ret = fn(t); }
#include "types.def"
#undef def_type

  /// Apply the visitor to t and return the accumulated value.
  T apply(type* t)
  {
    t->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct type_visitor<F, void> : type::visitor
{
  type_visitor(F f) : fn(f) { }

#define def_type(T, NS) \
  virtual void visit(NS::T ## _type* t) { fn(t); }
#include "types.def"
#undef def_type

  /// Apply the visitor to t.
  void apply(type* t)
  {
    t->accept(*this);
  }

  F fn;
};


/// A non-modifying visitor.
template<typename F, typename T>
struct const_type_visitor : type::const_visitor
{
  const_type_visitor(F f) : fn(f) { }

#define def_type(T, NS) \
  virtual void visit(const NS::T ## _type* t) { ret = fn(t); }
#include "types.def"
#undef def_type

  /// Apply the visitor to t and return the accumulated value.
  T apply(const type* t)
  {
    t->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct const_type_visitor<F, void> : type::const_visitor
{
  const_type_visitor(F f) : fn(f) { }

#define def_type(T, NS) \
  virtual void visit(const NS::T ## _type* t) { fn(t); }
#include "types.def"
#undef def_type

  /// Apply the visitor to t.
  void apply(const type* t)
  {
    t->accept(*this);
  }

  F fn;
};


/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(type* t, F fn)
{
  using T = typename std::result_of_t<F(type*)>;
  type_visitor<F, T> vis(fn);
  return vis.apply(t);
}

/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(const type* t, F fn)
{
  using T = typename std::result_of_t<F(const type*)>;
  const_type_visitor<F, T> vis(fn);
  return vis.apply(t);
}

} // namespace beaker
