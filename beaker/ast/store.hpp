#pragma once

#include <beaker/ast/seq.hpp>
#include <beaker/ast/value.hpp>

#include <list>
#include <memory>
#include <stack>
#include <unordered_map>


namespace beaker {

struct context;

// -------------------------------------------------------------------------- //
// Objects

/// An object stores a bound value, either directly or indirectly. A bound
/// value is one associated with a declaration in a store. The value of the
/// object is held indirectly if the value is a reference and is held directly
/// otherwise.
struct object
{
  object(value& v) : val(&v) { }

  /// Returns true if the object holds the value directly (i.e., if it is not
  /// a reference).
  bool is_direct() const { return !val->is_reference(); }

  /// Returns true if the object holds the value indirectly.
  bool is_indirect() const { return val->is_reference(); }
    
  /// Returns the bound value of the object.
  const value& get_value() const { return *val; }

  /// Returns either the directly or indirectly stored object.
  const value& get_object() const { return is_indirect() ? val->get_referenced_object() : *val; }
  value& get_object() { return is_indirect() ? val->get_referenced_object() : *val; }

  /// Equivalent to `get_object()`. 
  const value& operator*() const { return get_object(); }
  value& operator*() { return get_object(); }

  /// Equivalent to `&get_object()`. 
  const value* operator->() const { return &get_object(); }
  value* operator->() { return &get_object(); }

  value* val;
};


// -------------------------------------------------------------------------- //
// Store

/// At its core, a store is simply a doubly linked list of allocated objects.
/// For a call stack, this emulates stack memory s.t., the local variables
/// and temporaries are stored toward the back of the list.
///
/// Only complete objects are allocated to a store. Subobjects (e.g., elements
/// of an array) are not directly addressable; references to subobjects are
/// computed by a path.
///
/// Derived stores provide facilities for associating declarations with their
/// allocated values.
///
/// Each store has an associated id, which is used to create references to
/// the objects it contains.
///
/// \todo Consider this a vector and rewrite reference values in terms of
/// indexes. That might also make it even easier to validate references: a 
/// reference is only valid when it's index is in range?). This would make
/// the dynamic store harder to implement efficiently due to fragmentation.
struct store : std::list<value>
{
  store(int n) : id(n) { }

  /// Returns the id of the store.
  int get_id() const { return id; }

  /// Allocate a new value corresponding to the declaration `d`. If `d` does
  /// not require storage, this returns a new uninitialized value. Otherwise,
  /// it adds a new uninitialized value to the store and returns a reference
  /// to that object.
  value allocate(context& cxt, const decl* d);

  /// Materialize an object of type `t`. This will always allocate storage.
  value materialize(context& cxt, const type* t);

  int id;
};


/// The static store associates static declarations with their values and
/// addresses. Note that statically allocated objects are never deallocated.
struct static_store : store
{
  static_store() : store(-1) { }

  /// Allocate an object corresponding to `d`.
  object allocate(context& cxt, const decl* d);

  /// Associate `d` with it's value.
  void bind(const decl* d, value val);
  
  /// Returns the value of the object associated with `d`.
  const value& lookup(const decl* d) const;
  value& lookup(const decl* d);

  std::unordered_map<const decl*, value> map;
};


/// A (stack) frame is a store for local variables along with a list of
/// declaration/value bindings. This provides two distinct services: 
/// maintaining the list of stored objects, and associating declarations with
/// their values. Note that not all associated values are stored objects (e.g.,
/// constants).
///
/// The storage associated with a frame is deallocated when the frame is popped 
/// from the call stack.
struct frame : store
{
  /// Associates a declaration with its value.
  using binding = std::pair<const decl*, value>;

  /// Stores a list of bindings. These are dynamically allocated so that
  /// references to values in this frame are not invalidated when adding new
  /// objects.
  ///
  /// \todo We could flatten the extra indirection if we could guarantee that
  /// all locals and temporaries were allocated at the time the frame was
  /// created (i.e., reserve storage?).
  using binding_seq = std::vector<std::unique_ptr<binding>>;

  frame(int n) : store(n) { }

  /// Allocates an object corresponding to `d` and creates a binding to
  /// that value.
  object allocate(context& cxt, const decl* d);

  /// Materialize a temporary for an object of type `t`.
  value materialize(context& cxt, const type* t);

  /// Associate a pre-allocated object with `d` in the current frame.
  object bind(const decl* d, value v);

  /// Returns the number of bindings.
  std::size_t num_bindings() const  { return bindings.size(); }

  /// Returns the declaration of the nth binding.
  const decl* get_declaration(std::size_t n) const { return bindings[n]->first; }

  /// Returns the nth bound value.
  const value& get_value(std::size_t n) const { return bindings[n]->second; }
  value& get_value(std::size_t n) { return bindings[n]->second; }

  binding_seq bindings;
};


/// The automatic store is a stack of frames and a table that associates
/// declaration bindings with their innermost value.
struct automatic_store
{
  /// A reference to the innermost value associated with a declaration. This
  /// is a pair comprising a a frame pointer and an offset in its binding list.
  using binding = std::pair<frame*, std::size_t>;

  /// Represents the values associated with a particular declaration.
  struct binding_stack : std::vector<binding>
  {
    /// Returns a binding to the innermost binding for a declaration.
    const binding& top() const { return back(); }
    binding& top() { return back(); }

    /// Push a new value binding for the declaration.
    void push(frame* f, std::size_t n) { emplace_back(f, n); }
    
    /// Pop the innermost binding for the declaration.
    void pop() { pop_back(); }
  };

  /// Associates each declaration with a sequence of bindings.
  using decl_map = std::unordered_map<const decl*, binding_stack>;

  /// Records the current stack of frames.
  using call_stack = std::stack<std::unique_ptr<frame>>;

  // Allocation

  /// Allocate a single object in the current frame for `d`.
  object allocate(context& cxt, const decl* d);

  /// Materialize a temporary in the current frame for an object of type `t`.
  value materialize(context& cxt, const type* t);

  /// Returns the innermost value associated with the declaration.
  const value& lookup(const decl* d) const;
  value& lookup(const decl* d);

  // Call stack

  /// Allocates a new frame. Note that storage for parameters is allocated
  /// prior to pushing the frame on the call stack.
  frame* make_frame() { return new frame(stack.size()); }

  /// Returns the current stack frame.
  const frame* current_frame() const { return stack.top().get(); }
  frame* current_frame() { return stack.top().get(); }

  /// Enter a new stack frame.
  void enter_frame() { return enter_frame(make_frame()); }

  /// Enter the stack frame `f`, binding each declaration to its corresponding
  /// value.
  void enter_frame(frame* f);
  
  /// Leave the current stack frame.
  void leave_frame();

  /// Returns the number of frames on the stack.
  std::size_t frames() const { return stack.size(); }

  // Bindings

  /// Returns the stack associated with `d`. This is us
  const binding_stack& bindings(const decl* d) const;
  binding_stack& bindings(const decl* d);

  /// Returns the innermost value binding for `d`.
  const binding& innermost_binding(const decl* d) const;
  binding& innermost_binding(const decl* d);

  /// Build a binding for d in the given frame.
  void bind(const decl* d, frame* f, int n);

  decl_map map;
  call_stack stack;
};


} // namespace beaker
