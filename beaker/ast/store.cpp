#include "store.hpp"
#include "types.hpp"
#include "decls.hpp"
#include "print.hpp"

#include "lang.array/types_array.hpp"

#include <iostream>


namespace beaker {

// -------------------------------------------------------------------------- //
// Store

/// Defines an initial "shape" for the object. For aggregate types, this
/// creates all of the subobjects needed to define the type. For scalars,
/// this simply leaves the (single) object uninitialized.
///
/// This is done at allocation time so that initializers can directly 
/// sub-objects instead of copying result values.
static void
shape_storage(context& cxt, value& obj, const type* t)
{
  if (const auto* at = dynamic_cast<const lang_array::array_type*>(t)) {
    // For arrays, the object is an aggregate of its row type.
    const type* rt = at->get_row_type(cxt);
    agg_value agg(at->get_rows());
    std::for_each(agg.begin(), agg.end(), [&cxt, rt](value& v) {
      shape_storage(cxt, v, rt);
    });
    obj = agg;
  }
  else if (const auto* tt = dynamic_cast<const lang_array::tuple_type*>(t)) {
    // For tuples, the object is an aggregate of its element types.
    agg_value agg(tt->size());
    const type_seq& ts = tt->get_element_types();
    auto ti = ts.begin();
    auto vi = agg.begin();
    while (ti != ts.end()) {
      shape_storage(cxt, *vi, *ti);
      ++ti;
      ++vi;
    }
    obj = agg;
  }
  else {
    // For non-aggregate types, the object is uninitialized.
    assert(is_scalar(t) || is_void(t) && "invalid shape for aggregate");
  }
}

value
store::allocate(context& cxt, const decl* d)
{
  const type* t = d->get_type();
  if (is_variable(d) && is_object(t))
    return materialize(cxt, t);
  else
    return {};
}

value
store::materialize(context& cxt, const type* t)
{
  auto iter = emplace(end());
  shape_storage(cxt, *iter, t);
  return ref_value(id, &*iter);
}


// -------------------------------------------------------------------------- //
// Static store

object
static_store::allocate(context& cxt, const decl* d)
{
  assert(map.count(d) == 0 && "declaration already bound");
  value val = store::allocate(cxt, d);
  auto result = map.emplace(d, val);
  return result.first->second;
}

const value&
static_store::lookup(const decl* d) const
{
  assert(map.count(d) != 0 && "unbound declaration");
  return map.find(d)->second;
}

value&
static_store::lookup(const decl* d)
{
  assert(map.count(d) != 0 && "unbound declaration");
  return map.find(d)->second;
}


// -------------------------------------------------------------------------- //
// Automatic store

object
frame::allocate(context& cxt, const decl* d)
{
  value val = store::allocate(cxt, d);
  bindings.emplace_back(new binding(d, val));
  return bindings.back()->second;
}

value
frame::materialize(context& cxt, const type* t)
{
  return store::materialize(cxt, t);
}

object
frame::bind(const decl* d, value v)
{
  bindings.emplace_back(new binding(d, v));
  return bindings.back()->second;
}

object
automatic_store::allocate(context& cxt, const decl* d)
{
  assert(current_frame() && "allocation outside of frame");
  frame* f = current_frame();
  object obj = f->allocate(cxt, d);
  bind(d, f, f->num_bindings() - 1);
  return obj;
}

value
automatic_store::materialize(context& cxt, const type* t)
{
  assert(current_frame() && "materialization outside of frame");
  return current_frame()->materialize(cxt, t);
}

const value&
automatic_store::lookup(const decl* d) const
{
  // FIXME: Should the binding be in the innermost frame?
  assert(map.count(d) && !map.find(d)->second.empty() && "no such declaration");
  const binding& top = innermost_binding(d);
  return top.first->get_value(top.second);
}

value&
automatic_store::lookup(const decl* d)
{
  // FIXME: Should the binding be in the innermost frame?
  assert(map.count(d) && !map.find(d)->second.empty() && "no such declaration");
  binding& top = innermost_binding(d);
  return top.first->get_value(top.second);
}

void
automatic_store::enter_frame(frame* f)
{
  stack.emplace(f);

  // Establish bindings for the existing declaration/value pair. This
  // makes the bound values "active" in the current frame.
  for (std::size_t i = 0; i < f->bindings.size(); ++i) {
    const decl* d = f->bindings[i]->first;
    bind(d, f, i);
  }
}

void
automatic_store::leave_frame()
{
  frame* f = current_frame();

  // Remove the innermost bindings for each frame.
  for (auto& p : f->bindings) {
    binding_stack& binds = bindings(p->first);
    assert(!binds.empty() && "empty binding stack");
    binds.pop();
  }

  // Remove the current frame.
  stack.pop();
}

/// Returns true if the innermost binding is in frame `f`.
static bool
has_binding(automatic_store::binding_stack& vals, frame* f)
{
  if (!vals.empty())
    return vals.top().first == f;
  else
    return false;
}

void
automatic_store::bind(const decl *d, frame* f, int n)
{
  auto result = map.emplace(d, binding_stack());
  binding_stack& binds = result.first->second;
  assert(result.second || !has_binding(binds, f) && "declaration already bound");
  binds.push(f, n);
}

auto
automatic_store::bindings(const decl* d) const -> const binding_stack&
{
  auto iter = map.find(d);
  assert(iter != map.end() && "unbound declaration");
  return iter->second;
}

auto
automatic_store::bindings(const decl* d) -> binding_stack&
{
  auto iter = map.find(d);
  assert(iter != map.end() && "unbound declaration");
  return iter->second;
}

auto
automatic_store::innermost_binding(const decl* d) const -> const binding&
{
  const binding_stack& binds = bindings(d);
  assert(!binds.empty() && "empty value stack");
  return binds.top();
}

auto
automatic_store::innermost_binding(const decl* d) -> binding&
{
  binding_stack& binds = bindings(d);
  assert(!binds.empty() && "empty value stack");
  return binds.top();
}

} // namespace beaker
