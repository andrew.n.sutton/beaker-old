#include "context.hpp"
#include "decls.hpp"

#include "lang.tu/decls_tu.hpp"
#include "lang.void/types_void.hpp"
#include "lang.bool/types_bool.hpp"
#include "lang.int/types_int.hpp"
#include "lang.store/types_store.hpp"
#include "lang.fn/types_fn.hpp"
#include "lang.array/types_array.hpp"

#include <unordered_set>


namespace beaker {

/// Hashing for terms.
template<typename T>
struct term_hash
{
  std::size_t operator()(const T& t) const noexcept 
  {
    hasher h;
    hash(h, &t);
    return h;
  }
};

/// Equality comparison for terms.
template<typename T>
struct term_eq
{
  bool operator()(const T& a, const T& b) const noexcept 
  {
    return equal(&a, &b);
  }
};

/// Guarantees that all inserted elements are unique.
template<typename T>
using canonical_set = std::unordered_set<T, term_hash<T>, term_eq<T>>;


/// Stores canonical sets. This is a PIMPL class to avoid including all
/// of the type headers in the context header.
///
/// \todo This needs to accept language options as a constructor argument.
struct context::canonicals
{
  /// \todo Use language options (target arch, ABI) to determine the
  /// default size and alignment of builtin types.
  canonicals();

  /// Creates a layout for an object with size s and alignmnet a. The
  /// type is passed directly if it is less than or equal to the max
  /// direct size.
  type::layout make_layout(int s, int a);

  int max_direct; // Essentially the size of a register.

  // Default layouts
  type::layout default_byte;
  type::layout default_int;
  type::layout default_ptr;

  // Canonical types
  lang_void::void_type void_ty;
  lang_bool::bool_type bool_ty;
  lang_int::nat_type nat_ty;
  lang_int::int_type int_ty;
  canonical_set<lang_store::ref_type> ref_ty;
  canonical_set<lang_fn::fn_type> fn_ty;
  canonical_set<lang_array::array_type> array_ty;
  canonical_set<lang_array::buf_type> buf_ty;
  canonical_set<lang_array::tuple_type> tuple_ty;
};

/// \todo Use language options (target arch, ABI) to determine the
/// default size and alignment of builtin types.
context::canonicals::canonicals()
  : max_direct(64),
    default_byte(make_layout(8, 8)),
    default_int(make_layout(32, 32)),
    default_ptr(make_layout(64, 64)),
    void_ty(),
    bool_ty(default_byte),
    nat_ty(default_int),
    int_ty(default_int)
{ }

type::layout 
context::canonicals::make_layout(int s, int a)
{ 
  return {s, a, s <= max_direct}; 
}


context::context(symbol_table& syms)
  : syms(&syms),
    canon(new canonicals()),
    tu(new lang_tu::tu_decl(0))
{ }

context::~context()
{
  delete canon;
  delete tu;
}

type::layout
context::get_byte_layout() const
{
  return canon->default_byte;
}

type::layout
context::get_int_layout() const
{
  return canon->default_int;
}

type::layout
context::get_ptr_layout() const
{
  return canon->default_ptr;
}

type::layout
context::get_layout(int s, int a)
{
  return canon->make_layout(s, a);
}

type*
context::get_void_type() const
{ 
  return &canon->void_ty; 
}

type*
context::get_bool_type() const
{ 
  return &canon->bool_ty; 
}

type*
context::get_nat_type() const
{ 
  return &canon->nat_ty; 
}

type*
context::get_int_type() const
{ 
  return &canon->int_ty; 
}

type*
context::get_reference_type(type* t)
{
  const type& ins = *canon->ref_ty.emplace(t).first;
  return const_cast<type*>(&ins);
}

type*
context::get_function_type(type_seq& ps, type* r)
{
  const type& ins = *canon->fn_ty.emplace(*this, ps, r).first;
  return const_cast<type*>(&ins);
}

type*
context::get_function_type(type_seq&& ps, type* r)
{
  const type& ins = *canon->fn_ty.emplace(*this, std::move(ps), r).first;
  return const_cast<type*>(&ins);
}

type*
context::get_function_type(decl_seq& ps, decl* r)
{
  type_seq ts;
  for (decl* p : ps) {
    auto* parm = static_cast<typed_decl*>(p);
    ts.push_back(parm->get_type());
  }
  auto* ret = static_cast<typed_decl*>(r);
  return get_function_type(std::move(ts), ret->get_type());
}

type*
context::get_buffer_type(type* t)
{
  const type& ins = *canon->buf_ty.emplace(*this, t).first;
  return const_cast<type*>(&ins);
}

type*
context::get_array_type(type* t, expr* n)
{
  return get_array_type(t, expr_seq{n});
}

type*
context::get_array_type(type* t, expr_seq&& ns)
{
  const type& ins = *canon->array_ty.emplace(*this, t, std::move(ns)).first;
  return const_cast<type*>(&ins);
}

type*
context::get_tuple_type() 
{
  const type& ins = *canon->tuple_ty.emplace(*this).first;
  return const_cast<type*>(&ins);
}

type*
context::get_tuple_type(type_seq&& ts) 
{
  const type& ins = *canon->tuple_ty.emplace(*this, std::move(ts)).first;
  return const_cast<type*>(&ins);
}

} // namespace beaker