#include "value.hpp"

#include <beaker/util/hash.hpp>
#include <beaker/ast/decls.hpp>
#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

// -------------------------------------------------------------------------- //
// Refereneces

const value& 
ref_value::get_subobject() const
{
  return const_cast<ref_value*>(this)->get_subobject();
}

/// \todo We may have different aggregate value structures.
///
/// \todo Should this be an algorithm on ref values or within?
value& 
ref_value::get_subobject()
{
  assert(!path.empty() && "subobject with empty path");
  value* val = obj;
  for (auto iter = path.begin(); iter != path.end(); ++iter) {
    assert(val->is_aggregate() && "non-aggregate field in subobject path");
    agg_value& agg = val->get_aggregate();
    val = &agg[*iter];
  }
  return *val;
}

const value& 
ref_value::get_object() const
{
  if (is_complete_object())
    return get_complete_object();
  else
    return get_subobject();
}

value& 
ref_value::get_object()
{
  if (is_complete_object())
    return get_complete_object();
  else
    return get_subobject();
}

ref_value
ref_value::extend(int n)
{
  ref_value ret = *this;
  ret.path.push_back(n);
  return ret;
}

ref_value
ref_value::extend(const path_type& p)
{
  ref_value ret = *this;
  ret.path.insert(ret.path.end(), p.begin(), p.end());
  return ret;
}

ref_value
ref_value::designate(path_type&& p)
{
  ref_value ret = *this;
  ret.path = std::move(p);
  return ret;
}

bool
ref_value::operator==(const ref_value& v) const
{
  return store == v.store && obj == v.obj && path == v.path;
}

bool
ref_value::operator!=(const ref_value& v) const
{
  return !(*this == v);
}



// -------------------------------------------------------------------------- //
// Aggregates

agg_value::agg_value(int n) 
  : len(n), elems(new value[n]) 
{
  std::fill(begin(), end(), value());
}

agg_value::agg_value(const agg_value& v) 
  : len(v.len), elems(new value[len])
{ 
  std::copy(v.begin(), v.end(), begin());
}

agg_value::agg_value(agg_value&& v) 
  : len(v.len), elems(v.elems)
{
  v.len = 0;
  v.elems = nullptr;
}

agg_value::~agg_value()
{
  delete[] elems;
}

agg_value&
agg_value::operator=(const agg_value& v)
{
  if (this != &v) {
    delete[] elems;
    len = v.len;
    elems = new value[len];
    std::copy(v.begin(), v.end(), begin());
  }
  return *this;
}

agg_value&
agg_value::operator=(agg_value&& v)
{
  if (this != &v) {
    delete[] elems;
    len = v.len;
    elems = v.elems;
    v.len = 0;
    v.elems = nullptr;
  }
  return *this;
}

const value&
agg_value::operator[](int n) const
{
  return elems[n]; 
}

value&
agg_value::operator[](int n)
{
  return elems[n]; 
}

bool
agg_value::operator==(const agg_value& v) const
{
  return std::equal(begin(), end(), v.begin(), v.end());
}

bool
agg_value::operator!=(const agg_value& v) const
{
  return !std::equal(begin(), end(), v.begin(), v.end());
}

const value*
agg_value::begin() const
{
  return elems; 
}

const value*
agg_value::end() const 
{
  return elems + len; 
}

value*
agg_value::begin()
{
  return elems; 
}

value*
agg_value::end()
{
  return elems + len; 
}

// -------------------------------------------------------------------------- //
// Value

void
hash(hasher& h, const value& v)
{
  hash(h, (int)v.get_kind()); // FIXME: Shouldn't have to cast.
  switch (v.get_kind()) {
    case uninit_value_kind:
    case void_value_kind:
      return;
    case int_value_kind:
      return hash(h, v.get_int());
    case float_value_kind:
      return hash(h, v.get_float());
    case ref_value_kind: {
      const ref_value& ref = v.get_reference();
      hash(h, ref.get_store());
      hash(h, &ref.get_complete_object());
      for (int n : ref.get_path())
        hash(h, n);
      hash(h, ref.get_path().size());
      return;
    }
    case fn_value_kind:
      return hash(h, (void*)v.get_function());
    case agg_value_kind: {
      const agg_value& agg = v.get_aggregate();
      for (const value& v : agg)
        hash(h, v);
      return;
    }
  }
  assert(false && "invalid value kind");
}

std::ostream&
operator<<(std::ostream& os, const value& val)
{
  switch (val.get_kind()) {
    case uninit_value_kind:
      return os << "<uninit>";
    case void_value_kind:
      return os << "<void>";
    case int_value_kind:
      return os << "<int " << val.get_int() << '>';
    case float_value_kind:
      return os << "<float " << val.get_float() << '>';
    case ref_value_kind: {
      const ref_value& ref = val.get_reference();
      os << "<ref ";
      if (ref.in_static_store())
        os << "static ";
      else
        os << "auto." << ref.get_frame() << ' ';
      os << &ref.get_complete_object();
      const auto& path = ref.get_path();
      for (auto iter = path.begin(); iter != path.end(); ++iter)
        os << ' ' << *iter;
      os << '>';
      return os;
    }
    case fn_value_kind: {
      const mapping_decl* fn = val.get_function();
      return os << "<fn " << pretty(fn->get_name()) 
                << " : " << pretty(fn->get_type()) 
                << '>';
    }
    case agg_value_kind: {
      const agg_value& agg = val.get_aggregate();
      os << "<agg";
      if (!agg.empty())
        os << ' ';
      for (auto iter = agg.begin(); iter != agg.end(); ++iter) {
        os << *iter;
        if (std::next(iter) != agg.end())
          os << ',';
      }
      os << '>';
      return os;
    }
  }
  assert(false && "invalid value kind");
}

} // namespace beaker45
