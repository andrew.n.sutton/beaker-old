#include "exprs.hpp"
#include "types.hpp"
#include "print.hpp"

#include "lang.void/exprs_void.hpp"
#include "lang.bool/exprs_bool.hpp"
#include "lang.int/exprs_int.hpp"
#include "lang.store/exprs_store.hpp"
#include "lang.fn/exprs_fn.hpp"
#include "lang.array/exprs_array.hpp"

#include <iostream>


namespace beaker {

void 
expr::print() const
{
  std::cerr << pretty(this) << '\n';
}

void 
expr::print_verbose() const
{
  std::cerr << verbose(this) << '\n';
}

bool
expr::has_type(type const* t) const
{
  return equal(get_type(), t);
}

bool 
is_object(const expr* e)
{
  return is_object(e->get_type());
}

bool 
is_reference(const expr* e)
{
  return is_reference(e->get_type());
}

bool
is_initializer(const expr* e)
{
  struct fn {
    bool operator()(const expr*) const { return false; }
    
    // Scalar initializers
    bool operator()(const lang_store::nop_init*) const { return true; }
    bool operator()(const lang_store::zero_init*) const { return true; }
    bool operator()(const lang_store::copy_init*) const { return true; }
    bool operator()(const lang_store::bind_init*) const { return true; }
    
    // Aggregate initializers
    bool operator()(const lang_array::array_init*) const { return true; }
    bool operator()(const lang_array::elem_init*) const { return true; }
    bool operator()(const lang_array::copy_init*) const { return true; }
  };
  return apply(e, fn{});
}

bool
is_empty_initializer(const expr* e)
{
  struct fn {
    bool operator()(const expr*) const { return false; }
    bool operator()(const lang_store::nop_init*) const { return true; }
    bool operator()(const lang_store::zero_init*) const { return true; }
    bool operator()(const lang_array::array_init*) const { return true; }
  };
  return apply(e, fn{});
}

bool
is_copy_initializer(const expr* e)
{
  struct fn {
    bool operator()(const expr*) const { return false; }
    bool operator()(const lang_store::copy_init*) const { return true; }
    bool operator()(const lang_store::bind_init*) const { return true; }
    bool operator()(const lang_array::copy_init*) const { return true; }
  };
  return apply(e, fn{});
}

bool
is_brace_initializer(const expr* e)
{
  struct fn {
    bool operator()(const expr*) const { return false; }
    bool operator()(const lang_array::elem_init*) const { return true; }
  };
  return apply(e, fn{});
}

bool
is_constant(const expr* e)
{
  return dynamic_cast<const lang_array::const_expr*>(e);
}

value
get_constant_value(const expr* e)
{
  assert(is_constant(e) && "not a constant expression");
  return static_cast<const lang_array::const_expr*>(e)->get_value();
}

} // namespace beaker
