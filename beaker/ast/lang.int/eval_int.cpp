#include "eval_int.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

using namespace lang_int;

value 
evaluate_expr(evaluator& eval, const int_expr* e)
{
  return value(e->get_signed());
}

/// \todo Handle overflow.
value
evaluate_expr(evaluator& eval, const lang_int::add_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() + rhs.get_int());
}

/// \todo Handle overflow.
value
evaluate_expr(evaluator& eval, const lang_int::sub_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() - rhs.get_int());
}

/// \todo Handle overflow.
value
evaluate_expr(evaluator& eval, const lang_int::mul_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() * rhs.get_int());
}

/// \todo Handle overflow.
value
evaluate_expr(evaluator& eval, const lang_int::div_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  if (rhs.get_int() == 0)
    throw std::runtime_error("integer division by 0");
  return value(lhs.get_int() / rhs.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_int::rem_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  if (rhs.get_int() == 0)
    throw std::runtime_error("integer division by 0");
  return value(lhs.get_int() % rhs.get_int());
}

/// \todo Handle overflow.
value
evaluate_expr(evaluator& eval, const lang_int::neg_expr* e)
{
  value op = evaluate(eval, e->get_operand());
  return value(-op.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_int::eq_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() == rhs.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_int::ne_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() != rhs.get_int());
}

/// \todo Handle the signed unsigned comparison.
value
evaluate_expr(evaluator& eval, const lang_int::lt_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() < rhs.get_int());
}

/// \todo Handle the signed unsigned comparison.
value
evaluate_expr(evaluator& eval, const lang_int::gt_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() > rhs.get_int());
}

/// \todo Handle the signed unsigned comparison.
value
evaluate_expr(evaluator& eval, const lang_int::le_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() <= rhs.get_int());
}

/// \todo Handle the signed unsigned comparison.
value
evaluate_expr(evaluator& eval, const lang_int::ge_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() >= rhs.get_int());
}


} // namespace beaker
