#pragma once

#include <beaker/ast/types.hpp>


namespace beaker {
namespace lang_int {

/// The base class of all natural, integer, and modular types.
///
/// \todo Consider compacting all of these types into a single class that
/// is parameterized by semantics. Note that e.g., int may or may not be 
/// distinct from int32
struct integral_type : base_type 
{
  using base_type::base_type;
};


/// The base class of all signed integer types.
struct signed_type : integral_type
{
  using integral_type::integral_type;
};


/// The base class of all unsigned integer types.
struct unsigned_type : integral_type
{
  using integral_type::integral_type;
};


/// The base class of all modular integer types.
struct modular_types : integral_type
{
  using integral_type::integral_type;
};


/// Represents the type `nat`. This is an unsigned, non-modular integer
/// type.
struct nat_type : unsigned_type
{
  using unsigned_type::unsigned_type;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};  


/// Represents the type `int`, which is a signed integer type.
struct int_type : signed_type
{
  using signed_type::signed_type;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};  

} // namespace lang_int
} // namespace beaker