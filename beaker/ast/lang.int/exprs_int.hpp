#pragma once

#include <beaker/ast/exprs.hpp>


namespace beaker {
namespace lang_int {

/// Represents integer literals. These are internally stored as the max-sized
/// integer in the host architecture. Their interpretation as natural or
/// integral depends on the expression's type.
///
/// \todo Use an arbitrary precision integer class.
struct int_expr : nullary_expr
{
  int_expr(type* t, std::intmax_t n) : nullary_expr(t), val(n) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the value as a signed integer.
  std::intmax_t get_signed() const { return val; }

  /// Returns the truth as an unsigned integer.
  std::intmax_t get_unsigned() const { return val; }
  
  std::intmax_t val;
};


/// Represents the expression `e1 + e2`.
struct add_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 - e2`.
struct sub_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 * e2`.
struct mul_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 / e2`.
struct div_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 % e2`.
struct rem_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `-e1`.
struct neg_expr : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 == e2`.
struct eq_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 != e2`.
struct ne_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 < e2`.
struct lt_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 > e2`.
struct gt_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 <= e2`.
struct le_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 >= e2`.
struct ge_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


} // namespace lang_int
} // namespace beaker