#pragma once

#include "types_int.hpp"
#include "exprs_int.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_type(pretty_printer&, const lang_int::nat_type*);
void print_type(pretty_printer&, const lang_int::int_type*);

void print_expr(pretty_printer&, const lang_int::int_expr*);
void print_expr(pretty_printer&, const lang_int::add_expr*);
void print_expr(pretty_printer&, const lang_int::sub_expr*);
void print_expr(pretty_printer&, const lang_int::mul_expr*);
void print_expr(pretty_printer&, const lang_int::div_expr*);
void print_expr(pretty_printer&, const lang_int::rem_expr*);
void print_expr(pretty_printer&, const lang_int::neg_expr*);
void print_expr(pretty_printer&, const lang_int::eq_expr*);
void print_expr(pretty_printer&, const lang_int::ne_expr*);
void print_expr(pretty_printer&, const lang_int::lt_expr*);
void print_expr(pretty_printer&, const lang_int::gt_expr*);
void print_expr(pretty_printer&, const lang_int::le_expr*);
void print_expr(pretty_printer&, const lang_int::ge_expr*);

} // namespace beaker

