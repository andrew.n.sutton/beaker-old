#include "print_int.hpp"


namespace beaker {

using namespace lang_int;

void 
print_type(pretty_printer& pp, const nat_type*)
{
  print(pp, "nat");
}

void 
print_type(pretty_printer& pp, const int_type*)
{
  print(pp, "int");
}

void
print_expr(pretty_printer& pp, const int_expr* e)
{
  if (is_unsigned(e->get_type()))
    print(pp, e->get_unsigned());
  else
    print(pp, e->get_signed());
}

void
print_expr(pretty_printer& pp, const add_expr* e)
{
  print_infix_expr(pp, e, "+");
}

void
print_expr(pretty_printer& pp, const sub_expr* e)
{
  print_infix_expr(pp, e, "-");
}

void
print_expr(pretty_printer& pp, const mul_expr* e)
{
  print_infix_expr(pp, e, "*");
}

void
print_expr(pretty_printer& pp, const div_expr* e)
{
  print_infix_expr(pp, e, "/");
}

void
print_expr(pretty_printer& pp, const rem_expr* e)
{
  print_infix_expr(pp, e, "%");
}

void
print_expr(pretty_printer& pp, const neg_expr* e)
{
  print_prefix_expr(pp, e, "-");
}

void
print_expr(pretty_printer& pp, const eq_expr* e)
{
  print_infix_expr(pp, e, "==");
}

void
print_expr(pretty_printer& pp, const ne_expr* e)
{
  print_infix_expr(pp, e, "!=");
}

void
print_expr(pretty_printer& pp, const lt_expr* e)
{
  print_infix_expr(pp, e, "<");
}

void
print_expr(pretty_printer& pp, const gt_expr* e)
{
  print_infix_expr(pp, e, ">");
}

void
print_expr(pretty_printer& pp, const le_expr* e)
{
  print_infix_expr(pp, e, "<=");
}

void
print_expr(pretty_printer& pp, const ge_expr* e)
{
  print_infix_expr(pp, e, ">=");
}


} // namespace beaker

