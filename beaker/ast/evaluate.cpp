#include "evaluate.hpp"
#include "context.hpp"
#include "print.hpp"

#include "lang.tu/decls_tu.hpp"
#include "lang.void/eval_void.hpp"
#include "lang.bool/eval_bool.hpp"
#include "lang.int/eval_int.hpp"
#include "lang.store/eval_store.hpp"
#include "lang.fn/eval_fn.hpp"
#include "lang.io/eval_io.hpp"
#include "lang.array/eval_array.hpp"

#include <iostream>


namespace beaker {

object
evaluator::allocate_static(const decl* d)
{
  return cxt->allocate_static(d); 
}

const value&
evaluator::lookup_static(const decl* d) const
{
  return cxt->lookup_static(d);
}

value&
evaluator::lookup_static(const decl* d)
{
  return cxt->lookup_static(d);
}

object
evaluator::allocate_automatic(const decl* d)
{
  return stack.allocate(*cxt, d);
}

const value&
evaluator::lookup_automatic(const decl* d) const
{
  return stack.lookup(d);
}

value&
evaluator::lookup_automatic(const decl* d)
{
  return stack.lookup(d);
}

object
evaluator::allocate(const decl* d)
{
  if (has_automatic_storage(d))
    return allocate_automatic(d);
  else
    return allocate_static(d);
}

object
evaluator::allocate(frame* f, const decl* d)
{
  if (has_static_storage(d))
    return allocate_static(d);
  else
    return f->allocate(*cxt, d);
}

value
evaluator::materialize(const type* t)
{
  return stack.materialize(*cxt, t);
}

const value&
evaluator::fetch(const value& ref) const
{
  return fetch(const_cast<value&>(ref));
}

value&
evaluator::fetch(value& ref) const
{
  ref_value r = ref.get_reference();
  if (r.in_static_store()) {
    throw std::logic_error("static store references not implemented");
  }
  else {
    if (r.get_frame() >= stack.frames())
      throw std::runtime_error("reference to invalid object");
  }
  return ref.get_referenced_object();
}

/// Dispatches to `evaluate_expr(eval, e)`. Note that initializers are not
/// evaluated like normal expressions. 
value
evaluate(evaluator& eval, const expr* e)
{
  struct fn {
    evaluator& eval;
    value operator()(const expr* e) { throw std::logic_error("unreachable"); }
#define def_init(E, NS)
#define def_expr(E, NS) \
    value operator()(const NS::E ## _expr* e) { return evaluate_expr(eval, e); }
#include "exprs.def"
#undef def_expr
#undef def_init
  };
  return apply(e, fn{eval});
}

void
evaluate(evaluator& eval, const decl* d)
{
  // Dispatches to evaluate_decl(eval, d).
  struct fn {
    evaluator& eval;
    void operator()(const decl*) { throw std::logic_error("unreachable"); }
#define def_parm(D, NS)
#define def_decl(D, NS) \
    void operator()(const NS::D ## _decl* d) { evaluate_decl(eval, d); }
#include "decls.def"
#undef def_decl
#undef def_parm
  };
  apply(d, fn{eval});
}

flow
evaluate(evaluator& eval, const stmt* s)
{
  // Dispatches to evaluate_stmt(eval, s).
  struct fn {
    evaluator& eval;
    flow operator()(const stmt*) { throw std::logic_error("unreachable"); }
#define def_stmt(S, NS) \
    flow operator()(const NS::S ## _stmt* s) { return evaluate_stmt(eval, s); }
#include "stmts.def"
#undef def_stmt
  };
  return apply(s, fn{eval});
}

value 
evaluate_expr(evaluator& eval, const expr* e)
{
  throw std::logic_error("undefined expression evaluation");
}

void 
evaluate_decl(evaluator& eval, const decl* d)
{
  throw std::logic_error("undefined declaration evaluation");
}

flow 
evaluate_stmt(evaluator& eval, const stmt* s)
{
  throw std::logic_error("undefined statement evaluation");
}


// -------------------------------------------------------------------------- //
// Initialization

/// Select an appropriate initialization method for the given memory location.
void
initialize(evaluator& eval, object& obj, const expr* e)
{
  // Dispatches to evaluate_init(eval, obj, e).
  struct fn {
    evaluator& eval;
    object& obj;
    void operator()(const expr* e) { throw std::logic_error("unreachable initialization"); }
#define def_expr(E, NS)
#define def_init(I, NS) \
    void operator()(const NS::I ## _init* e) { return evaluate_init(eval, obj, e); }
#include "exprs.def"
#undef def_init
#undef def_expr
  };
  apply(e, fn{eval, obj});
}

void
evaluate_init(evaluator& eval, object& obj, const expr* e)
{
  throw std::logic_error("undefined initialization");
}

// -------------------------------------------------------------------------- //
// Common evaluation functions

/// Note that this can only return references to static objects. 
/// \todo Catch exceptions or no?
value
evaluate(context& cxt, const expr* e)
{
  evaluator eval(cxt);
  stack_frame this_frame(eval);
  value ret = evaluate(eval, e);
  if (ret.is_reference())
    return eval.fetch(ret);
  else
    return ret;
}

std::intmax_t
evaluate_as_signed(context& cxt, const expr* e)
{
  assert(is_signed(e->get_type()) && "expression does not have signed integer type");
  value v = evaluate(cxt, e);
  return v.get_int();
}

std::uintmax_t
evaluate_as_unsigned(context& cxt, const expr* e)
{
  assert(is_signed(e->get_type()) && "expression does not have unsigned integer type");
  value v = evaluate(cxt, e);
  return (std::uintmax_t)v.get_int();
}

expr*
evaluate_constant(context& cxt, const expr* e)
{
  expr* s = const_cast<expr*>(e);
  type* t = s->get_type();
  try {
    value v = evaluate(cxt, e);
    return new lang_array::const_expr(t, s, v);
  }
  catch (...) {
    return nullptr;
  }
}

} // namespace beaker
