#pragma once

#include "stmts_io.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_stmt(pretty_printer& pp, const lang_io::print_stmt* e);
void print_stmt(pretty_printer& pp, const lang_io::scan_stmt* e);

} // namespace beaker

