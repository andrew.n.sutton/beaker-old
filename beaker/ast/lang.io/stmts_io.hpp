#pragma once

#include <beaker/ast/stmts.hpp>


namespace beaker {

struct expr;

namespace lang_io {

/// Represents the statement `print e`. This prints the textual representation
/// of `e` to the default output file. 
struct print_stmt : stmt
{
  print_stmt(expr* e) : val(e) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the value to print.
  const expr* get_value() const { return val; }
  expr* get_value() { return val; }

  expr* val;
};

/// Represents statements of the form `scan e`. This reads the textual
/// representation for a value of type `e` and stores that in the object
/// referred to be `e`.
struct scan_stmt : stmt
{
  scan_stmt(expr* e) : ref(e) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the value to print.
  const expr* get_reference() const { return ref; }
  expr* get_reference() { return ref; }

  expr* ref;
};


} // namespace lang_io
} // namespace beaker
