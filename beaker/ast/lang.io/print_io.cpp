#include "print_io.hpp"


namespace beaker {

using namespace lang_io;

void
print_stmt(pretty_printer& pp, const lang_io::print_stmt* e)
{
  print(pp, "print");
  pp.space();
  print(pp, e->get_value());
}

void
print_stmt(pretty_printer& pp, const lang_io::scan_stmt* e)
{
  print(pp, "scan");
  pp.space();
  print(pp, e->get_reference());
}
  
} // namespace beaker
