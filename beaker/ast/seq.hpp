#pragma once

#include <beaker/util/hash.hpp>

#include <algorithm>
#include <stdexcept> // FIXME: This shouldn't be here.
#include <vector>


namespace beaker
{

// -------------------------------------------------------------------------- //
// General sequences

/// A sequence is simply a vector of AST nodes.
template<typename T>
using seq = std::vector<T*>;

struct type;
struct expr;
struct decl;
struct stmt;

using type_seq = seq<type>;
using expr_seq = seq<expr>;
using decl_seq = seq<decl>;
using stmt_seq = seq<stmt>;


// -------------------------------------------------------------------------- //
// Operations

/// Two sequences are equal if they have equal elements.
template<typename T>
inline bool
equal(const seq<T>& a, const seq<T>& b)
{
  auto cmp = [](const T* p, const T* q) { return equal(p, q); };
  return std::equal(a.begin(), a.end(), b.begin(), b.end(), cmp);
}

/// Hash the elements of the sequence.
template<typename T>
inline void
hash(hasher& h, const seq<T>& s)
{
  for (const T* t : s)
    hash(h, t);
  hash(h, s.size());
}

} // namespace beaker
