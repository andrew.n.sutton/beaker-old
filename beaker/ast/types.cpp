#include "types.hpp"
#include "print.hpp"

#include "lang.void/types_void.hpp"
#include "lang.bool/types_bool.hpp"
#include "lang.int/types_int.hpp"
#include "lang.store/types_store.hpp"
#include "lang.fn/types_fn.hpp"
#include "lang.array/types_array.hpp"

#include <cassert>
#include <iostream>
#include <typeinfo>


namespace beaker
{

void 
type::print() const
{
  std::cerr << pretty(this) << '\n';
}

void 
type::print_verbose() const
{
  std::cerr << verbose(this) << '\n';
}


// -------------------------------------------------------------------------- //
// Equality comparison

bool
equal(const type* a, const type* b)
{
  // Dispatches to equal_type(a, b) with both operands cast appropriately.
  struct fn {
    bool operator()(const type*) { 
      throw std::logic_error("unreachable"); 
    }

#define def_type(T, NS) \
    bool operator()(const NS::T ## _type* a) { \
      return equal_type(a, static_cast<const NS::T ## _type*>(b)); \
    }
#include "types.def"
#undef def_type
    
    const type* b;
  };

  if (a == b)
    return true;
  if (typeid(*a) != typeid(*b))
    return false;
  if (a->get_size_bits() != b->get_size_bits() ||
      a->get_alignment_bits() != b->get_alignment_bits() ||
      a->is_direct() != b->is_direct())
    return false;
  return apply(a, fn{b});
}

/// Compares two types for which no comparison method is defined. This is
/// undefined behavior.
bool
equal_type(const type*, const type*)
{
  throw std::logic_error("undefined behavior");
}

/// By default, base types are always equal.
bool
equal_type(const base_type* a, const base_type* b)
{
  return true;
}

// -------------------------------------------------------------------------- //
// Hashing

void
hash(hasher& h, const type* t)
{
  // Dispatches to hash_type(h, t).
  struct fn {
    void operator()(const type*) { 
      throw std::logic_error("unreachable"); 
    }

#define def_type(T, NS) \
    void operator()(const NS::T ## _type* t) { hash_type(h, t); }
#include "types.def"
#undef def_type
    
    hasher& h;
  };

  hash(h, typeid(*t));
  apply(t, fn{h});
}

/// Undefined hashing for the given type.
void
hash_type(hasher& h, const type*)
{
  throw std::logic_error("undefined behavior");
}

/// By default, no additional information needs to be added.
void
hash_type(hasher& h, const base_type* t)
{ }


// -------------------------------------------------------------------------- //
// Type classification

bool
is_void(const type* t)
{
  return dynamic_cast<const lang_void::void_type*>(t);
}

bool
is_bool(const type* t)
{
  return dynamic_cast<const lang_bool::bool_type*>(t);
}

bool
is_integral(const type* t)
{
  struct fn {
    bool operator()(const type*) const { return false; }
    bool operator()(const lang_int::nat_type*) const { return true; }
    bool operator()(const lang_int::int_type*) const { return true; }
  };
  return apply(t, fn{});
}

bool 
is_signed(const type* t)
{
  return dynamic_cast<const lang_int::signed_type*>(t);
}

bool 
is_unsigned(const type* t)
{
  return dynamic_cast<const lang_int::unsigned_type*>(t);
}

bool
is_reference(const type* t)
{
  return dynamic_cast<const lang_store::ref_type*>(t);
}

bool
is_function(const type* t)
{
  return dynamic_cast<const lang_fn::fn_type*>(t);
}

bool
is_array(const type* t)
{
  return dynamic_cast<const lang_array::array_type*>(t);
}

bool
is_buffer(const type* t)
{
  return dynamic_cast<const lang_array::buf_type*>(t);
}

bool
is_tuple(const type* t)
{
  return dynamic_cast<const lang_array::tuple_type*>(t);
}

const type*
get_object_type(const type* t)
{
  return get_object_type(const_cast<type*>(t));
}

type*
get_object_type(type* t)
{
  if (auto* ref = dynamic_cast<const lang_store::ref_type*>(t))
    return ref->get_object_type();
  else
    return t;
}

const type*
get_element_type(const type* t)
{
  return get_element_type(const_cast<type*>(t));
}

type*
get_element_type(type* t)
{
  struct fn {
    type* operator()(type* t) { assert(false && "unreachable"); }
    type* operator()(lang_array::array_type* t) { return t->get_element_type(); }
    type* operator()(lang_array::buf_type* t) { return t->get_element_type(); }
  };
  return apply(t, fn{});
}

} // namespace beaker
