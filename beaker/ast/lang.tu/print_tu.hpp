#pragma once

#include "decls_tu.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_decl(pretty_printer&, const lang_tu::tu_decl*);

} // namespace beaker

