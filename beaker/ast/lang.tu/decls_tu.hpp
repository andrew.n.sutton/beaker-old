#pragma once

#include <beaker/ast/decls.hpp>


namespace beaker {
namespace lang_tu {

/// Represents the declaration of a translation unit. This is the top-level
/// declaration of an individual translation.
///
/// \todo Should the translation have an output name? We'll actually need
/// this for codegen, although we could probably also get that from the
/// language options.
///
/// \todo This probably has a lot of useful information (imports, exports,
/// etc).
struct tu_decl : decl
{
  tu_decl(uid id) : decl(id, dc()) { }
  ~tu_decl();

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the module's declarations.
  const decl_seq& get_declarations() const { return decls; }
  decl_seq& get_declarations() { return decls; }

  decl_seq decls;
};

inline
tu_decl::~tu_decl() 
{ 
  for (decl* d : decls) 
    delete d; 
}

} // namespace lang_tu
} // namespace beaker

