#include "print_tu.hpp"


namespace beaker {

using namespace lang_tu;

void
print_decl(pretty_printer& pp, const tu_decl* tu)
{
  for (const decl* d : tu->get_declarations()) {
    print(pp, d);
    pp.newline();
  }
}


} // namespace beaker
