#pragma once

#include <beaker/ast/exprs.hpp>


namespace beaker {
namespace lang_fn {

/// Represents the expression `e0(e1, e2, ..., en)`.
struct call_expr : expr
{
  call_expr(type* t, expr* f, const expr_seq& as) : expr(t), fn(f), args(as) { }
  call_expr(type* t, expr* f, expr_seq&& as) : expr(t), fn(f), args(std::move(as)) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the function being called.
  expr* get_function() const { return fn; }

  /// Returns the arguments of the function call.
  const expr_seq& get_arguments() const { return args; }
  expr_seq& get_arguments() { return args; }

  expr* fn;
  expr_seq args;
};


/// Represents the expression `e1 == e2`.
struct eq_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the expression `e1 != e2`.
struct ne_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};

} // namespace lang_fn
} // namespace beaker
