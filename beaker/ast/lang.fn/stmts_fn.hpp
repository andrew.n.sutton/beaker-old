#pragma once

#include <beaker/ast/stmts.hpp>


namespace beaker {
namespace lang_fn {

/// Represents block or compound statements.
struct block_stmt : stmt
{
  block_stmt() = default;
  block_stmt(const stmt_seq& ss) : stmts(std::move(ss)) { }
  block_stmt(stmt_seq&& ss) : stmts(std::move(ss)) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the statements in the block.
  const stmt_seq& get_statements() const { return stmts; }
  stmt_seq& get_statements() { return stmts; }

  stmt_seq stmts;
};


/// Represents an if statement.
struct if_stmt : stmt
{
  if_stmt(expr* e, stmt* t, stmt* f) : cond(e), tb(t), fb(f) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  const expr* get_condition() const { return cond; }
  expr* get_condition() { return cond; }

  const stmt* get_true_branch() const { return tb; }
  stmt* get_true_branch() { return tb; }

  const stmt* get_false_branch() const { return fb; }
  stmt* get_false_branch() { return fb; }

  expr* cond;
  stmt* tb;
  stmt* fb;
};


/// Represents a while loop.
struct while_stmt : stmt
{
  while_stmt(expr* e, stmt* s) : cond(e), body(s) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  const expr* get_condition() const { return cond; }
  expr* get_condition() { return cond; }

  const stmt* get_body() const { return body; }
  stmt* get_body() { return body; }

  expr* cond;
  stmt* body;
};


/// Represents a break statement.
struct break_stmt : stmt
{
  using stmt::stmt;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents a continue statement.
struct cont_stmt : stmt
{
  using stmt::stmt;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents statements that return a value. Note that a return statement
/// initializes a return declaration.
struct ret_stmt : stmt
{
  ret_stmt(expr* e) : ret(e) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the returned expression.
  const expr* get_return_value() const { return ret; }
  expr* get_return_value() { return ret; }

  expr* ret;
};


/// Represents statements that evaluate an expression.
struct expr_stmt : stmt
{
  expr_stmt(expr* e) : ex(e) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the expression.
  const expr* get_expression() const { return ex; }
  expr* get_expression() { return ex; }

  expr* ex;
};


/// Represents statements that declares an entity.
struct decl_stmt : stmt
{
  decl_stmt(decl* d) : ent(d) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the declaration.
  const decl* get_declaration() const { return ent; }
  decl* get_declaration() { return ent; }

  decl* ent;
};

} // namespace lang_fn
} // namespace beaker

