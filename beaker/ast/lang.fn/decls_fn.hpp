#pragma once

#include "types_fn.hpp"

#include <beaker/ast/decls.hpp>


namespace beaker {
namespace lang_fn {


/// A function declaration.
struct fn_decl : mapping_decl
{
  using mapping_decl::mapping_decl;

  /// Returns the type of the function.
  const fn_type* get_type() const override;
  fn_type* get_type() override;

  /// Returns the return type of the function.
  const type* get_return_type() const;
  type* get_return_type();

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the declaration of a constant value. While constants have 
/// storage duration, that storage is not accessible (i.e., similar to a 
/// temporary).
struct const_decl : value_decl
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the declaration of a mutable object. A variable of object or
/// function type has associated storage for a value of that type. 
struct var_decl : value_decl
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents a materialized temporary object. Note that temporaries are
/// unnamed.
struct tmp_decl : value_decl
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents an immutable input to a function. Input parameters bind directly
/// to their arguments; they do not (necessarily) have storage.
struct in_parm : value_decl, parm
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};

/// Represents a mutable input to a function. Variable parameters have local
/// storage within the stack frame of the called function, which allows them
/// to be modified locally. 
struct var_parm : value_decl, parm
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the return value of a function. Return values bind directly
/// to a value, they do not (necessarily) have storage.
struct ret_parm : value_decl, parm
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents an output of a function. Output parameters are references to 
/// uninitialized storage. They can also be declared as inputs to the function.
struct out_parm : value_decl, parm
{
  using value_decl::value_decl;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};



} // namespace lang_fn
} // namespace beaker

