#pragma once

#include "exprs_fn.hpp"
#include "types_fn.hpp"
#include "decls_fn.hpp"
#include "stmts_fn.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_type(pretty_printer&, const lang_fn::fn_type*);

void print_expr(pretty_printer&, const lang_fn::call_expr*);
void print_expr(pretty_printer&, const lang_fn::eq_expr*);
void print_expr(pretty_printer&, const lang_fn::ne_expr*);

void print_decl(pretty_printer&, const lang_fn::fn_decl*);
void print_decl(pretty_printer&, const lang_fn::const_decl*);
void print_decl(pretty_printer&, const lang_fn::var_decl*);
void print_decl(pretty_printer&, const lang_fn::in_parm*);
void print_decl(pretty_printer&, const lang_fn::var_parm*);
void print_decl(pretty_printer&, const lang_fn::ret_parm*);
void print_decl(pretty_printer&, const lang_fn::out_parm*);

void print_stmt(pretty_printer&, const lang_fn::block_stmt*);
void print_stmt(pretty_printer&, const lang_fn::if_stmt*);
void print_stmt(pretty_printer&, const lang_fn::while_stmt*);
void print_stmt(pretty_printer&, const lang_fn::break_stmt*);
void print_stmt(pretty_printer&, const lang_fn::cont_stmt*);
void print_stmt(pretty_printer&, const lang_fn::ret_stmt*);
void print_stmt(pretty_printer&, const lang_fn::decl_stmt*);
void print_stmt(pretty_printer&, const lang_fn::expr_stmt*);

} // namespace beaker

