#include "decls_fn.hpp"
#include "types_fn.hpp"

#include <cassert>


namespace beaker {
namespace lang_fn {

const fn_type*
fn_decl::get_type() const
{
  return static_cast<const fn_type*>(ty);
}

fn_type*
fn_decl::get_type()
{
  return static_cast<fn_type*>(ty);
}


const type*
fn_decl::get_return_type() const
{
  return get_type()->get_return_type();
}

type*
fn_decl::get_return_type()
{
  return get_type()->get_return_type();
}


} // namespace lang_fn
} // namespace beaker
