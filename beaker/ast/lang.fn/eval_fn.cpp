#include "eval_fn.hpp"
#include "print_fn.hpp"

#include <iostream>


namespace beaker {

using namespace lang_fn;

// Make sure that we're returning a valid value.
static value
check_return_value(evaluator& eval, const fn_decl* fn)
{
  value ret = eval.lookup_automatic(fn->get_return());

  // What we return depends on the declaration and its type.
  const decl* d = fn->get_return();
  const type* t = fn->get_return_type();

  // Void returns are valid.
  if (is_void(t))
    return void_value();

  // A non-void return value shall be initialized.
  if (ret.is_uninitialized())
    throw std::runtime_error("uninitialized return value");

  // A reference return shall not refer to a locally declared variable. 
  // Note that we consult the value, not the type since aggregate constants 
  // are passed indirectly.
  if (ret.is_reference()) {
    ref_value& ref = ret.get_reference();
    if (ref.in_automatic_store() && ref.get_frame() >= eval.stack_depth())
      throw std::runtime_error("returning reference to local");
  }
  
  if (is_constant(d))
    // Constants are returned directly.
    return ret;
  else if (is_variable(d))
    // References are returned directly; Objects must be loaded.
    //
    // FIXME: Aggregate return values may actually be passed indirectly as
    // output parameters; we'd return void.
    if (is_reference(t))
      return ret;
    else
      return eval.fetch(ret);
  else if (is_reference(t)) {
    // References are returned directly.
    return ret;
  }
  else
    throw std::logic_error("invalid return value");
}

value
evaluate_expr(evaluator& eval, const call_expr* e)
{
  assert(eval.stack_depth() && "no call stack");
  
  // FIXME: Make call max depth a language option.
  if (eval.stack_depth() == 512)
    throw std::runtime_error("exceeded maximum call stack depth");

  // Evaluate the callee. This gives us a function declaration.
  value val = evaluate(eval, e->get_function());
  const auto* fn = static_cast<const fn_decl*>(val.get_function());
  if (!fn->get_body())
    throw std::runtime_error("function not defined");

  const expr_seq& args = e->get_arguments();
  const decl_seq& parms = fn->get_parameters();
  const decl* ret = fn->get_return();

  frame* caller = eval.current_frame();
  frame* callee = eval.make_frame();

  // Allocate a return object in the caller's frame as a temporary and then
  // bind it to the return declaration. Note that direct return values do not
  // produce temporary storage. 
  //
  // FIXME: This isn't right. If the return value requires an object, it
  // will have been materialized prior to evaluating this expression. We
  // would just want to bind that address as the return value.
  value rv = caller->materialize(eval.get_context(), fn->get_return_type());
  callee->bind(ret, rv);

  // Initialize function parameters with their corresponding arguments.
  auto parm_iter = parms.begin();
  auto arg_iter = args.begin();
  while (parm_iter != parms.end() && arg_iter != args.end()) {
    const decl* parm = *parm_iter;
    const expr* arg = *arg_iter;

    // Allocate a parameter in the callee's frame and initialize it with
    // the value of the corresponding argument.
    object obj = eval.allocate(callee, parm);
    initialize(eval, obj, arg);

    ++parm_iter;
    ++arg_iter;
  }

  // FIXME: Handle default arguments, variadics, etc.
  assert(parm_iter == parms.end() && arg_iter == args.end() && "argument mismatch");

  // Push the frame on the stack, activating the parameter bindings.
  stack_frame this_frame(eval, fn, callee);

  // Evaluate the function body.
  evaluate(eval, fn->get_body());

  return check_return_value(eval, fn);
}

/// Returns integer 1 if the two functions are identical.
value
evaluate_expr(evaluator& eval, const eq_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_function() == rhs.get_function());
}

/// Returns integer 1 if the two functions are different.
value
evaluate_expr(evaluator& eval, const ne_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_function() == rhs.get_function());
}

/// Allocate the constant in the current frame and initialize it.
void
evaluate_decl(evaluator& eval, const const_decl* d)
{
  object obj = eval.allocate(d);
  initialize(eval, obj, d->get_initializer());
}

/// Allocate the variable in the current frame and initialize it.
void
evaluate_decl(evaluator& eval, const var_decl* d)
{
  object obj = eval.allocate(d);
  initialize(eval, obj, d->get_initializer());
}

flow
evaluate_stmt(evaluator& eval, const block_stmt* s)
{
  stack_frame this_frame(eval);
  for (stmt *s1 : s->get_statements()) {
    flow f = evaluate(eval, s1);
    if (f != flow_next)
      return f;
  }
  return flow_next;
}

/// Evaluate the if statement. Introduce a new frame for the true/false
/// branch evaluated.
flow
evaluate_stmt(evaluator& eval, const if_stmt* s)
{
  value v = evaluate(eval, s->get_condition());
  stack_frame this_frame(eval);
  if (v.get_int())
    return evaluate(eval, s->get_true_branch());
  else
    return evaluate(eval, s->get_false_branch());
}

/// Evaluate the while statement. Introduce a new frame for each evaluation
/// of the loop body.
flow
evaluate_stmt(evaluator& eval, const while_stmt* s)
{
  while (true) {
    value v = evaluate(eval, s->get_condition());
    if (v.get_int()) {
      stack_frame this_frame(eval);
      flow f = evaluate(eval, s->get_body());
      if (f == flow_continue)
        continue;
      if (f == flow_break)
        break;
      if (f == flow_return)
        return f;
    }
    else {
      break;
    }
  }
  return flow_next;
}

flow
evaluate_stmt(evaluator& eval, const break_stmt* s)
{
  return flow_break;
}

flow
evaluate_stmt(evaluator& eval, const cont_stmt* s)
{
  return flow_continue;
}

/// A return statement initializes the return value.
flow
evaluate_stmt(evaluator& eval, const ret_stmt* s)
{
  const auto* fn = static_cast<const fn_decl*>(eval.current_function());
  object ret = eval.lookup_automatic(fn->get_return());
  initialize(eval, ret, s->get_return_value());
  return flow_return;
}

flow
evaluate_stmt(evaluator& eval, const decl_stmt* s)
{
  evaluate(eval, s->get_declaration());
  return flow_next;
}

flow
evaluate_stmt(evaluator& eval, const expr_stmt* s)
{
  evaluate(eval, s->get_expression());
  return flow_next;
}


} // namespace beaker
