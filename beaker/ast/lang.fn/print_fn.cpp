#include "print_fn.hpp"

#include <cassert>


namespace beaker {

using namespace lang_fn;

void 
print_type(pretty_printer& pp, const fn_type* t)
{
  pp.print('(');
  print_comma_separated(pp, t->get_parameter_types());
  pp.print(')');
  print_binary_op(pp, "->");
  print(pp, t->get_return_type());
}

void
print_expr(pretty_printer& pp, const lang_fn::call_expr* e)
{
  print(pp, e->get_function());
  print(pp, '(');
  print_comma_separated(pp, e->get_arguments());
  print(pp, ')');
}

void
print_expr(pretty_printer& pp, const lang_fn::eq_expr* e)
{
  print_infix_expr(pp, e, "==");
}

void
print_expr(pretty_printer& pp, const lang_fn::ne_expr* e)
{
  print_infix_expr(pp, e, "=!");
}

void
print_decl(pretty_printer& pp, const fn_decl* d)
{
  pp.print("def");
  pp.space();
  print(pp, d->get_name());
  pp.print('(');
  print_comma_separated(pp, d->get_parameters());
  pp.print(')');
  print_binary_op(pp, "->");
  print(pp, d->get_return());
  if (const stmt* s = d->get_body()) {
    pp.space();
    print(pp, s);
  }
  else {
    pp.print(';');
  }
}

static void
print_value(pretty_printer& pp, const value_decl* d, const char* intro) 
{
  if (intro) {
    print(pp, intro);
    pp.space();
  }
  
  print(pp, d->get_type());
  pp.space();

  // FIXME: There should always be a name (but there isn't).
  if (d->get_name())
    print(pp, d->get_name());

  if (const expr* e = d->get_initializer()) {
    if (is_empty_initializer(e)) {
      // Do nothing.
    }
    else if (is_copy_initializer(e)) {
      pp.print_space();
      print_binary_op(pp, "=");
      print(pp, e);
    }
    else if (is_brace_initializer(e)) {
      pp.print_space();
      print(pp, e);
    }
    else {
      assert(false && "invalid initializer");
    }
  }
}

void
print_decl(pretty_printer& pp, const const_decl* d)
{
  print_value(pp, d, "const");
  print(pp, ';');
}

void
print_decl(pretty_printer& pp, const var_decl* d)
{
  print_value(pp, d, "var");
  print(pp, ';');
}

void
print_decl(pretty_printer& pp, const in_parm* d)
{
  print_value(pp, d, "in");
}

void
print_decl(pretty_printer& pp, const out_parm* d)
{
  print_value(pp, d, d->get_index() >= 0 ? "out" : nullptr);
}

void
print_decl(pretty_printer& pp, const var_parm* d)
{
  print_value(pp, d, "var");
}

void
print_decl(pretty_printer& pp, const ret_parm* d)
{
  print_value(pp, d, nullptr);
}


void
print_stmt(pretty_printer& pp, const block_stmt* s)
{
  const stmt_seq& ss = s->get_statements();

  print(pp, '{');
  if (!ss.empty())
    pp.indent();
  pp.newline();
  for (auto iter = ss.begin(); iter != ss.end(); ++iter) {
    print(pp, *iter);
    if (std::next(iter) == ss.end())
      pp.undent();
    pp.newline();
  }
  print(pp, '}');
}

/// \todo The indentation here is gross. Only indent if nested statements if 
/// they are not blocks.
void
print_stmt(pretty_printer& pp, const if_stmt* s)
{
  print(pp, "if");
  pp.space();
  print_enclosed_expr(pp, s->get_condition(), '(', ')');
  pp.indent();
  pp.newline();
  print(pp, s->get_true_branch());
  pp.undent();
  pp.newline();
  print(pp, "else");
  pp.indent();
  pp.newline();
  print(pp, s->get_false_branch());
  pp.undent();
  pp.newline();
}

void
print_stmt(pretty_printer& pp, const while_stmt* s)
{
  print(pp, "while");
  pp.space();
  print_enclosed_expr(pp, s->get_condition(), '(', ')');
  pp.newline();
  print(pp, s->get_body());
}

void
print_stmt(pretty_printer& pp, const break_stmt* s)
{
  print(pp, "break;");
}

void
print_stmt(pretty_printer& pp, const cont_stmt* s)
{
  print(pp, "continue;");
}

void
print_stmt(pretty_printer& pp, const ret_stmt* s)
{
  print(pp, "return");
  pp.space();
  print(pp, s->get_return_value());
  print(pp, ';');
}

void
print_stmt(pretty_printer& pp, const decl_stmt* s)
{
  print(pp, s->get_declaration());
}

void
print_stmt(pretty_printer& pp, const expr_stmt* s)
{
  print(pp, s->get_expression());
  print(pp, ';');
}


} // namespace beaker
