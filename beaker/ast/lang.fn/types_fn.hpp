#pragma once

#include <beaker/ast/types.hpp>


namespace beaker {
namespace lang_fn {

/// The type `(t1, t2, ..., tn) -> t0`.
///
/// \todo Function types are essentially pointers. Their size and alignment
/// must be the same as the pointer type of the target system.
struct fn_type : type
{
  fn_type(context& cxt, const type_seq& ps, type* r);
  fn_type(context& cxt, type_seq&& ps, type* r);

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the sequence of parameter types.
  const type_seq& get_parameter_types() const { return parms; }
  type_seq& get_parameter_types() { return parms; }

  /// Returns the return type.
  type* get_return_type() const { return ret; }

  type_seq parms;
  type* ret;
};

} // namespace lang_fn


// -------------------------------------------------------------------------- //
// Operations

/// Two function types are equal if their parameter and return types are equal.
inline bool
equal_type(const lang_fn::fn_type* a, const lang_fn::fn_type* b)
{
  return equal(a->get_parameter_types(), b->get_parameter_types()) &&
         equal(a->get_return_type(), b->get_return_type());
}

/// Add the parameter and return types to the hash.
inline void
hash_type(hasher& h, const lang_fn::fn_type* t)
{
  hash(h, t->get_parameter_types());
  hash(h, t->get_return_type());
}

} // namespace beaker

