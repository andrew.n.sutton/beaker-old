#include "types_fn.hpp"

#include <beaker/ast/context.hpp>

namespace beaker {
namespace lang_fn {

fn_type::fn_type(context& cxt, const type_seq& ps, type* r) 
  : type(cxt.get_ptr_layout()), parms(), ret(r) 
{ }

fn_type::fn_type(context& cxt, type_seq&& ps, type* r) 
  : type(cxt.get_ptr_layout()), parms(std::move(ps)), ret(r) 
{ }

} // namespace lang_fn
} // namespace beaker
