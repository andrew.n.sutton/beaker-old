#pragma once

#include "types_fn.hpp"
#include "exprs_fn.hpp"
#include "decls_fn.hpp"
#include "stmts_fn.hpp"

#include <beaker/ast/evaluate.hpp>


namespace beaker {

value evaluate_expr(evaluator& eval, const lang_fn::call_expr* e);
value evaluate_expr(evaluator& eval, const lang_fn::eq_expr* e);
value evaluate_expr(evaluator& eval, const lang_fn::ne_expr* e);

void evaluate_decl(evaluator& eval, const lang_fn::const_decl* d);
void evaluate_decl(evaluator& eval, const lang_fn::var_decl* d);

flow evaluate_stmt(evaluator& eval, const lang_fn::block_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::if_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::while_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::break_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::cont_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::ret_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::decl_stmt* s);
flow evaluate_stmt(evaluator& eval, const lang_fn::expr_stmt* s);


} // namespace beaker

