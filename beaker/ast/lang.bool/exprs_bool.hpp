#pragma once

#include <beaker/ast/exprs.hpp>


namespace beaker {
namespace lang_bool {

/// Represents boolean literals `true` and `false`.
struct bool_expr : nullary_expr
{
  bool_expr(type* t, bool b) : nullary_expr(t), val(b) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the truth value of the literal.
  bool get_value() const { return val; }
  
  bool val;
};


/// Represents the expression `e1 & e2`.
struct and_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the inclusive-or expression `e1 | e2`.
struct or_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the exclusive-or expression `e1 ^ e2`.
struct xor_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};

/// Represents the implication `e1 => e2`.
struct imp_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the equivalence `e1 == e2`.
struct eq_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the negation expression `!e1`.
struct not_expr : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the conditional expression `e1 ? e2 : e3`.
struct if_expr : ternary_expr
{
  using ternary_expr::ternary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the condition of the expressions.
  const expr* get_condition() const { return get_first(); }
  expr* get_condition() { return get_first(); }

  /// Returns the value when the condition is true.
  const expr* get_true_value() const { return get_second(); }
  expr* get_true_value() { return get_second(); }

  /// Returns the value when the condition is false.
  const expr* get_false_value() const { return get_third(); }
  expr* get_false_value() { return get_third(); }
};


/// Represents the conditional and expression `e1 && e2`.
struct and_if_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the conditional or expression `e1 || e2`.
struct or_if_expr : binary_expr
{
  using binary_expr::binary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the equivalence `assert e1`.
struct assert_expr : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


} // namespace lang_bool
} // namespace beaker
