#include "print_bool.hpp"


namespace beaker {

void 
print_type(pretty_printer& pp, const lang_bool::bool_type*)
{
  print(pp, "bool");
}

void
print_expr(pretty_printer& pp, const lang_bool::bool_expr* e)
{
  if (e->get_value())
    print(pp, "true");
  else
    print(pp, "false");
}

void
print_expr(pretty_printer& pp, const lang_bool::and_expr* e)
{
  print_infix_expr(pp, e, "&");
}

void
print_expr(pretty_printer& pp, const lang_bool::or_expr* e)
{
  print_infix_expr(pp, e, "|");
}

void
print_expr(pretty_printer& pp, const lang_bool::xor_expr* e)
{
  print_infix_expr(pp, e, "^");
}

void
print_expr(pretty_printer& pp, const lang_bool::imp_expr* e)
{
  print_infix_expr(pp, e, "=>");

}

void
print_expr(pretty_printer& pp, const lang_bool::eq_expr* e)
{
  print_infix_expr(pp, e, "==");
}

void
print_expr(pretty_printer& pp, const lang_bool::not_expr* e)
{
  print_prefix_expr(pp, e, "!");
}

void
print_expr(pretty_printer& pp, const lang_bool::if_expr* e)
{
  print_grouped_expr(pp, e->get_first());
  print_binary_op(pp, "?");
  print_grouped_expr(pp, e->get_second());
  print_binary_op(pp, ":");
  print_grouped_expr(pp, e->get_third());
}

void
print_expr(pretty_printer& pp, const lang_bool::and_if_expr* e)
{
  print_infix_expr(pp, e, "&&");

}

void
print_expr(pretty_printer& pp, const lang_bool::or_if_expr* e)
{
  print_infix_expr(pp, e, "||");

}

void
print_expr(pretty_printer& pp, const lang_bool::assert_expr* e)
{
  print_builtin_call_expr(pp, "assert", e->get_operand());
}


} // namespace beaker

