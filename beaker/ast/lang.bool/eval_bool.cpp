#include "eval_bool.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

using namespace lang_bool;

value 
evaluate_expr(evaluator& eval, const bool_expr* e)
{
  return value(e->get_value());
}

value
evaluate_expr(evaluator& eval, const lang_bool::and_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() & rhs.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_bool::or_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() | rhs.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_bool::xor_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() ^ rhs.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_bool::imp_expr* e)
{
  throw std::logic_error("logical implication not implemented");
}

value
evaluate_expr(evaluator& eval, const lang_bool::eq_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());
  return value(lhs.get_int() == rhs.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_bool::not_expr* e)
{
  value op = evaluate(eval, e->get_operand());
  return value(!op.get_int());
}

value
evaluate_expr(evaluator& eval, const lang_bool::if_expr* e)
{
  value cond = evaluate(eval, e->get_first());
  if (cond.get_int())
    return evaluate(eval, e->get_second());
  else
    return evaluate(eval, e->get_third());
}

value
evaluate_expr(evaluator& eval, const lang_bool::and_if_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  if (lhs.get_int())
    return evaluate(eval, e->get_rhs());
  else
    return lhs;
}

value
evaluate_expr(evaluator& eval, const lang_bool::or_if_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  if (lhs.get_int())
    return lhs;
  else
    return evaluate(eval, e->get_rhs());
}

value
evaluate_expr(evaluator& eval, const lang_bool::assert_expr* e)
{
  value cond = evaluate(eval, e->get_operand());
  if (!cond.get_int()) {
    // FIXME: Include a reasonable error message.
    throw std::runtime_error("assertion failed");
  }
  return cond;
}


} // namespace beaker
