#pragma once

#include "types_bool.hpp"
#include "exprs_bool.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_type(pretty_printer&, const lang_bool::bool_type*);

void print_expr(pretty_printer&, const lang_bool::bool_expr*);
void print_expr(pretty_printer&, const lang_bool::and_expr*);
void print_expr(pretty_printer&, const lang_bool::or_expr*);
void print_expr(pretty_printer&, const lang_bool::xor_expr*);
void print_expr(pretty_printer&, const lang_bool::imp_expr*);
void print_expr(pretty_printer&, const lang_bool::eq_expr*);
void print_expr(pretty_printer&, const lang_bool::not_expr*);
void print_expr(pretty_printer&, const lang_bool::if_expr*);
void print_expr(pretty_printer&, const lang_bool::and_if_expr*);
void print_expr(pretty_printer&, const lang_bool::or_if_expr*);
void print_expr(pretty_printer&, const lang_bool::assert_expr*);

} // namespace beaker

