#pragma once

#include "types_bool.hpp"
#include "exprs_bool.hpp"

#include <beaker/ast/evaluate.hpp>


namespace beaker {

value evaluate_expr(evaluator& eval, const lang_bool::bool_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::and_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::or_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::xor_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::imp_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::eq_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::not_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::if_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::and_if_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::or_if_expr* e);
value evaluate_expr(evaluator& eval, const lang_bool::assert_expr* e);

} // namespace beaker

