#pragma once

#include <beaker/ast/types.hpp>


namespace beaker {
namespace lang_bool {

/// The type `bool`.
struct bool_type : base_type
{
  using base_type::base_type;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};

} // namespace lang_bool
} // namespace beaker
