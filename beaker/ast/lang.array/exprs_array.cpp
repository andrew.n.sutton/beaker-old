#include "exprs_array.hpp"
#include "types_array.hpp"


namespace beaker {
namespace lang_array {

/// \todo Can the expression be value dependent.
proj_expr::proj_expr(type* t, expr* e1, expr* e2)
  : binary_expr(t, e1, e2)
{
  assert(dynamic_cast<const_expr*>(e2));
}

int 
proj_expr::get_index() const
{
  const const_expr* e = static_cast<const const_expr*>(get_lhs());
  return e->get_value().get_int();
}

const tuple_type* 
tuple_expr::get_type() const
{
  return static_cast<const tuple_type*>(nary_expr::get_type());
}

tuple_type* 
tuple_expr::get_type()
{
  return static_cast<tuple_type*>(nary_expr::get_type());
}

const array_type* 
array_init::get_type() const
{
  return static_cast<const array_type*>(unary_expr::get_type());
}

array_type* 
array_init::get_type()
{
  return static_cast<array_type*>(unary_expr::get_type());
}


} // namespace lang_array
} // namespace beaker
