#include "types_array.hpp"
#include "exprs_array.hpp"

#include <beaker/ast/context.hpp>

#include <algorithm>


namespace beaker {
namespace lang_array {

// -------------------------------------------------------------------------- //
// Arrays

/// Returns the integer extent of the array type.
static int
get_constant_extent(const expr* e)
{
  assert(dynamic_cast<const const_expr*>(e) && "not a constant expression");
  const const_expr* ce = static_cast<const const_expr*>(e);
  return ce->get_value().get_int();
}

/// Returns the product of values in the sequence.
static int
get_constant_size(const expr_seq& es)
{
  std::size_t elems = 1;
  std::for_each(es.begin(), es.end(), [&elems](const expr* e) {
    elems *= get_constant_extent(e);
  });
  return elems;
}

/// Compute the layout of an array. Note that this is the layout for all
/// homogeneous aggregate types. Consider moving this into the context.
static type::layout
get_layout(context& cxt, const type* t, const expr_seq& es)
{
  assert(!es.empty() && "array type with no bounds");
  int size = t->get_size_bits();
  int elems = get_constant_size(es);
  return cxt.get_layout(elems * size, t->get_alignment_bits());
}

array_type::array_type(context& cxt, type* t, const expr_seq& es)
  : type(get_layout(cxt, t, es)), elem(t), extents(es)
{ }

array_type::array_type(context& cxt, type* t, expr_seq&& es)
  : type(get_layout(cxt, t, es)), elem(t), extents(std::move(es))
{ }

const type*
array_type::get_subobject_type(context& cxt, const expr_seq& es) const
{
  return const_cast<array_type*>(this)->get_subobject_type(cxt, es);
}

/// \todo Support slice indexes. The return type when slicing is involved
/// is a little more interesting because you can't simply strip indexes
/// from the result (like we do now).
type*
array_type::get_subobject_type(context& cxt, const expr_seq& es)
{
  assert(es.size() <= get_rank() && "invalid index sequence");
  
  // Compute a new sequence of extents. For simple integer indexes, this
  // will essentially strip off the first es.size() bounds, yielding a 
  // subarray whose extents are those remaining. A slice either computes new
  // constant bound or a placeholder at the index in which it appears.
  expr_seq newext;
  auto ext_iter = extents.begin();
  auto ix_iter = es.begin();
  while (ix_iter != es.end()) {
    // FIXME: If the index is a slice, then insert a placeholder into the
    // sequence of new extents. Also, make sure that this actually makes sense.
    const expr* e = *ix_iter;
    if (!is_integral(e->get_type()))
      throw std::logic_error("index of non-integral type");
    ++ext_iter;
    ++ix_iter;
  }

  // Append all remaining bounds to the computed extents.
  while (ext_iter != extents.end()) {
    newext.push_back(*ext_iter);
    ++ext_iter;
  }

  // If we consumed all of the bounds, then return the element type.
  if (newext.empty())
    return elem;

  // FIXME: If we inserted any placeholders, the result type is a buffer.
  return cxt.get_array_type(elem, std::move(newext));
}

const type*
array_type::get_row_type(context& cxt) const
{
  return const_cast<array_type*>(this)->get_row_type(cxt);
}

type*
array_type::get_row_type(context& cxt)
{
  if (get_rank() == 1)
    return get_element_type();

  expr_seq es(get_rank() - 1);
  std::copy(++extents.begin(), extents.end(), es.begin());
  type* t = const_cast<type*>(get_element_type());
  return cxt.get_array_type(t, std::move(es));
}


int
array_type::get_extent(int k) const
{
  assert (0 <= k && k < extents.size() && "extent of invalid rank");
  return get_constant_extent(extents[k]);
}

std::vector<int> 
array_type::get_extents() const
{
  std::vector<int> ret(extents.size());
  std::transform(extents.begin(), extents.end(), ret.begin(), [](expr* e) {
    return get_constant_extent(e);
  });
  return ret;
}

int
array_type::get_size() const
{
  return get_constant_size(extents);
}

// -------------------------------------------------------------------------- //
// Buffers

buf_type::buf_type(context& cxt, type* t)
  : type(cxt.get_ptr_layout()), elem(t)
{ }

// -------------------------------------------------------------------------- //
// Tuples

/// Compute the layout for the tuple. Note that this is the layout for all 
/// heterogeneous aggregate types. Maybe we should move this into the context 
/// along with array layouts?.
static type::layout
get_layout(context& cxt, const type_seq& ts)
{
  int align = 0;
  int size = 0;
  for (const type* t : ts) {
    align = std::max(align, t->get_alignment_bits());
    size += std::max(t->get_size_bits(), t->get_alignment_bits());
  }
  return cxt.get_layout(size, align);
}

/// An empty tuple occupies 1 byte.
tuple_type::tuple_type(context& cxt)
  : type(cxt.get_byte_layout()), type_seq()
{ }

tuple_type::tuple_type(context& cxt, const type_seq& ts)
  : type(get_layout(cxt, ts)), type_seq(ts)
{ }

tuple_type::tuple_type(context& cxt, type_seq&& ts)
  : type(get_layout(cxt, ts)), type_seq(std::move(ts))
{ }

} // namespace lang_array


using namespace lang_array;

bool
equal_type(const array_type* a, const array_type* b)
{
  return equal(a->get_element_type(), b->get_element_type()) &&
         a->get_extents() == b->get_extents();
}

bool
equal_type(const buf_type* a, const buf_type* b)
{
  return equal(a->get_element_type(), b->get_element_type());
}

bool
equal_type(const tuple_type* a, const tuple_type* b)
{
  return equal(a->get_element_types(), b->get_element_types());
}

void
hash_type(hasher& h, const array_type* t)
{
  hash(h, t->get_element_type());
  for (int n : t->get_extents())
    hash(h, n);
}

void
hash_type(hasher& h, const buf_type* t)
{
  hash(h, t->get_element_type());
}

void
hash_type(hasher& h, const tuple_type* t)
{
  hash(h, t->get_element_types());
}

} // namespace beaker