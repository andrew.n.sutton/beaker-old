#include "print_array.hpp"


namespace beaker {

using namespace lang_array;

void 
print_type(pretty_printer& pp, const array_type* t)
{
  print(pp, t->get_element_type());
  print(pp, '[');
  print_comma_separated(pp, t->get_extent_expressions());
  print(pp, ']');
}

void 
print_type(pretty_printer& pp, const buf_type* t)
{
  print(pp, t->get_element_type());
  print(pp, "[]");
}

void 
print_type(pretty_printer& pp, const tuple_type* t)
{
  print(pp, '{');
  print_comma_separated(pp, t->get_element_types());
  print(pp, '}');
}

void 
print_expr(pretty_printer& pp, const const_expr* e)
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__const", e->get_expression());
  else
    print(pp, e->get_expression());
}

void 
print_expr(pretty_printer& pp, const index_expr* e)
{
  print_grouped_expr(pp, e->get_array());
  print(pp, '[');
  print_comma_separated(pp, e->get_indices());
  print(pp, ']');
}

void 
print_expr(pretty_printer& pp, const proj_expr* e)
{
  print_grouped_expr(pp, e->get_lhs());
  print_enclosed_expr(pp, e->get_rhs(), '[', ']');
}

void 
print_expr(pretty_printer& pp, const tuple_expr* e)
{
  print(pp, '{');
  print_comma_separated(pp, e->get_elements());
  print(pp, '}');
}

void 
print_expr(pretty_printer& pp, const array_init* e)
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__array", e->get_element_initializer());
  else
    print(pp, e->get_element_initializer());
}

/// \todo This is wrong.
void 
print_expr(pretty_printer& pp, const elem_init* e)
{
  print(pp, '{');
  print_comma_separated(pp, e->get_operands());
  print(pp, '}');
}

void 
print_expr(pretty_printer& pp, const copy_init* e)
{
  if (pp.verbose_output())
    print_builtin_call_expr(pp, "__copy", e->get_value());
  else
    print(pp, e->get_value());
}

} // namespace beaker

