#include "eval_array.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

using namespace lang_array;

/// Returns the cached value of the expression.
value
evaluate_expr(evaluator& eval, const const_expr* e)
{
  return e->get_value();
}

value
evaluate_expr(evaluator& eval, const index_expr* e)
{
  value array = evaluate(eval, e->get_array());

  // Compute the path to the sub-object.
  const expr_seq& args = e->get_indices();
  std::vector<int> path(args.size());
  std::transform(args.begin(), args.end(), path.begin(), [&](const expr* e) {
    return evaluate(eval, e).get_int();
  });

  // Build a new reference that designates the subobject. Append the computed
  // path to the previous path.
  ref_value& base = array.get_reference();
  ref_value ref = base.extend(path);

  // The return value depends on the type; either a reference to the sub
  // object or the object itself.
  const type* t = e->get_array()->get_type();
  if (is_reference(t))
    return ref;
  else
    return ref.get_object();
}

value
evaluate_expr(evaluator& eval, const proj_expr* e)
{
  value lhs = evaluate(eval, e->get_lhs());
  value rhs = evaluate(eval, e->get_rhs());

  // Build a new reference that designates the subobject.
  ref_value& base = lhs.get_reference();
  ref_value ref = base.extend(rhs.get_int());

  // The return value depends on the type; either a reference to the sub
  // object or the object itself.
  const type* t = e->get_lhs()->get_type();
  if (is_reference(t))
    return ref;
  else
  return ref.get_object();  
}

// A tuple expression should never be evaluated direction. They are only
// used as a placeholder for a subsequent aggregate or direct initialization.
value
evaluate_expr(evaluator& eval, const tuple_expr* e)
{
  throw std::logic_error("tuple evaluation");
}

/// Materialize a temporary and return its reference ()
value
evaluate_expr(evaluator& eval, const tmp_expr* e)
{
  assert(is_aggregate(e->get_type()) && "materialization of non-aggregate");
  return eval.materialize(e->get_type());
}

void 
evaluate_init(evaluator& eval, object& obj, const array_init* e)
{
  assert(obj.is_indirect() && "array initialization of direct value");
  
  const array_type* t = e->get_type();

  // Initialize each subobject of the array with the element initializer.
  agg_value& arr = obj->get_aggregate();
  assert(arr.size() == t->get_rows() && "ill-shaped array");
  std::for_each(arr.begin(), arr.end(), [&eval, e](value& v) {
    object obj = v;
    initialize(eval, obj, e->get_element_initializer());
  });
}

// For arrays, returns the number of rows, for tuples, the number of elements.
static std::size_t
get_aggregate_size(const type* t)
{
  if (const array_type* at = dynamic_cast<const array_type*>(t))
    return at->get_rows();
  else if (const tuple_type* tt = dynamic_cast<const tuple_type*>(t))
    return tt->size();
  else
    throw std::logic_error("invalid aggregate");
}

void
evaluate_init(evaluator& eval, object& obj, const elem_init* e)
{
  std::size_t n = get_aggregate_size(e->get_type());
  agg_value& agg = obj->get_aggregate();
  assert(agg.size() == n && "ill-shaped aggregate");
  auto init_iter = e->begin();
  auto val_iter = agg.begin();
  while (init_iter != e->end() && val_iter != agg.end()) {
    object sub = *val_iter;
    initialize(eval, sub, *init_iter);
    ++init_iter;
    ++val_iter;
  }
}

/// Recursively copy values from a source value to a destination value.
static void 
copy(value& dst, const value& src)
{
  switch (src.get_kind()) {
    default:
      // For scalars and references, this is just a copy.
      assert(!dst.is_aggregate() && "overwriting aggregate by scalar");
      dst = src;
      break;

    case agg_value_kind: {
      // For aggregates, the destination should already have been shaped,
      // so we want to recursively copy scalars and references.
      const agg_value& s = src.get_aggregate();
      agg_value& d = dst.get_aggregate();
      assert(s.size() == d.size() && "differently sized aggregates");
      auto si = s.begin();
      auto di = d.begin();
      while (si != s.end()) {
        copy(*di, *si);
        ++si;
        ++di;
      }
      break;
    }
  }
}

/// Copy initialize the referenced aggregate. This could either be a value
/// binding or copy. If the object holds its value directly, then this is a
/// value binding, and it will be initialized as a reference. If the object
/// holds the value indirectly, then this is a copy.
void
evaluate_init(evaluator& eval, object& obj, const copy_init* e)
{
  value ref = evaluate(eval, e->get_value());
  if (obj.is_direct())
    *obj = ref;
  else
    copy(*obj, eval.fetch(ref));
}

} // namespace beaker
