#pragma once

#include "types_array.hpp"
#include "exprs_array.hpp"

#include <beaker/ast/print.hpp>


namespace beaker {

void print_type(pretty_printer&, const lang_array::array_type*);
void print_type(pretty_printer&, const lang_array::buf_type*);
void print_type(pretty_printer&, const lang_array::tuple_type*);

void print_expr(pretty_printer&, const lang_array::const_expr*);
void print_expr(pretty_printer&, const lang_array::index_expr*);
void print_expr(pretty_printer&, const lang_array::proj_expr*);
void print_expr(pretty_printer&, const lang_array::tuple_expr*);
void print_expr(pretty_printer&, const lang_array::array_init*);
void print_expr(pretty_printer&, const lang_array::elem_init*);
void print_expr(pretty_printer&, const lang_array::copy_init*);

} // namespace beaker

