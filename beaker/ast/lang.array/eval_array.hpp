#pragma once

#include "types_array.hpp"
#include "exprs_array.hpp"

#include <beaker/ast/evaluate.hpp>


namespace beaker {

value evaluate_expr(evaluator& eval, const lang_array::const_expr* e);
value evaluate_expr(evaluator& eval, const lang_array::index_expr* e);
value evaluate_expr(evaluator& eval, const lang_array::proj_expr* e);
value evaluate_expr(evaluator& eval, const lang_array::tuple_expr* e);
value evaluate_expr(evaluator& eval, const lang_array::tmp_expr* e);

void evaluate_init(evaluator& eval, object& obj, const lang_array::array_init* e);
void evaluate_init(evaluator& eval, object& obj, const lang_array::elem_init* e);
void evaluate_init(evaluator& eval, object& obj, const lang_array::copy_init* e);

} // namespace beaker

