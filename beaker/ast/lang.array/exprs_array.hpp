#pragma once

#include <beaker/ast/exprs.hpp>


namespace beaker {
namespace lang_array {

struct array_type;
struct tuple_type;

/// A constant expression represents an expression whose constant value
/// has been computed. This is used to preserve source information about
/// certain language constructs (e.g., array type extents), when the underlying
/// construct depends only on the computed value.
///
/// \todo This belongs in a separate language module.
struct const_expr : unary_expr
{
  const_expr(type* t, expr* e, const value& v) : unary_expr(t, e), val(v) { }
  const_expr(type* t, expr* e, value&& v) : unary_expr(t, e), val(std::move(v)) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the computed expression.
  const expr* get_expression() const { return get_operand(); }
  expr* get_expression() { return get_operand(); }

  /// Returns the computed value.
  const value& get_value() const { return val; }

  value val;
};


/// Represents the expression `e1[e2]`. The expression `e1` is an array and
/// `e2` is an integer index into the array. The value of the expression is
/// the value at the given index.
struct index_expr : expr
{
  index_expr(type* t, expr* a, const expr_seq& es) : expr(t), array(a), index(es) { }
  index_expr(type* t, expr* a, expr_seq&& es) : expr(t), array(a), index(std::move(es)) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the expression denoting the array.
  const expr* get_array() const { return array; }
  expr* get_array() { return array; }

  /// Returns the sequence of indices.
  const expr_seq get_indices() const { return index; }
  expr_seq get_indices() { return index; }

  expr* array;
  expr_seq index;
};


/// Represents the projection `e1[e2]` of a tuple element. The expression `e1`
/// is a tuple and `e2` is a constant integral expression denoting the 
/// requested tuple element.
///
/// \todo the abstract syntax for tuples is too close to arrays. The '.'
/// notation would be fine except that we might want to allow an arbitrary
/// expression in that context.
///
/// \todo Project multiple elements?
struct proj_expr : binary_expr
{
  proj_expr(type* t, expr* e1, expr* e2);

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the array.
  const expr* get_tuple() const { return get_lhs(); }
  expr* get_tuple() { return get_lhs(); }

  /// Returns the requested index.
  const expr* get_index_expression() const { return get_rhs(); }
  expr* get_index_expression() { return get_rhs(); }

  int get_index() const;
};


/// Represents the tuple expression `{e1, e2, ..., en}`. This computes a
/// tuple object of type `{t1, t2, ..., tn}` where each `ti` is the object
/// type of the corresponding expression. 
///
/// \todo Tuple values are copy initialized.
struct tuple_expr : nary_expr
{
  using nary_expr::nary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the type of the initializer.
  const tuple_type* get_type() const;
  tuple_type* get_type();

  /// Returns the sequence of elements in the tuple.
  const expr_seq& get_elements() const { return *this; }
  expr_seq& get_elements() { return *this; }
};


/// Represents the materialization of a temporary as the return value of an
/// expression that results in an aggregate. The operand of this expression
/// initializes the object.
///
/// Note that the method of initialization depends on the subexpression. For
/// example, if the subexpression is a function call, then the temporary is
/// initialized by the function's return value. 
struct tmp_expr : nullary_expr
{
  tmp_expr(type* t, decl* d) : nullary_expr(t), tmp(d) { }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the temporary declaration.
  const decl* get_temporary() const { return tmp; }
  decl* get_temporary() { return tmp; }

  decl* tmp;
};


/// Represents a recursive initialization of the elements of an array. The 
/// element initializer is applied to each element. For example:
///
///   var int[3] n; // default initialization
///
/// The array initializer contains a zero initializer, which will be applied
/// to each object.
///
/// The type of the expression is always the type of array being initialized.
struct array_init : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the type of the initializer.
  const array_type* get_type() const;
  array_type* get_type();

  /// Returns the expression used to default initialize each element
  /// of an array.
  const expr* get_element_initializer() const { return get_operand(); }
  expr* get_element_initializer() { return get_operand(); }
};


/// Represents the initialization of each element of an aggregate by a distinct
/// value. For example:
///
///   var int[3] a {0, 1};
///
/// The element initializer contains two copy initializers for the integer
/// values of the array.
///
/// The type of the expression is the type of the aggregate being initialized.
struct elem_init : nary_expr
{
  using nary_expr::nary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }
};


/// Represents the copy initialization of an aggregate. For example:
///
///   var int[3] a2 = a1; // a1 has type int[3]
///
/// The initializer copy initializes each subobject of the aggregate with
/// it's corresponding subobject in the source expression.
///
/// The type of the expression is the type of the array being initialized.
struct copy_init : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the source expression of the initializer.
  const expr* get_value() const { return get_operand(); }
  expr* get_value() { return get_operand(); }
};


/// Represents the initialization of an aggregate as the return value of
/// a function. This is typically used to initialize return values of function
/// calls.
struct ret_init : unary_expr
{
  using unary_expr::unary_expr;

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the function call that initializes the object.
  const expr* get_call() const { return get_operand(); }
  expr* get_call() { return get_operand(); }
};

} // namespace lang_array
} // namespace beaker
