#pragma once

#include <beaker/ast/types.hpp>


namespace beaker {
namespace lang_array {

/// An array of constant bound has the form `t[n1, n2, ..., nk]`. Each `n_i`
/// is a constant integer (expression) denoting the extent of the array at
/// rank $i$.
///
/// Note: an array type where any bounds are unknown is called a buffer.
struct array_type : type
{
  array_type(context& cxt, type* t, const expr_seq& es);
  array_type(context& cxt, type* t, expr_seq&& es);

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the innermost element type of the array.
  const type* get_element_type() const { return elem; }
  type* get_element_type() { return elem; }

  /// Returns the type of element computed by the given index sequence.
  const type* get_subobject_type(context& cxt, const expr_seq&) const;
  type* get_subobject_type(context& cxt, const expr_seq&);

  /// Returns the row type of a multidimensional array. This effectively 
  /// returns the array type stored in rank 0. Note that for simple arrays,
  /// this is the same as the element type.
  const type* get_row_type(context& cxt) const;
  type* get_row_type(context& cxt);

  /// Returns the expression that defines the extent.
  const expr_seq& get_extent_expressions() const { return extents; }
  expr_seq& get_extent_expressions() { return extents; }

  /// Returns the rank of the array.
  int get_rank() const { return extents.size(); }

  /// Returns the extents of the array type.
  std::vector<int> get_extents() const;

  /// Returns the extent of the array at rank k.
  int get_extent(int k) const;

  /// Returns the number of rows in a multidimensional array. This is the
  /// same as the extent in the 0th rank.
  int get_rows() const { return get_extent(0); }

  /// Returns the total number of elements in the array.
  int get_size() const;

  type* elem;
  expr_seq extents;
};


/// An array of whose any bounds are unknown. Buffer types have a similar
/// representation to array types except that bounds can be omitted. For 
/// example:
///
/// - `t[]` is a one-dimension buffer of unknown extent
/// - `t[,]` is a two-dimensional buffer where neither extent is known
/// - `t[n,]` is a two-dimensional buffer whose extent at rank 0 is `n`
///    and whose extent at rank 1 is unknown.
/// - `t[,n]` is a two-dimensional buffer whose extent at rank 0 is unknown
///   and whose extent at rank 2 is `n`.
///
/// Buffers of unknown extent are typically used as pointers to arrays,
/// often in foreign language runtimes (e.g., C arrays). Note that arrays
/// can be implicitly converted to buffers with the same extent. Buffers with 
/// partial extents are typically derived from array slices of non-constant 
/// bound.
///
/// \todo Actually implement this.
///
/// \todo Support slices and slicing.
struct buf_type : type
{
  buf_type(context& cxt, type* t);

  /// Returns the element type of the array.
  const type* get_element_type() const { return elem; }
  type* get_element_type() { return elem; }

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  type* elem;
};


/// A tuple type represents the cross-product of values of different types.
/// Tuple types have the form `{t1, t2, ..., tn}`.
struct tuple_type : type, type_seq
{
  tuple_type(context& cxt);
  tuple_type(context& cxt, const type_seq& ts);
  tuple_type(context& cxt, type_seq&& ts);

  void accept(const_visitor& v) const override { v.visit(this); }
  void accept(visitor& v) override { v.visit(this); }

  /// Returns the sequence of types in the tuple.
  const type_seq& get_element_types() const { return *this; }
  type_seq& get_element_types() { return *this; }

  /// Returns the nth element type.
  const type* get_element_type(int n) const { return (*this)[n]; }
  type* get_element_type(int n) { return (*this)[n]; }
};

} // namespace lang_array


// -------------------------------------------------------------------------- //
// Operations

/// Two array types are equal if they have the same element type and extent.
bool equal_type(const lang_array::array_type* a, const lang_array::array_type* b);

/// Two array pointer types are equal if they have the same element.
bool equal_type(const lang_array::buf_type* a, const lang_array::buf_type* b);

/// Two tuple types are equal if they have the same element types.
bool equal_type(const lang_array::tuple_type* a, const lang_array::tuple_type* b);


/// Add the element type and extent to the hash.
void hash_type(hasher& h, const lang_array::array_type* t);

/// Add the element type and extent to the hash.
void hash_type(hasher& h, const lang_array::buf_type* t);

/// Add the element types to the hash.
void hash_type(hasher& h, const lang_array::tuple_type* t);


} // namespace beaker
