#pragma once

#include <beaker/ast/seq.hpp>
#include <beaker/ast/store.hpp>

#include <stack>


namespace beaker {

struct context;
struct type;
struct expr;
struct decl;
struct stmt;
struct module;


// Statement evaluation returns a 'flow' instruction that instructs the
// caller (of a statement sequence) how execution proceeds.
enum flow
{
  /// Control passes to the next statement.
  flow_next,

  /// Control passes to the caller of a function.
  flow_return,

  /// Control passes to the bottom of a loop.
  flow_break,

  /// Control passes to the top of a loop.
  flow_continue,
};


/// The evaluator maintains information about the state of an evaluation.
///
/// \todo Can this be re-implemented as a postfix evaluator?
struct evaluator
{
  evaluator(context& cxt) : cxt(&cxt) { }

  /// Returns the translation context.
  context& get_context() { return *cxt; }

  // Frames

  frame* make_frame() { return stack.make_frame(); }

  /// Enter a call stack frame.
  void enter_frame() { stack.enter_frame(); }
  void enter_frame(frame* f) { stack.enter_frame(f); }

  /// Leave the current call frame.
  void leave_frame() { stack.leave_frame(); }

  /// Returns the current frame.
  const frame* current_frame() const { return stack.current_frame(); }
  frame* current_frame() { return stack.current_frame(); }

  /// Returns the number of frames on the call stack.
  int stack_depth() const { return stack.stack.size(); }

  /// Start evaluating a function. This makes `d` the current evaluation.
  void start_function(const decl* d) { fns.push_back(const_cast<decl*>(d)); }

  /// Finish evaluating a function.
  void finish_function() { fns.pop_back(); }

  /// Returns the function currently being evaluated.
  const decl* current_function() const { return fns.back(); }

  // Static storage

  /// Allocate static storage for an object.
  object allocate_static(const decl* d);

  /// Retrieve the location of an object with static storage.
  const value& lookup_static(const decl* d) const;
  value& lookup_static(const decl* d);

  // Automatic storage

  /// Allocate automatic storage for an object in the current frame.
  object allocate_automatic(const decl* d);

  /// Retrieve the location of an object in the current frame.
  const value& lookup_automatic(const decl* d) const;
  value& lookup_automatic(const decl* d);

  /// Returns true if `loc` is a valid memory location.
  ///
  /// \todo This is wrong.
  bool is_valid_location(value loc) {
    return true;
    // return stack.is_valid(loc); 
  }

  /// Allocate an object for `d` based on its storage specifier. If `d` has
  /// automatic storage, the object is allocated in the current frame.
  object allocate(const decl* d);

  // Allocate an object for `d` in the frame `f`. If `d` has static storage, 
  /// the object is allocated statically.
  object allocate(frame* f, const decl* d);

  /// Allocate a temporary for an object of type t. Returns a reference to
  /// the temporary object.
  value materialize(const type* t);

  /// Returns a reference to the designated object. This validates that the
  /// storage for the object is valid.
  const value& fetch(const value& ref) const;
  value& fetch(value& ref) const;

  // Members

  /// The translation context.
  context* cxt;

  /// The call stack.
  automatic_store stack;

  /// Evaluation context.
  decl_seq fns;
};


/// An RAII class used to enter a new frame on construction, and leave it
/// on destruction.
struct stack_frame
{
  stack_frame(evaluator& eval) 
    : eval(eval), fn()
  { 
    eval.enter_frame(); 
  }
  
  stack_frame(evaluator& eval, const decl* d, frame* f)
    : eval(eval), fn(d)
  {
    eval.start_function(d);
    eval.enter_frame(f); 
  }
  
  ~stack_frame() 
  { 
    eval.leave_frame(); 
    if (fn)
      eval.finish_function();
  }

  evaluator& eval;
  const decl* fn;
};


/// Evaluate the expression `e`, returning its value.
value evaluate(evaluator& eval, const expr* e);

/// Evaluate the declaration `d`. This will allocate storage for a declaration,
/// if needed, and initialize the resulting object.
void evaluate(evaluator& eval, const decl* d);

/// Evaluate the statement `s`. This returns a flow control indicator which
/// is used to determine the next step in the execution of a sequence of 
/// statements.
flow evaluate(evaluator& eval, const stmt* s);

/// Initialize the object `obj` with the initializer `e`. 
void initialize(evaluator& eval, object& obj, const expr* e);

// -------------------------------------------------------------------------- //
// Dispatch

value evaluate_expr(evaluator& eval, const expr* e);
void evaluate_decl(evaluator& eval, const decl* d);
flow evaluate_stmt(evaluator& eval, const stmt* s);
void evaluate_init(evaluator& eval, object& obj, const expr* e);


// -------------------------------------------------------------------------- //
// Common evaluation functions

/// Evaluate the expression `e` and return a value.
value evaluate(context& cxt, const expr* e);

/// Evaluate the expression `e` as a signed integer value.
std::intmax_t evaluate_as_signed(context& cxt, const expr* e);

/// Evaluate the expression `e` as an unsigned integer value.
std::uintmax_t evaluate_as_unsigned(context& cxt, const expr* e);

/// Try evaluating `e` to produce a constant expression. Returns nullptr if
/// the expression does not result in a constant expression.
///
/// \todo Collect diagnostics resulting from evaluation errors.
expr* evaluate_constant(context& cxt, expr* e);

} // namespace beaker

