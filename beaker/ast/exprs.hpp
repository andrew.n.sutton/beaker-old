#pragma once

#include <beaker/ast/seq.hpp>
#include <beaker/ast/value.hpp>


namespace beaker {

struct type;
struct decl;
struct value_decl;

// Forward declarations
struct expr;
#define pp_expr(E, X, NS) \
  namespace NS { struct E ## X; }
#include "exprs.def"
#undef pp_expr


/// The base class of all expressions in the language. Every expression has
/// a type.
struct expr
{
  struct const_visitor;
  struct visitor;

  expr(type* t) : ty(t) { }
  
  virtual ~expr() = default;

  /// Accept a visitor for this object.
  virtual void accept(const_visitor&) const = 0;
  virtual void accept(visitor&) = 0;

  /// Returns the expression's type.
  const type* get_type() const { return ty; }
  type* get_type() { return ty; }

  /// Returns true if this expression has type t.
  bool has_type(const type* t) const;
  
  /// Returns true if this expression has that of e.
  bool has_type_of(const expr* e) const { return has_type(e->get_type()); }

  /// Pretty print this expression.
  void print() const;

  /// Pretty print this expression, including internal expressions.
  void print_verbose() const;

  type* ty;
};


/// The const expression visitor.
struct expr::const_visitor
{
#define pp_expr(E, X, NS) \
  virtual void visit(const NS::E ## X*) { }
#include "exprs.def"
#undef pp_expr
};

/// The non-const expression visitor.
struct expr::visitor
{
#define pp_expr(E, X, NS) \
  virtual void visit(NS::E ## X*) { }
#include "exprs.def"
#undef pp_expr
};


/// An expression with no operands.
struct nullary_expr : expr
{
  nullary_expr(type* t) : expr(t) { }
};


/// An expression with one operand.
struct unary_expr : expr
{
  unary_expr(type* t, expr* e1) : expr(t), sub{e1} { }
  ~unary_expr() { delete sub[0]; }

  /// Returns the sole operand of the expression.
  const expr* get_first() const { return sub[0]; }
  expr* get_first() { return sub[0]; }

  /// Returns the sole operand. Equivalent to get_first().
  const expr* get_operand() const { return get_first(); }
  expr* get_operand() { return get_first(); }

  expr* sub[1];
};


/// An expression with two operands.
struct binary_expr : expr
{
  binary_expr(type* t, expr* e1, expr* e2) : expr(t), sub{e1, e2} { }
  ~binary_expr() { for (expr* e: sub) delete e; }

  /// Returns the first operand.
  const expr* get_first() const { return sub[0]; }
  expr* get_first() { return sub[0]; }

  /// Returns the second operand.
  const expr* get_second() const { return sub[1]; }
  expr* get_second() { return sub[1]; }

  /// Returns the left-had operand. Equivalent to get_first().
  const expr* get_lhs() const { return get_first(); }
  expr* get_lhs() { return get_first(); }

  /// Returns the right-hand operand. Equivalent to get_second().
  const expr* get_rhs() const { return get_second(); }
  expr* get_rhs() { return get_second(); }

  expr* sub[2];
};


/// An expression with three operands.
struct ternary_expr : expr
{
  ternary_expr(type* t, expr* e1, expr* e2, expr* e3) : expr(t), sub{e1, e2, e3} { }
  ~ternary_expr() { for (expr* e: sub) delete e; }

  /// Returns the first operand.
  const expr* get_first() const { return sub[0]; }
  expr* get_first() { return sub[0]; }

  /// Returns the second operand.
  const expr* get_second() const { return sub[1]; }
  expr* get_second() { return sub[1]; }

  /// Returns the third operand.
  const expr* get_third() const { return sub[2]; }
  expr* get_third() { return sub[2]; }

  expr* sub[3];
};


/// An expression that with a variable number of operands.
struct nary_expr : expr, expr_seq
{
  nary_expr(type* t, const expr_seq& es) : expr(t), expr_seq(es) { }
  nary_expr(type* t, expr_seq&& es) : expr(t), expr_seq(std::move(es)) { }
  ~nary_expr() { for (expr* e : *this) delete e; }

  const expr_seq& get_operands() const { return *this; }
  expr_seq& get_operands() { return *this; }
};


// -------------------------------------------------------------------------- //
// Operations

/// Returns true if `e` has object type.
bool is_object(const expr* e);

/// Returns true if `e` has reference type.
bool is_reference(const expr* e);

/// Returns true if `e` is an initializer.
bool is_initializer(const expr* e);

/// Returns true if `e` represents a form of empty initialization. The value
/// of an empty initializer is implicit in the expression.
bool is_empty_initializer(const expr* e);

/// Returns true if `e` represents a form of copy initialization.
bool is_copy_initializer(const expr* e);

/// Returns true if `e` represents a is a aggregate initialization.
bool is_brace_initializer(const expr* e);

/// Returns true if `e` is a constant expression.
bool is_constant(const expr* e);


/// Returns the value of a constant expression `e`.
value get_constant_value(const expr* e);

// -------------------------------------------------------------------------- //
// Visitors

template<typename F, typename T>
struct expr_visitor : expr::visitor
{
  expr_visitor(F f) : fn(f) { }

#define pp_expr(E, X, NS) \
  virtual void visit(NS::E ## X* e) { ret = fn(e); }
#include "exprs.def"
#undef pp_expr

  /// Apply the visitor to e and return the accumulated value.
  T apply(expr* e)
  {
    e->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct expr_visitor<F, void> : expr::visitor
{
  expr_visitor(F f) : fn(f) { }

#define pp_expr(E, X, NS) \
  virtual void visit(NS::E ## X* e) { fn(e); }
#include "exprs.def"
#undef pp_expr

  /// Apply the visitor to e.
  void apply(expr* e)
  {
    e->accept(*this);
  }

  F fn;
};


/// A non-modifying visitor.
template<typename F, typename T>
struct const_expr_visitor : expr::const_visitor
{
  const_expr_visitor(F f) : fn(f) { }

#define pp_expr(E, X, NS) \
  virtual void visit(const NS::E ## X* e) { ret = fn(e); }
#include "exprs.def"
#undef pp_expr

  /// Apply the visitor to `e` and return the accumulated value.
  T apply(const expr* e)
  {
    e->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct const_expr_visitor<F, void> : expr::const_visitor
{
  const_expr_visitor(F f) : fn(f) { }

#define pp_expr(E, X, NS) \
  virtual void visit(const NS::E ## X* e) { fn(e); }
#include "exprs.def"
#undef pp_expr

  /// Apply the visitor to e.
  void apply(const expr* e)
  {
    e->accept(*this);
  }

  F fn;
};


/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(expr* e, F fn)
{
  using T = typename std::result_of_t<F(expr*)>;
  expr_visitor<F, T> vis(fn);
  return vis.apply(e);
}

/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(const expr* e, F fn)
{
  using T = typename std::result_of_t<F(const expr*)>;
  const_expr_visitor<F, T> vis(fn);
  return vis.apply(e);
}


} // namespace beaker
