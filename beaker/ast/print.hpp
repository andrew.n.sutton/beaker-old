#pragma once

#include <beaker/common/symbols.hpp>
#include <beaker/ast/seq.hpp>

#include <cstdint>
#include <iosfwd>


namespace beaker {

struct type;
struct expr;
struct decl;
struct stmt;
struct module;

/// The pretty printer object provides context for printing terms of the
/// languages. This wraps an output stream and helps manages indentation and
/// other printing options.
struct pretty_printer
{
  /// Prints all expressions in AST, including the internal nodes. This
  /// will generate non-compilable output.
  static constexpr unsigned verbose = 1;

  explicit pretty_printer(unsigned flags = 0);
  pretty_printer(std::ostream&, unsigned flags = 0);

  void print(char);
  void print(const char*);
  void print(const std::string&);
  void print(std::intmax_t);
  void print(std::uintmax_t);

  /// Print a space character.
  void space();

  /// Prints a newline character and indents to the current nesting.
  void newline();

  // FIXME: Remove these.
  void print_space() { space(); }
  void print_newline() { newline(); }

  void indent() { ++nesting; }
  void undent() { --nesting; }

  bool verbose_output() const { return flags & verbose; }

  std::ostream& os;
  int nesting;
  unsigned flags;
};

inline void print(pretty_printer& pp, char c) { pp.print(c); }
inline void print(pretty_printer& pp, const char* s) { pp.print(s); }
inline void print(pretty_printer& pp, const std::string& s) { pp.print(s); }
inline void print(pretty_printer& pp, std::intmax_t n) { pp.print(n); }
inline void print(pretty_printer& pp, std::uintmax_t n) { pp.print(n); }
inline void print(pretty_printer& pp, int n) { pp.print((std::intmax_t)n); }

void print(pretty_printer&, const symbol*);
void print(pretty_printer&, const type*);
void print(pretty_printer&, const expr*);
void print(pretty_printer&, const decl*);
void print(pretty_printer&, const stmt*);

void print_type(pretty_printer&, const type*);
void print_expr(pretty_printer&, const expr*);
void print_decl(pretty_printer&, const decl*);
void print_decl(pretty_printer&, const module*);
void print_stmt(pretty_printer&, const stmt*);


// -------------------------------------------------------------------------- //
// Helper functions

struct unary_expr;
struct binary_expr;

/// Prints a binary operator offset by spaces.
///
/// \todo Make the spaces a configuration option for the printer.
inline void 
print_binary_op(pretty_printer& pp, char op)
{
  pp.print_space();
  pp.print(op);
  pp.print_space();
}

/// Prints a binary operator offset by spaces.
///
/// \todo Make the spaces a configuration option for the printer.
inline void 
print_binary_op(pretty_printer& pp, const char* op)
{
  pp.print_space();
  pp.print(op);
  pp.print_space();
}

void print_enclosed_type(pretty_printer&, const type*, char, char);

void print_enclosed_expr(pretty_printer&, char, char);
void print_enclosed_expr(pretty_printer&, const expr* e, char, char);
void print_enclosed_expr(pretty_printer&, const expr* e1, const expr* e2, char, char);

void print_builtin_call_expr(pretty_printer&, const char*);
void print_builtin_call_expr(pretty_printer&, const char*, const expr*);
void print_builtin_call_expr(pretty_printer&, const char*, const expr*, const expr*);

void print_grouped_expr(pretty_printer&, const expr*);
void print_prefix_expr(pretty_printer&, const unary_expr*, const char*);
void print_suffix_expr(pretty_printer&, const unary_expr*, const char*);
void print_infix_expr(pretty_printer&, const binary_expr*, const char*);

/// Print a comma-separated list of elements.
template<typename T>
void 
print_comma_separated(pretty_printer& pp, const seq<T>& s)
{
  for (auto iter = s.begin(); iter != s.end(); ++iter) {
    print(pp, *iter);
    if (std::next(iter) != s.end()) {
      pp.print(',');
      pp.print_space();
    }
  }
}


// -------------------------------------------------------------------------- //
// IOstream integration

template<typename T>
struct pretty_term
{
  const T* term;
  unsigned flags;
};

template<typename T>
struct quote_term
{
  const T* term;
  unsigned flags;
};


template<typename T>
inline auto 
pretty(const T* t, unsigned flags = 0) 
{ 
  return pretty_term<T>{t, flags}; 
}

template<typename T>
inline auto 
verbose(const T* t, unsigned flags = pretty_printer::verbose) 
{ 
  return pretty_term<T>{t, flags}; 
}

template<typename T>
inline auto 
quote(const T* t, unsigned flags = 0) 
{ 
  return quote_term<T>{t, flags}; 
}

template<typename C, typename T, typename U>
std::basic_ostream<C, T>&
operator<<(std::basic_ostream<C, T>& os, pretty_term<U> t)
{
  pretty_printer pp(os, t.flags);
  if (!t.term)
    print(pp, "<null>");
  else
    print(pp, t.term);
  return os;
}

/// Pretty print the given term in quotes.
template<typename C, typename T, typename U>
std::basic_ostream<C, T>&
operator<<(std::basic_ostream<C, T>& os, quote_term<U> t)
{
  return os << '\'' << pretty(t.term, t.flags) << '\'';
}


template<typename T>
struct typed_term
{
  const T* term;
  unsigned flags;
};

template<typename T>
inline auto
typed(const T* tern, unsigned flags = pretty_printer::verbose)
{
  return typed_term<T>{tern, flags};
}

/// Pretty print a type binding.
template<typename C, typename T, typename U>
std::basic_ostream<C, T>&
operator<<(std::basic_ostream<C, T>& os, typed_term<U> t)
{
  return os << pretty(t.term, t.flags) << " : " 
            << pretty(t.term->get_type(), t.flags);
}


} // namespace beaker

