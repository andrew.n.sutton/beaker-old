#pragma once

#include <beaker/common/symbols.hpp>
#include <beaker/ast/types.hpp>
#include <beaker/ast/store.hpp>

#include <memory>


namespace beaker {

struct module;


/// The (AST) context is a repository of facilities used to support the
/// construction of a module from source code. In particular, the context
/// provides a single point of ownership for the module being constructed
/// and types.
struct context
{
  struct canonicals;

  context(symbol_table&);
  ~context();

  /// Returns the symbol table.
  const symbol_table& get_symbol_table() const { return *syms; }
  symbol_table& get_symbol_table() { return *syms; }

  /// Type layouts

  /// Returns the default layout for byte-sized types.  
  type::layout get_byte_layout() const;

  /// Returns the default layout for system integer types.
  type::layout get_int_layout() const;

  /// Returns the default layout for system pointer types.
  type::layout get_ptr_layout() const;

  /// Returns a layout for an object with `s` bits of size and alignment `a`.
  type::layout get_layout(int s, int a);

  /// Returns the translation unit.
  const decl* get_tu() const { return tu; }
  decl* get_tu() { return tu; }

  /// Returns the type `void`.
  type* get_void_type() const;

  /// Returns the type `bool`.
  type* get_bool_type() const;

  /// Returns the type `nat`.
  type* get_nat_type() const;

  /// Returns the type `int`.
  type* get_int_type() const;

  /// Returns a canonical reference type `t&`.
  type* get_reference_type(type*);

  /// Returns a canonical function type `(t1, t2, ..., tn)->t`.
  type* get_function_type(type_seq& ts, type* t);
  type* get_function_type(type_seq&& ts, type* t);

  /// Returns a canonical function type given list of parameters and return
  /// value.
  type* get_function_type(decl_seq& ps, decl* r);

  /// Returns the buffer type `t[]`.
  type* get_buffer_type(type* t);

  /// Returns the array type `t[n]`.
  type* get_array_type(type* t, expr* n);

  /// Returns the array type `t[n1, n2, ..., nk]`.
  type* get_array_type(type* t, expr_seq&& ns);

  /// Returns the tuple type `{}`.
  type* get_tuple_type();

  /// Returns the tuple type `{t1, t2, ..., tn}`.
  type* get_tuple_type(type_seq&& ts);

  // Static data

  /// Allocate static storage for a (future) object.
  object allocate_static(const decl* d) { return statics.allocate(*this, d); }

  /// Returns the location of a declaration with static storage.
  const value& lookup_static(const decl* d) const { return statics.lookup(d); }
  value& lookup_static(const decl* d) { return statics.lookup(d); }

private:
  /// Stores all unique symbols (names) in a program.
  symbol_table* syms;

  /// Stores canonical objects.
  canonicals* canon;

  /// The translation unit.
  decl* tu;

  /// Maintains the set of initialized static declaration.
  static_store statics;
};

} // namespace beaker
