#pragma once

#include <beaker/common/symbols.hpp>
#include <beaker/ast/seq.hpp>


namespace beaker {

// Forward declarations
struct type;
struct expr;
struct stmt;
struct decl;
#define pp_decl(D, X, NS) \
  namespace NS { struct D ## X; }
#include "decls.def"
#undef pp_decl


/// The type of a declaration's unique identifier.
using uid = int;

/// A declaration's context is the declaration in which the declaration
/// appears. The semantic and lexical contexts of the root declaration are 
/// both null.
struct dc
{
  dc() : sema(), lex() { }
  dc(decl* d) : sema(d), lex(d) { }
  dc(decl* s, decl* l) : sema(s), lex(l) { }

  /// Returns the semantic declaration context, or nullptr if this is the 
  /// root declaration context.
  const decl* get_context() const { return sema; }
  decl* get_context() { return sema; }

  /// Returns the semantic context of the declaration.
  const decl* get_semantic_context() const { return sema; }
  decl* get_semantic_context() { return sema; }

  /// Returns the lexical context of the declaration.
  const decl* get_lexical_context() const { return lex; }
  decl* get_lexical_context() { return lex; }

  decl* sema;
  decl* lex;
};


/// The base class of all declarations in the language. All declarations have
/// a name and type.
struct decl
{
  struct const_visitor;
  struct visitor;

  decl(uid id, dc cxt) : id(id), cxt(cxt) { }
  virtual ~decl() = default;

  virtual void accept(const_visitor& v) const = 0;
  virtual void accept(visitor& v) = 0;

  /// Returns the declaration's unique id.
  uid get_id() const { return id; }

  /// Returns the declaration's semantic context.
  const decl* get_context() const { return cxt.get_context(); }
  decl* get_context() { return cxt.get_context(); }

  /// Returns true if this `d` is declared within the declarative region 
  /// associated with this context. Note that a declaration does not enclose
  /// itself.
  bool encloses(const decl* d) const;

  /// Returns declared name nullptr if this is not a named declaration.
  virtual symbol* get_name() const { return nullptr; }

  /// Returns declared type or nullptr if this is not typed declaration.
  virtual const type* get_type() const { return nullptr; }
  virtual type* get_type() { return nullptr; }

  /// Pretty print the declaration (and its definition).
  void print() const;

  /// Verbose print the declaration (and its definition).
  void print_verbose() const;

  uid id;
  dc cxt;
};

/// The constant declaration visitor.
struct decl::const_visitor
{
#define pp_decl(D, X, NS) \
  virtual void visit(const NS::D ## X*) { }
#include "decls.def"
#undef pp_decl
};

/// The non-constant declaration visitor.
struct decl::visitor
{
#define pp_decl(D, X, NS) \
  virtual void visit(NS::D ## X*) { }
#include "decls.def"
#undef pp_decl
};


/// Represents the named declaration.
struct named_decl : decl
{
  named_decl(uid id, dc cxt, symbol* n) : decl(id, cxt), name(n) { }

  /// Returns the name of the declaration.
  symbol* get_name() const override { return name; }

  symbol* name;
};


/// Represents a declaration with both a name and a type.
struct typed_decl : named_decl
{
  typed_decl(uid id, dc cxt, symbol* n, type* t) 
    : named_decl(id, cxt, n), ty(t) { }

  /// Returns the declaration's type.
  const type* get_type() const override { return ty; }
  type* get_type() override { return ty; }

  /// Returns true if this declaration has type `t`.
  bool has_type(const type* t) const;

  /// Returns true if this declaration has the type of `e`.
  bool has_type_of(const expr* e) const;

  type* ty;
};


/// Represents the declaration of a (possibly stored) value. Value declarations
/// are typed declarations with an initializer expression. Examples include
/// variables and constants.
struct value_decl : typed_decl
{
  value_decl(uid id, dc cxt, symbol* n, type* t)
    : typed_decl(id, cxt, n, t), init() { }
  value_decl(uid id, dc cxt, symbol* n, type* t, expr* e) 
    : typed_decl(id, cxt, n, t), init(e) { }

  /// Returns the initializer of the value.
  const expr* get_initializer() const { return init; }
  expr* get_initializer() { return init; }

  expr* init;
};


/// An additional base class for all parameters. This provides positional
/// indexes for parameters of functions and templates. 
struct parm
{
  /// Returns the nesting depth of the parameter. This is only meaningful
  /// for template parameters since templates can be nested.
  int get_depth() const { return depth; }
  
  /// Returns the parameter index (starting at 0). 
  int get_index() const { return index; }

  int depth : 10;
  int index : 22;
};


/// Represents the mapping of input values to output values. Mapping 
/// declarations have an associated statement that defines the mapping.
/// Examples are functions and axioms.
struct mapping_decl : typed_decl
{
  mapping_decl(uid id, dc cxt, symbol* n, type* t, const decl_seq& ps, decl* r);
  mapping_decl(uid id, dc cxt, symbol* n, type* t, decl_seq&& ps, decl* r);
  mapping_decl(uid id, dc cxt, symbol* n, type* t, const decl_seq& ps, decl* r, stmt* s);
  mapping_decl(uid id, dc cxt, symbol* n, type* t, decl_seq&& ps, decl* r, stmt* s);

  /// Returns the parameters of the mapping.
  const decl_seq& get_parameters() const { return parms; }
  decl_seq& get_parameters() { return parms; }

  /// Returns the declared return of the mapping.
  const value_decl* get_return() const { return static_cast<value_decl*>(ret); }
  value_decl* get_return() { return static_cast<value_decl*>(ret); }

  /// Returns the body of the mapping.
  const stmt* get_body() const { return body; }
  stmt* get_body() { return body; }

  decl_seq parms;
  decl* ret;
  stmt* body;
};


// -------------------------------------------------------------------------- //
// Operations

/// Returns true if `d` is a constant. A constant is any declaration whose
/// name binds directly to a value. Constants are immutable.
///
/// \todo Distinguish between constants initialized at runtime and compile
/// time. For example, function parameters are initialized at runtime. A
/// constant whose initializer is a constant expression can be used e.g., as
/// an array bound or template argument.
bool is_constant(const decl* d);

/// Returns true if `d` is a variable. A variable is any declaration whose
/// name binds to a memory location. Variables are mutable.
bool is_variable(const decl* d);

/// Returns true if `d` is a parameter of a function or template.
bool is_parameter(const decl* d);

/// Returns true if `d` is a function declaration.
bool is_function(const decl* d);

/// Returns true if `d` has static storage.
bool has_static_storage(const decl* d);

/// Returns true if `d` has automatic storage.
bool has_automatic_storage(const decl* d);

/// Returns true if `d` has external name linkage.
bool has_external_linkage(const decl* d);

/// Returns true if `d` has no name linkage. Local variables and parameters 
/// have no name linkage.
bool has_no_linkage(const decl* d);

// -------------------------------------------------------------------------- //
// Visitors

template<typename F, typename T>
struct decl_visitor : decl::visitor
{
  decl_visitor(F f) : fn(f) { }

#define pp_decl(D, X, NS) \
  virtual void visit(NS::D ## X* d) { ret = fn(d); }
#include "decls.def"
#undef pp_decl

  /// Apply the visitor to d and return the accumulated value.
  T apply(decl* d)
  {
    d->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct decl_visitor<F, void> : decl::visitor
{
  decl_visitor(F f) : fn(f) { }

#define pp_decl(D, X, NS) \
  virtual void visit(NS::D ## X* d) { fn(d); }
#include "decls.def"
#undef pp_decl

  /// Apply the visitor to d.
  void apply(decl* d)
  {
    d->accept(*this);
  }

  F fn;
};


/// A non-modifying visitor.
template<typename F, typename T>
struct const_decl_visitor : decl::const_visitor
{
  const_decl_visitor(F f) : fn(f) { }

#define pp_decl(D, X, NS) \
  virtual void visit(const NS::D ## X* d) { ret = fn(d); }
#include "decls.def"
#undef pp_decl

  /// Apply the visitor to d and return the accumulated value.
  T apply(const decl* d)
  {
    d->accept(*this);
    return ret;
  }

  F fn;
  T ret;
};

/// Specialization for void dispatch functions.
template<typename F>
struct const_decl_visitor<F, void> : decl::const_visitor
{
  const_decl_visitor(F f) : fn(f) { }

#define pp_decl(D, X, NS) \
  virtual void visit(const NS::D ## X* d) { fn(d); }
#include "decls.def"
#undef pp_decl

  /// Apply the visitor to d.
  void apply(const decl* d)
  {
    d->accept(*this);
  }

  F fn;
};


/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(decl* d, F fn)
{
  using T = typename std::result_of_t<F(decl*)>;
  decl_visitor<F, T> vis(fn);
  return vis.apply(d);
}

/// Apply fn to the the given type.
template<typename F>
decltype(auto)
apply(const decl* d, F fn)
{
  using T = typename std::result_of_t<F(const decl*)>;
  const_decl_visitor<F, T> vis(fn);
  return vis.apply(d);
}

} // namespace beaker
