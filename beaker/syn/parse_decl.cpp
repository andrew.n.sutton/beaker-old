#include "parser.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

/// translation -> toplevel-declaration-seq
decl*
parser::translation()
{
  decl* mod = sema.on_start_translation();
  
  // Module scope starts at the beginning of translation and continues until
  // the end of translation.
  declarative_region reg(sema, module_scope, mod);

  // Note that declarations "self add" to their enclosing declaration.
  toplevel_declaration_seq();

  return sema.on_finish_translation(mod);
}

/// toplevel-declaration-seq -> toplevel-declaration-seq toplevel-declaration
///                           | toplevel-declaration
decl_seq
parser::toplevel_declaration_seq()
{
  decl_seq ds;
  while (!eof()) {
    decl* d = toplevel_declaration();
    ds.push_back(d);
  }
  return ds;  
}

/// toplevel-declaration -> module-declaration
///                       | import-declaration
///                       | function-declaration
///                       | constant-declaration
decl*
parser::toplevel_declaration()
{
  switch (lookahead()) {
    case def_kw:
      return function_declaration();
    case const_kw:
      return constant_declaration();
    default:
      break;
  }
  throw std::runtime_error("expected toplevel-declaration");
}

/// block-declaration -> variable-declaration
decl* 
parser::block_declaration()
{
  switch (lookahead()) {
    case const_kw:
      return constant_declaration();
    case var_kw:
      return variable_declaration();
    default:
      break;
  }
  throw std::runtime_error("expected block-declaration");
}

/// function-declaration -> 'def' function-prototype ';'
///                       | 'def' function-prototype function-definition
///
/// function-prototype -> identifier '(' function-parameter-list? ')' '->' type-specifier
///
/// function-definition -> block-statement
decl*
parser::function_declaration()
{
  require(def_kw);
  symbol* n = identifier();
  
  // Function parameter scope extends from form the left parenthesis preceding
  // the parameter list to the end of the function declaration or definition.
  declarative_region parm_reg(sema, function_parameter_scope);
  decl_seq ps;
  match(lparen_tok);
  if (lookahead() != rparen_tok)
    ps = function_parameter_list();
  match(rparen_tok);
  
  match(arrow_tok);
  type* t = type_specifier();

  // The point of declaration for a function is after the parameters and
  // return type specifier.
  decl* fn = sema.on_function_declaration(n, std::move(ps), t);
  
  if (lookahead() == semicolon_tok)
    return sema.on_function_definition(fn);
  
  if (lookahead() == lbrace_tok) {
    // Function scope extends from the start of the definition (left brace)
    // to the end of the function definition.
    declarative_region fn_reg(sema, function_scope, fn);
    stmt* s = block_statement();
    return sema.on_function_definition(fn, s);
  }
  else {
    throw std::runtime_error("expected function-definition");
  }
}

/// function-parameter-list -> function-parameter-list ',' function-parameter
///                          | function-parameter
decl_seq
parser::function_parameter_list()
{
  decl_seq ps;
  while (true) {
    decl* p = function_parameter();
    ps.push_back(p);
    if (match_if(comma_tok))
      continue;
    if (lookahead() == rparen_tok)
      break;
    else
      throw std::runtime_error("expected function-parameter");
  }
  return ps;
}

/// function-parameter -> [parameter-specifier] type-specifier [identifier]
///
/// parameter-specifier -> 'in' | 'out' | 'var'
///
/// \todo Implement default arguments.
decl*
parser::function_parameter()
{
  token* s = match_any(in_kw, out_kw, var_kw);
  type* t = type_specifier();
  if (lookahead() == id_tok) {
    symbol* n = identifier();
    return sema.on_function_parameter(s, t, n);
  }
  else {
    return sema.on_function_parameter(s, t);
  }
}

/// constant-declaration -> 'const' type-specifier identifier initializer-clause ';'
///
/// initializer-clause -> value-initializer
///                     | brace-initialier
///
/// value-initializer -> '=' conditional-expression
///
/// brace-initializer -> '{' function-argument-list '}'
decl*
parser::constant_declaration()
{
  require(const_kw);
  type* t = type_specifier();
  symbol* n = identifier();

  // The point of declaration for a constant is after its name.
  //
  // FIXME: Do not allow this name to be used within its initializer.
  decl* var = sema.on_constant_declaration(t, n);

  if (match(eq_tok)) {
    expr* e = conditional_expression();
    var = sema.on_constant_initialization(var, e);
  }
  else if (match_if(lbrace_tok)) {
    expr_seq args = function_argument_list();
    var = sema.on_constant_initialization(var, std::move(args));
    match(rbrace_tok);
  }
  match(semicolon_tok);
  return var;
}

/// variable-declaration -> 'var' type-specifier identifier [initializer-clause] ';'
decl*
parser::variable_declaration()
{
  require(var_kw);
  type* t = type_specifier();
  symbol* n = identifier();

  // The point of declaration for a variable is after its name.
  //
  // FIXME: Do not allow this name to be used within its initializer.
  decl* var = sema.on_variable_declaration(t, n);

  if (lookahead() == semicolon_tok) {
    var = sema.on_variable_initialization(var);
  }
  else if (match_if(eq_tok)) {
    expr* e = conditional_expression();
    var = sema.on_variable_initialization(var, e);
  }
  else if (match_if(lbrace_tok)) {
    expr_seq args = function_argument_list();
    var = sema.on_variable_initialization(var, std::move(args));
    match(rbrace_tok);
  }
  match(semicolon_tok);
  return var;
}

} // namespace beaker
