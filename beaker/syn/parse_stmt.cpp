#include "parser.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

/// statement-seq -> statement-seq statement | statement
stmt_seq
parser::statement_seq()
{
  stmt_seq ss;
  while (lookahead() != rbrace_tok) {
    stmt* s = statement();
    ss.push_back(s);
  }
  return ss;
}

/// statement -> block-statement
///            | return-statement
///            | expression-statement
///            | declaration-statement
stmt*
parser::statement()
{
  switch (lookahead()) {
    case lbrace_tok:
      return block_statement();
    case if_kw:
      return if_statement();
    case while_kw:
      return while_statement();
    case break_kw:
      return break_statement();
    case continue_kw:
      return continue_statement();
    case return_kw:
      return return_statement();

    // FIXME: Eventually remove these.
    case assert_kw:
      return assert_statement();
    case print_kw:
      return print_statement();
    case scan_kw:
      return scan_statement();
    case skip_kw:
      return skip_statement();
    case trap_kw:
      return trap_statement();

    case var_kw:
    case const_kw:
      // Declaration introducers.
      return declaration_statement();
    
    default:
      return expression_statement();
  }
}

/// block-statement -> '{' statement-seq '}'
stmt* 
parser::block_statement()
{
  declarative_region reg(sema, block_scope);
  require(lbrace_tok);
  stmt_seq ss;
  if (lookahead() != rbrace_tok)
    ss = statement_seq();
  match(rbrace_tok);
  return sema.on_block_statement(std::move(ss));
}

/// if-statement -> 'if' '(' expression ')' statement 'else' statement
///
/// \todo Does it make sense to allow assignment inside an expression?
stmt*
parser::if_statement()
{
  require(if_kw);
  match(lparen_tok);
  expr* e = expression();
  match(rparen_tok);
  stmt* s1 = statement();
  match(else_kw);
  stmt* s2 = statement();
  return sema.on_if_statement(e, s1, s2);
}

/// while-statement -> 'while' '(' expression ')' statement
stmt*
parser::while_statement()
{
  require(while_kw);
  match(lparen_tok);
  expr* e = expression();
  match(rparen_tok);
  stmt* l = sema.on_while_initiation(e);
  stmt* s = statement();
  return sema.on_while_completion(l, s);
}

/// break-statement -> 'break' ';'
stmt*
parser::break_statement()
{
  require(break_kw);
  match(semicolon_tok);
  return sema.on_break_statement();
}

/// continue-statement -> 'continue' ';'
stmt*
parser::continue_statement()
{
  require(continue_kw);
  match(semicolon_tok);
  return sema.on_break_statement();
}

/// return-statement -> 'return' initializer? ';'
///
/// \todo Implement brace and paren initialization of the return value?
stmt*
parser::return_statement()
{
  require(return_kw); {
  if (match_if(semicolon_tok))
    return sema.on_return_statement();
  }
  expr* e = expression();
  match(semicolon_tok);
  return sema.on_return_statement(e);
}

/// assert-statement -> 'assert' conditional-expression ';'
stmt*
parser::assert_statement()
{
  require(assert_kw);
  expr* e = conditional_expression();
  match(semicolon_tok);
  return sema.on_assert_statement(e);
}

/// print-statement -> 'print' conditional-expression ';'
stmt*
parser::print_statement()
{
  require(print_kw);
  expr* e = conditional_expression();
  match(semicolon_tok);
  return sema.on_print_statement(e);
}

/// scan-statement -> 'scan' conditional-expression ';'
stmt*
parser::scan_statement()
{
  require(scan_kw);
  expr* e = conditional_expression();
  match(semicolon_tok);
  return sema.on_scan_statement(e);
}

/// skip-statement -> 'skip' ';'
stmt*
parser::skip_statement()
{
  require(skip_kw);
  match(semicolon_tok);
  return sema.on_skip_statement();
}

/// trap-statement -> 'trap' ';'
stmt*
parser::trap_statement()
{
  require(trap_kw);
  match(semicolon_tok);
  return sema.on_trap_statement();
}

/// declaration-statement -> block-declaration
stmt*
parser::declaration_statement()
{
  decl* d = block_declaration();
  return sema.on_declaration_statement(d);
}

/// expression-statement -> expression ';'
stmt*
parser::expression_statement()
{
  expr* e = expression();
  match(semicolon_tok);
  return sema.on_expression_statement(e);
}

} // namespace beaker
