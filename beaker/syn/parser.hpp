#pragma once

#include <beaker/lex/lexer.hpp>
#include <beaker/sema/translator.hpp>
#include <beaker/ast/seq.hpp>

#include <deque>
#include <vector>


namespace beaker {

struct stmt;
struct decl;
struct type;
struct expr;


/// The parser is responsible for transforming a sequence of tokens into
/// an abstract syntax tree. The grammar for the language is defined by the
/// various parsing functions.
struct parser
{
  parser(const keyword_table&, symbol_table&, context&, const std::string&);

  /// translation -> toplevel-declaration-seq
  decl* translation();

  // Declarations
  
  /// toplevel-declaration-seq -> toplevel-declaration-seq toplevel-declaration
  ///                           | toplevel-declaration
  decl_seq toplevel_declaration_seq();
  
  /// toplevel-declaration -> module-declaration
  ///                       | import-declaration
  ///                       | function-declaration
  ///                       | constant-declaration
  decl* toplevel_declaration();

  /// block-declaration -> constant-declaration
  ///                    | variable-declaration
  decl* block_declaration();

  /// function-declaration -> 'def' function-prototype ';'
  ///                       | 'def' function-prototype function-definition
  ///
  /// function-prototype -> identifier '(' [function-parameter-list] ')' '->' type-specifier
  ///
  /// function-definition -> block-statement
  decl* function_declaration();

  /// function-parameter-list -> function-parameter-list ',' function-parameter
  ///                          | function-parameter
  decl_seq function_parameter_list();

  /// function-parameter -> ['var'] type-specifier [identifier]
  ///
  /// \todo Support a set of parameter specifiers (var, in, out, etc).
  decl* function_parameter();

  /// constant-declaration -> 'const' type-specifier identifier initializer-clause ';'
  ///
  /// initializer-clause -> value-initializer
  ///                     | brace-initializer
  ///
  /// value-initializer -> '=' conditional-expression
  ///
  /// brace-initializer -> '{' function-argument-list '}'
  decl* constant_declaration();

  /// variable-declaration -> 'var' type-specifier identifier [initializer-clause] ';'
  decl* variable_declaration();


  // Types

  /// type-specifier -> postfix-type-specifier reference-qualifier?
  ///
  /// reference-qualifier -> '&'
  type* type_specifier();

  /// postfix-type-specifier -> primary-type-specifier '[' array-bound-list ']'
  ///                         | primary-type-specifier
  ///
  /// \todo Allow a sequence of array bound clauses, so that `t[e1][e2]` is
  /// equivalent to `t[e1, e2]`. This should also allow: `t[][][]` to be 
  /// equivalent to `t[,,]`
  type* postfix_type_specifier();

  /// array-bound-list -> array-bound-list ',' array-bound
  ///
  /// array-bound -> conditional-expression | <empty>
  ///
  /// \todo Rewrite the grammar to omit the empty string.
  expr_seq array_bound_list();

  /// primary-type-specifier -> simple-type-specifier
  ///                         | function-type-specifier
  ///                         | tuple-type-specifier
  ///                         '(' primary-type-specifier ')'
  ///
  /// function-type-specifier -> '(' [type-specifier-list] ')' '->' type
  type* primary_type_specifier();

  /// tuple-type-specifier -> '{' [type-specifier-list] '}'
  type* tuple_type_specifier();

  /// type-specifier-list -> type-specifier 
  ///                      | type-specifier-list ',' type-specifier
  type_seq type_specifier_list();

  /// simple-type-specifier -> 'void' | 'bool' | 'nat' | 'int'
  type* simple_type_specifier();

  // Statements

  /// statement-seq -> statement-seq statement | statement
  stmt_seq statement_seq();
  
  /// statement -> block-statement
  ///            | if-statement
  ///            | while-statement
  ///            | break-statement
  ///            | continue-statement
  ///            | return-statement
  ///            | assert-statement
  ///            | print-statement
  ///            | scan-statement
  ///            | skip-statement
  ///            | trap-statement
  ///            | expression-statement
  ///            | declaration-statement
  stmt* statement();

  /// block-statement -> '{' statement-seq '}'
  stmt* block_statement();

  /// if-statement -> 'if' '(' expression ')' statement 'else' statement
  stmt* if_statement();

  /// while-statement -> 'while' '(' expression ')' statement
  stmt* while_statement();

  /// break-statement -> 'break' ';'
  stmt* break_statement();
  
  /// continue-statement -> 'continue' ';'
  stmt* continue_statement();

  /// return-statement -> 'return' initializer? ';'
  stmt* return_statement();

  /// assert-statement -> 'assert' conditional-expression ';'
  stmt* assert_statement();

  /// print-statement -> 'print' conditional-expression ';'
  stmt* print_statement();

  /// scan-statement -> 'scan' conditional-expression ';'
  stmt* scan_statement();

  /// skip-statement -> 'skip' ';'
  stmt* skip_statement();

  /// trap-statement -> 'trap' ';'
  stmt* trap_statement();

  /// declaration-statement -> block-declaration
  stmt* declaration_statement();

  /// expression-statement -> expression ';'
  stmt* expression_statement();

  // Expressions

  /// expression -> assignment-expression
  expr* expression();

  /// assignment-expression -> logical-or-expression '=' assignment-expression
  ///                        | conditional-expression
  expr* assignment_expression();

  /// conditional-expression -> logical-or-expression '?' expression ':' expression
  ///                         | logical-or-expression
  expr* conditional_expression();

  /// logical-or-expression -> logical-or-expression '||' logical-and-expression
  ///                        | logical-and-expression
  expr* logical_or_expression();

  /// logical-and-expression -> logical-and-expression '&&' inclusive-or-expression
  ///                         | inclusive-or-expression
  expr* logical_and_expression();

  /// inclusive-or-expression -> inclusive-or-expression '|' exclusive-or-expression
  ///                          | exclusive-or-expression
  expr* inclusive_or_expression();

  /// exclusive-or-expression -> exclusive-or-expression '^' and-expression
  ///                          | and-expression
  expr* exclusive_or_expression();

  /// and-expression -> and-expression '&' ordering-expression
  ///                 | ordering-expression
  expr* and_expression();

  /// ordering-expression -> ordering-expression '<' equality-expression
  ///                      | ordering-expression '>' equality-expression
  ///                      | ordering-expression '<=' equality-expression
  ///                      | ordering-expression '>=' equality-expression
  ///                      | equality-expression
  expr* ordering_expression();

  /// equality-expression -> equality-expression '==' additive-expression
  ///                      | equality-expression '!=' additive-expression
  ///                      | additive-expression
  expr* equality_expression();

  /// additive-expression -> additive-expression '+' multiplicative-expression
  ///                      | additive-expression '-' multiplicative-expression
  ///                      | multiplicative-expression
  expr* additive_expression();

  /// multiplicative-expression -> multiplicative-expression '*' unary-expression
  ///                            | multiplicative-expression '/' unary-expression
  ///                            | multiplicative-expression '%' unary-expression
  ///                            | unary-expression
  expr* multiplicative_expression();

  /// unary-expression -> '-' unary-expression
  ///                   | postfix-expression
  expr* unary_expression();

  /// postfix-expression -> postfix-expression '(' function-argument-list? ')'
  ///                       postfix-expression '[' function-argument-list ']'
  ///                       primary-expression
  expr *postfix_expression();

  /// function-argument-list -> function-argument_list ',' function-argument
  ///                           function-argument
  expr_seq function_argument_list();

  /// function-argument -> expression
  expr* function_argument();

  /// primary-expression -> literal
  ///                     | id-expression
  ///                     | '(' expression ')'
  ///
  /// literal -> boolean-literal
  ///          | integer-literal
  ///          | character-literal
  ///          | string-literal
  ///
  /// boolean-literal -> 'true' | 'false'
  expr* primary_expression();

  /// id-expression -> identifier
  expr* id_expression();

  /// identifiers
  symbol* identifier();

  /// constant-expression -> conditional-expression
  expr* constant_expression();

  /// Returns the translator.
  translator& get_translator() { return sema; }

private:
  /// Returns true if there is no more input.
  bool eof();

  /// Returns the current token.
  token* peek();

  /// Returns the nth token past the current token.
  token* peek(int);

  /// Returns the kind of the current token.
  token_kind lookahead();

  /// Returns the nth token past the current token.
  token_kind lookahead(int);

  /// Consume the current token and fetch the next into the queue.
  token* consume();

  /// Require that the token have the given kind and consume it.
  token* require(token_kind);
  
  /// If the current token has the given kind, consume it. Otherwise, diagnose
  /// the input as being ill-formed.
  token* match(token_kind);

  /// If the current token has the given kind, consume it. Otherwise, no
  /// action is taken.
  token* match_if(token_kind);

  /// If the current token matches any in the given set, consume it. Otherwise,
  /// no action is taken.
  token* match_any(token_kind, token_kind);
  token* match_any(token_kind, token_kind, token_kind);

  /// The lexer.
  lexer lex;
  
  /// A queue of lexed tokens. This typically contains a single token or
  /// is empty. When looking past the current token, those tokens are added
  /// to the queue. We only fetch the next token when the queue is empty.
  std::deque<token*> toks;

  /// Semantic actions.
  translator sema;
};


// -------------------------------------------------------------------------- //
// Declarative regions

/// A declarative region is a portion of program text in which names can
/// be declared as particular kinds of entities.
///
/// Entering a declarative region corresponds to entering a new scope.
///
/// \todo Support different kinds of scopes with different lookup and
/// declaration rules?
struct declarative_region
{
  declarative_region(translator& sema, int sk, decl* d);
  declarative_region(translator& sema, int sk);
  ~declarative_region();

  translator& sema;
  decl* prev;
};


/// A loop region is a region of text where break and continue statements
/// are allowed.
struct loop_region
{
  loop_region(translator& sema);
  ~loop_region();
  
  translator& sema;
};

} // namespace beaker
