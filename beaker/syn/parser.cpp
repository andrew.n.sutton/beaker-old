#include "parser.hpp"

#include <cassert>
#include <iostream>
#include <sstream>


namespace beaker {

parser::parser(const keyword_table& kt, symbol_table& st, context& cxt, const std::string& str)
  : lex(kt, st, str), sema(cxt)
{
  // Queue the first token.
  if (token* tok = lex.lex())
    toks.push_front(tok);
}

bool
parser::eof()
{
  return toks.empty();
}

token*
parser::peek()
{
  if (!toks.empty())
    return toks.front();
  else
    return nullptr;
}

token*
parser::peek(int n)
{
  // peek(0) is equivalent to peek().
  if (n == 0)
    return peek();

  // We already have n tokens cached.
  if (n < toks.size())
    return toks[n];
  
  // Lex enough tokens to satisfy the requeest, and return the last token
  // lexed. If there aren't enough tokens, return null.
  n = n - toks.size() + 1;
  while (n) {
    if (token* tok = lex.lex())
      toks.push_back(tok);
    else
      return nullptr;
    --n;
  }
  return toks.back();
}

token_kind
parser::lookahead()
{
  if (token* tok = peek())
    return tok->kind;
  else
    return eof_tok;
}

token_kind
parser::lookahead(int n)
{
  if (token* tok = peek(n))
    return tok->kind;
  else
    return eof_tok;
}

token*
parser::consume()
{
  assert(!toks.empty());

  // Take the current token.
  token* tok = toks.front();
  toks.pop_front();

  // Only refill the queue when needed. 
  if (toks.empty()) {
    if (token* next = lex.lex())
      toks.push_front(next);
  }

  return tok;
}

token*
parser::require(token_kind k)
{
  assert(lookahead() == k);
  return consume();
}

token*
parser::match(token_kind k)
{
  if (lookahead() == k)
    return consume();
  else {
    std::stringstream ss;
    ss << "expected '" << token::str(k) << '\'';
    throw std::runtime_error(ss.str().c_str());
  }
}

token*
parser::match_if(token_kind k)
{
  if (lookahead() == k)
    return consume();
  else
    return nullptr;
}

token*
parser::match_any(token_kind k1, token_kind k2)
{
  const token_kind k = lookahead();
  if (k == k1 || k == k2)
    return consume();
  else
    return nullptr;
}

token*
parser::match_any(token_kind k1, token_kind k2, token_kind k3)
{
  const token_kind k = lookahead();
  if (k == k1 || k == k2 || k == k3)
    return consume();
  else
    return nullptr;
}


// -------------------------------------------------------------------------- //
// Declarative regions

/// Enter a new declarative region with of the given kind and update
/// the current declaration context.
declarative_region::declarative_region(translator& s, int sk, decl* d)
  : sema(s), prev(sema.current_context())
{
  sema.set_current_context(d);
  sema.enter_scope(sk);
}

/// Enter a new declarative region with the the given kind. This does not
/// modify the current declaration context. 
declarative_region::declarative_region(translator& s, int sk)
  : sema(s), prev(nullptr)
{
  sema.enter_scope(sk);
}

/// Restore the previous declarative region and the scope stack.
declarative_region::~declarative_region()
{
  if (prev)
    sema.set_current_context(prev);
  sema.leave_scope();
}


// -------------------------------------------------------------------------- //
// Loop regions

loop_region::loop_region(translator& sema)
  : sema(sema)
{ }

} // namespace beaker
