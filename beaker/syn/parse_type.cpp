#include "parser.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

/// type-specifier -> primary-type-specifier reference-qualifier?
///
/// reference-qualifier -> '&'
type* 
parser::type_specifier()
{
  type* t = postfix_type_specifier();
  if (match_if(amp_tok))
    return sema.on_reference_type(t);
  return t;
}

/// postfix-type-specifier -> primary-type-specifier '[' array-bound-list ']'
///                         | primary-type-specifier
///
/// \todo Allow a sequence of array bound clauses, so that `t[e1][e2]` is
/// equivalent to `t[e1, e2]`. This should also allow: `t[][][]` to be 
/// equivalent to `t[,,]`.
type* 
parser::postfix_type_specifier()
{
  type* t = primary_type_specifier();
  if (match_if (lbrack_tok)) {
    if (match_if(rbrack_tok))
      return sema.on_array_type(t);

    expr_seq es = array_bound_list();
    match(rbrack_tok);
    t = sema.on_array_type(t, std::move(es));
  }

  return t;
}

/// array-bound-list -> array-bound-list ',' array-bound
///
/// array-bound -> constant-expression | <empty>
///
/// \todo Rewrite the grammar to omit the empty string.
///
/// \todo Allow placeholder expressions to be equivalent to an omitted constant
/// expression.
expr_seq 
parser::array_bound_list()
{
  expr_seq es;
  while (true) {
    expr* e;
    if (lookahead() == comma_tok || lookahead() == rbrack_tok) {
      // Match an empty array bound.
      e = sema.on_array_bound();
    }
    else {
      // Match a non-empty array bound.
      e = conditional_expression();
      e = sema.on_array_bound(e);
    }
    es.push_back(e);

    if (match_if(comma_tok))
      continue;
    if (lookahead() == rbrack_tok)
      break;
    throw std::runtime_error("expected array-bound");
  }
  return es;
}


/// primary-type-specifier -> simple-type-specifier
///                         | function-type-specifier
///                         | tuple-type-specifier
///                         '(' primary-type-specifier ')'
///
/// function-type-specifier -> '(' type-list? ')' '->' type
///
/// \todo Implement parsing for grouped and function types. 
type* 
parser::primary_type_specifier()
{
  // Parse '(' type-specifier-list ')' and then figure out if its a grouped
  // type expression or if its a function type.
  if (lookahead() == lparen_tok) {
    match(lparen_tok);

    // Check for '()->t'.
    if (match_if(rparen_tok)) {
      match(arrow_tok);
      type* t = type_specifier();
      return sema.on_function_type(t);
    }

    // Match '(' type-specifier-list ')'.
    type_seq ts = type_specifier_list();
    match(rparen_tok);

    // We parsed '(t)'.
    if (ts.size() == 1 && lookahead() != arrow_tok)
      return ts.front();

    // This must be a function type.
    match(arrow_tok);
    type* t = type_specifier();
    return sema.on_function_type(std::move(ts), t);
  }

  if (lookahead() == lbrace_tok)
    return tuple_type_specifier();

  return simple_type_specifier();
}

/// type-specifier-list -> type-specifier 
///                      | type-specifier-list ',' type-specifier
type_seq
parser::type_specifier_list()
{
  type_seq ts;
  while (true) {
    type* t = type_specifier();
    ts.push_back(t);
    if (match_if(comma_tok))
      continue;
    if (lookahead() == rparen_tok || lookahead() == rbrace_tok)
      break;
    else
      throw std::runtime_error("expected type-specifier");
  }
  return ts;
}

/// tuple-type-specifier -> '{' [type-specifier-list] '}'
type* 
parser::tuple_type_specifier()
{
  require(lbrace_tok);
  if (match_if(rbrace_tok)) {
    return sema.on_tuple_type();
  }
  else {
    type_seq ts = type_specifier_list();
    match(rbrace_tok);
    return sema.on_tuple_type(std::move(ts));
  }
}

/// simple-type-specifier -> 'void' | 'bool' | 'nat' | 'int'
type* 
parser::simple_type_specifier()
{
  switch (lookahead()) {
    case void_kw:
      return sema.on_void_type(consume());
    case bool_kw:
      return sema.on_bool_type(consume());
    case int_kw:
      return sema.on_int_type(consume());
    case nat_kw:
      return sema.on_nat_type(consume());
    default:
      break;
  }
  throw std::runtime_error("expected simple-type-specifier");
}

} // namespace beaker
