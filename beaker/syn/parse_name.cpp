#include "parser.hpp"


namespace beaker {

/// identifiers
symbol*
parser::identifier()
{
  token* id = match(id_tok);
  return sema.on_identifier(id);
}

} // namespace beaker
