#include "parser.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

/// expression -> assignment-expression
expr*
parser::expression()
{
  return assignment_expression();
}

/// assignment-expression -> logical-or-expression '=' assignment-expression
///                        | conditional-expression
///
/// Note that this duplicates the parsing for conditional expressions since
/// both start with a logical-or-expression.
expr*
parser::assignment_expression()
{
  expr* e1 = logical_or_expression();
  if (match_if(question_tok)) {
    expr* e2 = expression();
    match(colon_tok);
    expr* e3 = expression();
    return sema.on_cond_expression(e1, e2, e3);
  }
  if (match_if(eq_tok)) {
    expr* e2 = assignment_expression();
    return sema.on_assign_expression(e1, e2);
  }
  return e1;
}

/// conditional-expression -> logical-or-expression '?' expression ':' expression
///                         | logical-or-expression
expr*
parser::conditional_expression()
{
  expr* e1 = logical_or_expression();
  if (match_if(question_tok)) {
    expr* e2 = expression();
    match(colon_tok);
    expr* e3 = expression();
    return sema.on_cond_expression(e1, e2, e3);
  }
  return e1;
}

/// logical-or-expression -> logical-or-expression '||' logical-and-expression
///                        | logical-and-expression
expr*
parser::logical_or_expression()
{
  expr* e1 = logical_and_expression();
  while (true) {
    if (match_if(bar_bar_tok)) {
      expr* e2 = logical_and_expression();
      e1 = sema.on_logical_or_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// logical-and-expression -> logical-and-expression '&&' inclusive-or-expression
///                         | inclusive-or-expression
expr*
parser::logical_and_expression()
{
  expr* e1 = inclusive_or_expression();
  while (true) {
    if (match_if(amp_amp_tok)) {
      expr* e2 = inclusive_or_expression();
      e1 = sema.on_logical_and_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// inclusive-or-expression -> inclusive-or-expression '|' exclusive-or-expression
///                          | exclusive-or-expression
expr*
parser::inclusive_or_expression()
{
  expr* e1 = exclusive_or_expression();
  while (true) {
    if (match_if(bar_tok)) {
      expr* e2 = exclusive_or_expression();
      e1 = sema.on_or_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// exclusive-or-expression -> exclusive-or-expression '^' and-expression
///                          | and-expression
expr*
parser::exclusive_or_expression()
{
  expr* e1 = and_expression();
  while (true) {
    if (match_if(caret_tok)) {
      expr* e2 = and_expression();
      e1 = sema.on_xor_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// and-expression -> and-expression '&' ordering-expression
///                 | ordering-expression
expr*
parser::and_expression()
{
  expr* e1 = ordering_expression();
  while (true) {
    if (match_if(amp_tok)) {
      expr* e2 = ordering_expression();
      e1 = sema.on_and_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// ordering-expression -> ordering-expression '<' equality-expression
///                      | ordering-expression '>' equality-expression
///                      | ordering-expression '<=' equality-expression
///                      | ordering-expression '>=' equality-expression
///                      | equality-expression
expr*
parser::ordering_expression()
{
  expr* e1 = equality_expression();
  while (true) {
    if (match_if(lt_tok)) {
      expr* e2 = equality_expression();
      e1 = sema.on_lt_expression(e1, e2);
    }
    else if (match_if(gt_tok)) {
      expr* e2 = equality_expression();
      e1 = sema.on_gt_expression(e1, e2);
    }
    else if (match_if(le_tok)) {
      expr* e2 = equality_expression();
      e1 = sema.on_le_expression(e1, e2);
    }
    else if (match_if(ge_tok)) {
      expr* e2 = equality_expression();
      e1 = sema.on_ge_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// equality-expression -> equality-expression '==' additive-expression
///                      | equality-expression '!=' additive-expression
///                      | additive-expression
expr*
parser::equality_expression()
{
  expr* e1 = additive_expression();
  while (true) {
    if (match_if(eq_eq_tok)) {
      expr* e2 = additive_expression();
      e1 = sema.on_eq_expression(e1, e2);
    }
    else if (match_if(bang_eq_tok)) {
      expr* e2 = additive_expression();
      e1 = sema.on_ne_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}

/// additive-expression -> additive-expression '+' multiplicative-expression
///                      | additive-expression '-' multiplicative-expression
///                      | multiplicative-expression
expr*
parser::additive_expression()
{
  expr* e1 = multiplicative_expression();
  while (true) {
    if (match_if(plus_tok)) {
      expr* e2 = multiplicative_expression();
      e1 = sema.on_add_expression(e1, e2);
    }
    else if (match_if(minus_tok)) {
      expr* e2 = multiplicative_expression();
      e1 = sema.on_sub_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}


/// multiplicative-expression -> multiplicative-expression '*' unary-expression
///                            | multiplicative-expression '/' unary-expression
///                            | multiplicative-expression '%' unary-expression
///                            | unary-expression
expr*
parser::multiplicative_expression()
{
  expr* e1 = unary_expression();
  while (true) {
    if (match_if(star_tok)) {
      expr* e2 = unary_expression();
      e1 = sema.on_mul_expression(e1, e2);
    }
    else if (match_if(slash_tok)) {
      expr* e2 = unary_expression();
      e1 = sema.on_div_expression(e1, e2);
    }
    else if (match_if(percent_tok)) {
      expr* e2 = unary_expression();
      e1 = sema.on_rem_expression(e1, e2);
    }
    else
      break;
  }
  return e1;
}


/// unary-expression -> '-' unary-expression
///                   | '!' unary-expression
///                   | primary-expression
expr*
parser::unary_expression()
{
  if (match_if(minus_tok)) {
    expr* e = unary_expression();
    return sema.on_neg_expression(e);
  }
  else if (match_if(bang_tok)) {
    expr* e = unary_expression();
    return sema.on_not_expression(e);
  }
  else {
    return postfix_expression();
  }
}

/// postfix-expression -> postfix-expression '(' [function-argument-list] ')'
///                       postfix-expression '[' function-argument-list ']'
///                       primary-expression
///
/// \todo Implement multi-dimensional array indexes.
expr *
parser::postfix_expression()
{
  expr* e1 = primary_expression();
  while (true) {
    if (match_if(lparen_tok)) {
      expr_seq args;
      if (lookahead() != rparen_tok)
        args = function_argument_list();
      match(rparen_tok);
      e1 = sema.on_call_expression(e1, std::move(args));
    }
    else if (match_if(lbrack_tok)) {
      expr_seq args = function_argument_list();
      match(rbrack_tok);
      e1 = sema.on_index_expression(e1, std::move(args));
    }
    else {
      break;
    }
  }
  return e1;
}

/// function-argument-list -> function-argument_list ',' function-argument
///                           function-argument
expr_seq
parser::function_argument_list()
{
  expr_seq args;
  while (true) {
    expr* arg = function_argument();
    args.push_back(arg);
    if (match_if(comma_tok))
      continue;
    // The terminating token depends no whether we're parsing a function
    // call or an index expression.
    if (lookahead() == rparen_tok || // function argument list
        lookahead() == rbrace_tok || // brace-init-list
        lookahead() == rbrack_tok)   // array index
      break;
    else
      throw std::runtime_error("expected function-argument");
  }
  return args;
}

/// function-argument -> expression
expr* 
parser::function_argument()
{
  return expression();
}

/// primary-expression -> boolean-literal
///                     | integer-literal
///                     | id-expression
///                     | '{' function-argument-list '}'
///                     | '(' expression ')'
expr*
parser::primary_expression()
{
  switch (lookahead()) {
    case true_kw:
      return sema.on_true_literal(consume());
    case false_kw:
      return sema.on_false_literal(consume());
    case int_tok:
      return sema.on_integer_literal(consume());
    case char_tok:
      return sema.on_character_literal(consume());
    case str_tok:
      return sema.on_string_literal(consume());
    case id_tok:
      return id_expression();
    case lbrace_tok: { // '{' function-argument-list '}'
      match(lbrace_tok);
      expr_seq es = function_argument_list();
      match(rbrace_tok);
      return sema.on_tuple_expression(std::move(es));
    }
    case lparen_tok: { // '(' expression ')'
      consume();
      expr* e = expression();
      match(rparen_tok);
      return e;
    }
    default:
      break;
  }
  throw std::runtime_error("expected primary-expression");
}

/// id-expression -> identifier
expr*
parser::id_expression()
{
  symbol* id = identifier();
  return sema.on_id_expression(id);
}

/// constant-expression -> conditional-expression
expr*
parser::constant_expression()
{
  expr* e = conditional_expression();
  return sema.on_constant_expression(e);
}

} // namespace beaker
