#include "lookup.hpp"
#include "translator.hpp"

#include <beaker/ast/context.hpp>
#include <beaker/ast/print.hpp>

#include <iostream>
#include <sstream>


namespace beaker {

decl_seq
translator::unqualified_lookup(const symbol *sym)
{
  decl_seq ds;
  if (auto* ent = env.lookup(sym))
    ds.push_back(ent->get_declaration());

  // FIXME: We also need to search for namespace imports (whenever we
  // get namespaces and imports).

  return ds;
}

// -------------------------------------------------------------------------- //
// Diagnostics

std::string
unresolved_id_error::format() const
{
  std::stringstream ss;
  ss << "no matching declaration for " << quote(sym) << '\n';
  return ss.str();
}

} // namespace beaker
