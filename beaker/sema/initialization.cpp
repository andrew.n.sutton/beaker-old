#include "translator.hpp"

#include <beaker/ast/lang.store/types_store.hpp>
#include <beaker/ast/lang.store/exprs_store.hpp>
#include <beaker/ast/lang.fn/types_fn.hpp>
#include <beaker/ast/lang.array/types_array.hpp>
#include <beaker/ast/lang.array/exprs_array.hpp>
#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

/// Trivial initialization initializes an object of type `t` with an 
/// indeterminate value. Note that this value may be the existing bits in the
/// storage of the object or an explicit trap representation. Trivial 
/// initialization depends on the type `t`.
///
/// - If `t` is scalar, the object is initialized with an indeterminate value.
///
/// - If `t` is an aggregate, each element is trivially initialized.
///
/// - Otherwise the program is ill-formed.
///
/// Returns an expression denoting the trivial initialization of an object.
expr*
translator::trivially_initialize(type* t)
{
  if (is_scalar(t))
    return new lang_store::nop_init(t);
  
  if (auto* at = dynamic_cast<lang_array::array_type*>(t)) {
    // Find a trivial initializer for all rows.
    expr* e = trivially_initialize(at->get_row_type(*cxt));
    return new lang_array::array_init(t, e);
  }

  if (auto* tt = dynamic_cast<lang_array::tuple_type*>(t)) {
    // Find a trivial initializer for each element.
    expr_seq args(tt->size());
    std::transform(tt->begin(), tt->end(), args.begin(), [this](type* t) {
      return trivially_initialize(t);
    });
    return new lang_array::elem_init(tt, std::move(args));
  }

  throw std::runtime_error("invalid trivial initialization");
}

/// Zero initialization assigns an appropriately typed 0 value to an object.
/// An object of type `t` is zero initialized according to the following
/// rules:
///
/// - If `t` is a scalar type, then the object is initialized by the value 0 
///   cast to `t`. That is, the object receives a bit pattern of all 0's.
///
/// - If `t` is an aggregate, each element of the array is zero-initialized.
///
/// - Otherwise, the program is ill-formed.
///
/// Returns an expression indicating the zero-initialization of a declaration.
expr*
translator::zero_initialize(type* t)
{
  if (is_scalar(t))
    return new lang_store::zero_init(t);

  if (auto* at = dynamic_cast<lang_array::array_type*>(t)) {
    // Find a zero initializer for all rows.
    expr* e = zero_initialize(at->get_row_type(*cxt));
    return new lang_array::array_init(t, e);
  }

  if (auto* tt = dynamic_cast<lang_array::tuple_type*>(t)) {
    // Find a zero initializer for each element.
    expr_seq args(tt->size());
    std::transform(tt->begin(), tt->end(), args.begin(), [this](type* t) {
      return zero_initialize(t);
    });
    return new lang_array::elem_init(tt, std::move(args));
  }

  throw std::runtime_error("invalid zero initialization");
}

/// Default initialization assigns a default value to an object. Default
/// initialization depends on the type `t`.
///
/// - If `t` is a scalar type, the object is zero initialized.
///
/// - If `t` is an aggregate, then each element is default-initialized.
///
/// - If `t` is a reference type, the program is ill-formed.
///
/// Returns an expression representing the default initialization of an object.
expr*
translator::default_initialize(type* t)
{
  if (is_reference(t))
    throw std::runtime_error("default initialization of reference");

  if (is_scalar(t))
    return zero_initialize(t);

  if (auto* at = dynamic_cast<lang_array::array_type*>(t)) {
    // Find a default initializer for all rows.
    expr* e = default_initialize(at->get_row_type(*cxt));
    return new lang_array::array_init(t, e);
  }

  if (auto* tt = dynamic_cast<lang_array::tuple_type*>(t)) {
    // Find a default initializer for each element.
    expr_seq args(tt->size());
    std::transform(tt->begin(), tt->end(), args.begin(), [this](type* t) {
      return default_initialize(t);
    });
    return new lang_array::elem_init(tt, std::move(args));
  }

  throw std::logic_error("invalid type for default initialization");
}

/// Copy initializations assigns the value of an expression `e` to a variable 
/// of type `t` according to the following rules:
///
/// If `t` is a reference, then the variable is reference-initialized.
///
/// - If `t` is a reference, then reference initialization is performed.
///
/// - If `t` is an aggregate, then:
///
///   - If `e` is a tuple expression, then perform aggregate initialization
///     using the elements in `t`.
///
///   - otherwise, each subobject is copy initialized by the corresponding 
///     subobject in the value `e`.
///
/// - Otherwise the object is initialized by the converted expression `e`.
///
/// Returns an expression indicating that copy initialization is required.
/// The type of the expression is `t`.
expr*
translator::copy_initialize(type* t, expr* e)
{
  if (is_reference(t))
    return reference_initialize(t, e);

  if (is_aggregate(t)) {
    // FIXME: Can we generalize this to support aggregate initialization
    // from tuple-typed expressions and not just tuple-shaped expressions?
    // Probably not. We need access to the sequence of initializers. 
    if (auto* te = dynamic_cast<lang_array::tuple_expr*>(e))
      return aggregate_initialize(t, te->get_elements());
    else
      return new lang_array::copy_init(t, e);
  }

  // e is converted to t.
  e = standard_conversion(e, t);
  return new lang_store::copy_init(t, e);
}

/// Reference initializations binds a reference of type `t` to an expression 
/// `e`. The initializer `e` shall have the type of `t`.
///
/// Returns an expression indicating that reference initialization is required.
/// The type of the expression is `t`.
///
/// \todo Support conversion. Support materialization/lifetime extension.
expr*
translator::reference_initialize(type* t, expr* e)
{
  if (!is_reference(e))
    throw std::runtime_error("binding a reference to a value");
  if (!e->has_type(t))
    throw std::runtime_error("reference type mismatch");
  
  return new lang_store::bind_init(t, e);  
}

/// Aggregate initialization copy initializes each subobject of an aggregate
/// with its corresponding value in a list of expressions.
expr*
translator::aggregate_initialize(type* t, expr_seq& es)
{
  if (is_array(t))
    return array_initialize(t, es);
  if (is_tuple(t))
    return tuple_initialize(t, es);
  throw std::runtime_error("aggregate initialization of non-aggregate");
}

/// This is a subroutine of aggregate_initialize. Copy initialize each row 
/// of the array with its corresponding value in the list of expressions.
///
/// \todo Support nested array initializer.
///
///   int[1][1] a { {0} }; // ok
///
/// This implies that we need a kind of tuple expression or, like C++, an
/// untyped brace-init-list syntax. I'd prefer tuples.
expr*
translator::array_initialize(type* t, expr_seq& es)
{
  auto* at = static_cast<lang_array::array_type*>(t);
  auto* rt = at->get_row_type(*cxt);

  if (at->get_extent(0) < es.size())
    throw std::runtime_error("too many initializers");
  if (at->get_extent(0) > es.size())
    throw std::runtime_error("too few initializers");

  // Copy initialize each element of the array.
  expr_seq args(es.size());
  std::transform(es.begin(), es.end(), args.begin(), [this, rt](expr* e) {
    return copy_initialize(rt, e);
  });

  return new lang_array::elem_init(at, std::move(args));
}

/// This is a subroutine of aggregate_initialize. Copy initialize each element 
/// of the tuple with its corresponding value in the list of expressions.
expr*
translator::tuple_initialize(type* t, expr_seq& es)
{
  auto* tt = static_cast<lang_array::tuple_type*>(t);

  if (tt->size() < es.size())
    throw std::runtime_error("too many initializers");
  if (tt->size() > es.size())
    throw std::runtime_error("too few initializers");

  // Copy initialize each element of the tuple.
  expr_seq args(es.size());
  std::transform(tt->begin(), tt->end(), es.begin(), args.begin(), [this](type* t, expr* e) {
    return copy_initialize(t, e);
  });

  return new lang_array::elem_init(tt, std::move(args));
}


} // namespace beaker