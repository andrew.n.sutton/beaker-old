#include "conversion.hpp"
#include "translator.hpp"

#include <beaker/ast/print.hpp>
#include <beaker/ast/lang.store/types_store.hpp>
#include <beaker/ast/lang.store/exprs_store.hpp>
#include <beaker/ast/lang.fn/types_fn.hpp>

#include <iostream>
#include <sstream>


namespace beaker {

const type*
conv_pair::get_type() const
{ 
  return first->get_type(); 
}

type*
conv_pair::get_type()
{ 
  return first->get_type(); 
}

/// Attempt to convert an expression `e` to its value type. If `e` is a
/// reference with the `t&`, then the conversion is a value expression with
/// type `t`. This is used to load a value from storage.
expr* 
translator::reference_conversion(expr* e)
{
  type* t = e->get_type();
  if (auto* ref = dynamic_cast<lang_store::ref_type*>(t)) {
    t = ref->get_object_type();
    return new lang_store::value_expr(t, e);
  }
  return e;
}

expr* 
translator::standard_conversion(expr* e, type* t)
{
  // The expression already has type t.
  if (e->has_type(t))
    return e;

  // Reference-to-value conversions.
  e = reference_conversion(e);
  if (e->has_type(t))
    return e;

  throw standard_conversion_error(e, t);
}

expr*
translator::function_conversion(expr* e)
{
  type *t = e->get_type();

  // Reference-to-function conversion.
  e = reference_conversion(e);
  if (is_function(e->get_type()))
    return e;
  
  throw function_conversion_error(e);
}

expr*
translator::boolean_conversion(expr* e)
{
  return standard_conversion(e, cxt->get_bool_type());
}

conv_pair
translator::boolean_conversion(expr* e1, expr* e2)
{
  return {boolean_conversion(e1), boolean_conversion(e2)};
}

/// \todo Perform integer promotion.
expr*
translator::arithmetic_conversion(expr* e)
{
  return standard_conversion(e, cxt->get_int_type());
}

/// \todo Propagate source locations for converted expressions.
///
/// \todo Take a source location for the operator that requires a common
/// arithmetic type.
conv_pair
translator::arithmetic_conversion(expr* e1, expr* e2)
{
  type* t1 = e1->get_type();
  type* t2 = e2->get_type();

  // Apply reference-to-value conversions.
  e1 = reference_conversion(e1);
  t1 = e1->get_type();
  e2 = reference_conversion(e2);
  t2 = e2->get_type();

  // FIXME: Only allow arithmetic types. We only have arithmetic types
  // for now, so it's okay.

  if (equal(t1, t2))
    return {e1, e2};

  // FIXME: Replace this with a meaningful error.
  throw std::runtime_error("cannot perform arithmetic conversions");
}

expr*
translator::integral_conversion(expr* e)
{
  e = reference_conversion(e);
  if (is_integral(e->get_type()))
    return e;

  // FIXME: Improve diagnostics.
  throw std::runtime_error("cannot perform integral conversion");
}

/// \todo Define common type and then perform the conversions needed
/// to reach the common type.
conv_pair
translator::common_conversion(expr* e1, expr* e2)
{
  type* t1 = e1->get_type();
  type* t2 = e2->get_type();

  if (equal(t1, t2))
    return {e1, e2};

  // FIXME: Replace this with a meaningful error.
  throw std::runtime_error("cannot determine common type");
}


// -------------------------------------------------------------------------- //
// Conversion errors

std::string
standard_conversion_error::format() const
{
  std::stringstream ss;
  ss << "no conversion from " 
     << quote(src) << " (type " << quote(src->get_type()) << ") to " 
     << quote(dst);
  return ss.str();
}

std::string
function_conversion_error::format() const
{
  std::stringstream ss;
  ss << "cannot convert " << quote(src) << " to a function";
  return ss.str();
}

} // namespace beaker
