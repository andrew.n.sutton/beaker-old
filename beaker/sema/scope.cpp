#include "scope.hpp"

#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

const char*
scope::get_scope_name() const
{
  switch (kind) {
    default: throw std::logic_error("unknown scope kind");
    case module_scope: return "module_scope";
    case function_parameter_scope: return "function_parameter_scope";
    case function_scope: return "function_scope";
    case block_scope: return "block_scope";
  }
}

/// \todo What if we want to merge d into the current binding level (i.e.,
/// generate a declaration or overload set?).
void
environment::add(scope* s, named_decl* d)
{
  // Insert d into the given scope.
  s->add(d);

  // Push the declaration onto the bindings.
  auto result = map.emplace(d->get_name(), bindings{});
  bindings& decls = result.first->second;
  assert((decls.empty() ? true : (decls.top().s != s)) && "cannot rebind declaration");
  decls.push(s, d);
}

/// Returns the bindings for the given name.
auto
environment::get_bindings(const symbol* n) const -> const bindings*
{
  auto iter = map.find(n);
  if (iter != map.end())
    return &iter->second;
  else
    return nullptr;
}

/// Returns the bindings for the given name.
auto
environment::get_bindings(const symbol* n) -> bindings*
{
  auto iter = map.find(n);
  if (iter != map.end())
    return &iter->second;
  else
    return nullptr;
}


/// Returns the innermost declaration for the name.
auto
environment::lookup(const symbol* n) const -> const entry*
{
  if (const bindings* decls = get_bindings(n)) {
    if (!decls->empty())
      return &decls->top();
  }
  return nullptr;
}

/// Returns the innermost declaration for the name.
auto
environment::lookup(const symbol* n) -> entry*
{
  if (bindings* decls = get_bindings(n)) {
    if (!decls->empty())
      return &decls->top();
  }
  return nullptr;
}

/// Enter a new scope.
///
/// \todo Allow this to dynamically allocate derived scopes.
void
environment::enter_scope(int k)
{
  stack.push(k);
}

/// Leave the current scope. This removes the innermost binding for each
/// declaration in the scope.
void
environment::leave_scope()
{
  scope* s = current_scope();
  for (decl* d : *s)
    remove(static_cast<named_decl*>(d));
  stack.pop();
}

void
print_scope(std::ostream& os, const scope* s) 
{
  os << "scope: " << s->get_scope_name() << '\n';
  for (const decl* d : *s) {
    if (const symbol* n = d->get_name()) {
      os << *n << " : ";
      if (const type* t = d->get_type())
        os << pretty(t);
      else
        os << "<untyped>";
      os << '\n';
    }
  }
}

} // namespace beaker
