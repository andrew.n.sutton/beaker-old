#include "translator.hpp"
#include "lookup.hpp"

#include <beaker/lex/tokens.hpp>
#include <beaker/ast/context.hpp>
#include <beaker/ast/evaluate.hpp>
#include <beaker/ast/print.hpp>
#include <beaker/ast/lang.bool/exprs_bool.hpp>
#include <beaker/ast/lang.int/exprs_int.hpp>
#include <beaker/ast/lang.store/types_store.hpp>
#include <beaker/ast/lang.store/exprs_store.hpp>
#include <beaker/ast/lang.fn/types_fn.hpp>
#include <beaker/ast/lang.fn/exprs_fn.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>
#include <beaker/ast/lang.array/types_array.hpp>
#include <beaker/ast/lang.array/exprs_array.hpp>

#include <iostream>
#include <sstream>


namespace beaker {

expr*
translator::on_assign_expression(expr* e1, expr* e2)
{
  type* t = e1->get_type();
  if (!is_reference(t)) {
    std::stringstream ss;
    ss << "assignment to non-reference " << quote(e1);
    throw std::runtime_error(ss.str());
  }
  
  // e2 is a value.
  e2 = reference_conversion(e2);

  // The converted e2 shall and the object type of e1 shall be the same.
  if (!e2->has_type(get_object_type(t)))
    throw std::runtime_error("assignment from invalid type");

  return new lang_store::assign_expr(t, e1, e2);
}

expr*
translator::on_cond_expression(expr* e1, expr* e2, expr* e3)
{
  expr* c1 = boolean_conversion(e1);
  conv_pair cs = common_conversion(e2, e3);
  return new lang_bool::if_expr(cs.get_type(), c1, cs.first, cs.second);
}

expr*
translator::on_logical_and_expression(expr* e1, expr* e2)
{
  conv_pair cs = boolean_conversion(e1, e2);
  return new lang_bool::and_if_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_logical_or_expression(expr* e1, expr* e2)
{
  conv_pair cs = boolean_conversion(e1, e2);
  return new lang_bool::or_if_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_or_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    return new lang_bool::or_expr(cs.get_type(), cs.first, cs.second);
  else
    throw std::logic_error("inter binary operations not implemented");
}

expr*
translator::on_xor_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    return new lang_bool::xor_expr(cs.get_type(), cs.first, cs.second);
  else
    throw std::logic_error("inter binary operations not implemented");
}

expr*
translator::on_and_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    return new lang_bool::and_expr(cs.get_type(), cs.first, cs.second);
  else
    throw std::logic_error("inter binary operations not implemented");
}

// FIXME: Re-implement these using a template to simplify logic.
//
// FIXME: Provide ordering operators for boolean values.

expr*
translator::on_lt_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    throw std::runtime_error("comparison of boolean values");
  return new lang_int::lt_expr(cxt->get_bool_type(), cs.first, cs.second);
}

expr*
translator::on_gt_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    throw std::runtime_error("comparison of boolean values");
  return new lang_int::gt_expr(cxt->get_bool_type(), cs.first, cs.second);
}

expr*
translator::on_le_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    throw std::runtime_error("comparison of boolean values");
  return new lang_int::le_expr(cxt->get_bool_type(), cs.first, cs.second);
}

expr*
translator::on_ge_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    throw std::runtime_error("comparison of boolean values");
  return new lang_int::ge_expr(cxt->get_bool_type(), cs.first, cs.second);
}

expr*
translator::on_eq_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    return new lang_bool::eq_expr(cxt->get_bool_type(), cs.first, cs.second);
  else
    return new lang_int::eq_expr(cxt->get_bool_type(), cs.first, cs.second);
}

expr*
translator::on_ne_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  if (equal(cs.get_type(), cxt->get_bool_type()))
    return new lang_bool::xor_expr(cxt->get_bool_type(), cs.first, cs.second);
  else
    return new lang_int::ne_expr(cxt->get_bool_type(), cs.first, cs.second);
}

// FIXME: Handle arithmetic operations for boolean values.

expr*
translator::on_add_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  return new lang_int::add_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_sub_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  return new lang_int::sub_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_mul_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  return new lang_int::mul_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_div_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  return new lang_int::div_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_rem_expression(expr* e1, expr* e2)
{
  conv_pair cs = arithmetic_conversion(e1, e2);
  return new lang_int::rem_expr(cs.get_type(), cs.first, cs.second);
}

expr*
translator::on_neg_expression(expr* e1)
{
  expr* c = arithmetic_conversion(e1);
  return new lang_int::neg_expr(c->get_type(), c);
}

expr*
translator::on_not_expression(expr* e1)
{
  e1 = boolean_conversion(e1);
  return new lang_bool::not_expr(e1->get_type(), e1);
}

expr*
translator::on_call_expression(expr* e, expr_seq&& args)
{
  e = function_conversion(e);
  auto* t = dynamic_cast<lang_fn::fn_type*>(e->get_type());
  if (!t)
    throw std::runtime_error("callee is not a function");

  // FIXME: Initialize parameters from default arguments. Also, handle 
  // variadic functions and parameter packs.
  type_seq& parms = t->get_parameter_types();
  if (args.size() < parms.size())
    throw std::runtime_error("too few arguments");
  if (args.size() > parms.size())
    throw std::runtime_error("too many arguments");

  // Copy initialize each parameter with its corresponding argument.
  expr_seq conv;
  auto parm_iter = parms.begin();
  auto arg_iter = args.begin();
  while (parm_iter != parms.end() && arg_iter != args.end()) {
    // FIXME: In certain instances, we need to materialize a temporary. For
    // example, this is one such case:
    //
    //    def f(int[3] a) ...
    //
    //    f({0, 1, 2});
    //
    // The expression {0, 1, 2} needs to be used as the initializer for the
    // int[3] argument. The resulting set of expressions would be something
    // like: __value(_temp(__elem(...))). Here, the __temp expression creates
    // a new temporary and the initializers are given by the __elem expression.
    //
    // Note that we'd get the same for operands of other expressions on
    // aggregates:
    //
    //    {a, b, c} == {0, 1, 2}
    //
    // would result in the materialization of two aggregates (although we could
    // probably optimize that away?).
    expr* arg = copy_initialize(*parm_iter, *arg_iter);
    conv.push_back(arg);
    ++parm_iter;
    ++arg_iter;
  }

  type* ret = t->get_return_type();

  expr* call = new lang_fn::call_expr(ret, e, std::move(conv));

  // If the return type is an aggregate, materialize a temporary.
  if (is_aggregate(ret)) {
    decl* fn = current_function();
    expr* init = new lang_array::ret_init(ret, call);
    decl* tmp = new lang_fn::tmp_decl(uid(), fn, nullptr, ret, init);
    return new lang_array::tmp_expr(ret, tmp);
  }
  else {
    return call;
  }
}

/// Build an expression that accesses an element of an array or tuple.
///
/// If `e1` is an array, the expression computes the subobject designated
/// by the sequence of integer values in `es`.
///
/// If `e1` is a tuple, the expression computes the subobject designated
/// by the single value in `es`.
///
/// \todo Update the specification to manage index sequences.
///
/// \todo Support array and tuple values.
expr*
translator::on_index_expression(expr* e1, expr_seq&& es)
{
  type* t = get_object_type(e1->get_type());
  
  if (auto* at = dynamic_cast<lang_array::array_type*>(t))
    return on_array_index_expression(at, e1, std::move(es));

  if (auto* tt = dynamic_cast<lang_array::tuple_type*>(t)) {
    if (es.size() > 1)
      throw std::runtime_error("multidimensional access of tuple");
    return on_tuple_index_expression(tt, e1, es[0]);
  }

  throw std::runtime_error("not an array or tuple");
}

expr*
translator::on_array_index_expression(lang_array::array_type* t, expr* e1, expr_seq&& es)
{
  if (es.size() > t->get_rank())
    // The number of array indexes shall not exceed the rank.
    throw std::runtime_error("too many indexes for array");

  // Process each array subscript in turn.
  expr_seq exts(es.size());
  for (std::size_t i = 0; i < es.size(); ++i) {
    expr* e = es[i];

    // e is a converted integer expression.
    e = integral_conversion(e);

    // e shall be in the range [0, `n`) where `n` is the extent of the array
    // type. Try to determine bounds errors at compile time.
    e = try_constant_expression(e);
    if (auto* ce = dynamic_cast<lang_array::const_expr*>(e)) {
      std::intmax_t n = ce->get_value().get_int();
      if (n < 0 || t->get_extent(i) <= n)
        throw std::runtime_error("array index out of bounds");
    }

    exts[i] = e;
  }

  // If `e1` is a reference, the type of the expression is `t&`. Otherwise, 
  // the type of the expression is `t`.
  type* et =  t->get_subobject_type(*cxt, exts);

  if (is_reference(e1->get_type()))
    et = cxt->get_reference_type(et);

  return new lang_array::index_expr(et, e1, std::move(exts));
}

expr*
translator::on_tuple_index_expression(lang_array::tuple_type* t, expr* e1, expr* e2)
{
  // e2 is a converted integer expression.
  e2 = integral_conversion(e2);

  // e2 is a constant expression.
  e2 = require_constant_expression(e2);

  // The value of `e2` shall be in the range [0, `n`) where `n` is the number
  // of elements in the tuple type.
  std::intmax_t n = get_constant_value(e2).get_int();
  if (n < 0 || t->size() <= n)
    throw std::runtime_error("tuple index out of bounds");

  // If `e1` is a reference, the type of the expression is `t&`. Otherwise, 
  // the type of the expression is `t`. Here, `t` is the type of the requested
  // subobject.
  type* et =  t->get_element_type(n);
  if (is_reference(e1->get_type()))
    et = cxt->get_reference_type(et);

  return new lang_array::proj_expr(et, e1, e2);
}


expr*
translator::on_id_expression(symbol* sym)
{
  decl_seq found = unqualified_lookup(sym);

  // FIXME: An unresolved identifier may still be valid if used in a call
  // expression (think C++ ADL).
  if (found.empty())
    throw unresolved_id_error(sym);
  
  // FIXME: Support overload resolution -- in some cases.
  if (found.size() > 1)
    throw std::runtime_error("name is ambiguous");

  return on_id_expression(found.front());
}

/// Build an id-expression that refers to the given declaration. This is
/// primarily used by semantic facilities outside of the parser.
expr*
translator::on_id_expression(decl* d)
{
  assert(dynamic_cast<typed_decl*>(d) && "reference to an untyped declaration");
  auto* td = static_cast<typed_decl*>(d);
  
  type* t = td->get_type();

  // A name referring to a function with type `(ts...)->t` has that type.
  if (is_function(d)) 
    return new lang_store::ref_expr(t, td);

  // A name referring to a constant with type `t` has that type.
  if (is_constant(d))
    return new lang_store::ref_expr(t, td);

  // A name referring to a variable with object type `t` has type `t&`. If
  // the variable has reference type `t&`, the expression has that type.
  if (is_variable(td)) {
    if (is_object(t))
      t = cxt->get_reference_type(t);
    return new lang_store::ref_expr(t, td);
  }

  // FIXME: Implement better diagnostics.
  std::cout << "HERE: " << verbose(td) << ' ' << is_variable(td) << '\n';
  throw std::runtime_error("declaration is not a value or function");
}

expr*
translator::on_true_literal(token*)
{
  return new lang_bool::bool_expr(cxt->get_bool_type(), true);
}

expr*
translator::on_false_literal(token*)
{
  return new lang_bool::bool_expr(cxt->get_bool_type(), false);
}

/// Integer literals have type `int`.
expr*
translator::on_integer_literal(token* tok)
{
  int_token* num = static_cast<int_token*>(tok);
  return new lang_int::int_expr(cxt->get_int_type(), num->get_value());
}

expr*
translator::on_character_literal(token* tok)
{
  std::cout << "CHAR: " << tok->str() << '\n';
  throw std::runtime_error("not implemented");
}

expr*
translator::on_string_literal(token* tok)
{
  std::cout << "STR: " << tok->str() << '\n';
  throw std::runtime_error("not implemented");
}

/// In general, tuple expressions are used to initialize an object. In 
/// practice, they wlll very rarely occur as outside of an initialization
/// context. However, we do not always which object is being initialized, or
/// what type it has. This function computes an *initial typing* of the
/// tuple, that simply preserves its synctactic structure and types. During
/// initialization, this will be rewritten an element initializer.
///
/// \todo Support using tuple expressions as std::tie expressions. For example:
///
///   var int x;
///   var bool b;
///   {x, b} = f(); // assigns to x, b
///
/// the tuple expression has object type, but the elements all have reference
/// type. If the value of f() is {int, bool}, then the result should be to
/// assign values pairwise. Note that this is one place where tuples are not
/// used to initialize objects.
expr*
translator::on_tuple_expression(expr_seq&& es)
{
  /// Compute the type sequence for the initializer.
  type_seq types(es.size());
  std::transform(es.begin(), es.end(), types.begin(), [this](expr* e) {
    return e->get_type();
  });

  type* t = cxt->get_tuple_type(std::move(types));
  expr* e = new lang_array::tuple_expr(t, std::move(es));
  return e;
}

/// \todo We need a special evaluation mode for constant expressions. That
/// would need to limit the set of declarations that could be referred to
/// from the expression.
expr*
translator::on_constant_expression(expr* e)
{
  return require_constant_expression(e);
}

/// Returns a constant expression for `e`. The program is ill-formed if `e`
/// is not a constant expression.
expr*
translator::require_constant_expression(expr* e)
{
  value v;
  try {
    evaluator eval(*cxt);
    v = evaluate(eval, e);
    return new lang_array::const_expr(e->get_type(), e, v);
  }
  catch (std::runtime_error& err) {
    // FIXME: The error is: "e is not a constant expression". Use the caught
    // exception as a note to explain why.
    throw std::runtime_error("not a constant expression");
  }
}

/// Try evaluating `e` as a constant expression. If `e` cannot be evaluated,
/// returns `e` unmodified. Otherwise, returns a constant expression for `e`.
expr*
translator::try_constant_expression(expr* e)
{
  value v;
  try {
    evaluator eval(*cxt);
    v = evaluate(eval, e);
    return new lang_array::const_expr(e->get_type(), e, v);
  }
  catch (std::runtime_error&) {
    return e;
  }
}

} // namespace beaker
