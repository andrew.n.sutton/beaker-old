#include "translator.hpp"

#include <beaker/ast/print.hpp>
#include <beaker/ast/lang.void/exprs_void.hpp>
#include <beaker/ast/lang.bool/exprs_bool.hpp>
#include <beaker/ast/lang.store/exprs_store.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>
#include <beaker/ast/lang.fn/stmts_fn.hpp>
#include <beaker/ast/lang.io/stmts_io.hpp>

#include <iostream>


namespace beaker {

/// Create a new block statement.
stmt*
translator::on_block_statement(stmt_seq&& s)
{
  return new lang_fn::block_stmt(std::move(s));
}

stmt*
translator::on_if_statement(expr* e, stmt* s1, stmt* s2)
{
  e = boolean_conversion(e);
  return new lang_fn::if_stmt(e, s1, s2);
}

stmt*
translator::on_while_initiation(expr* e)
{
  e = boolean_conversion(e);
  auto* loop = new lang_fn::while_stmt(e, nullptr);
  push_control(loop);
  return loop;
}

stmt*
translator::on_while_completion(stmt* l, stmt* s)
{
  assert(current_control() == l && "control stack imbalance");
  auto* loop = static_cast<lang_fn::while_stmt*>(l);
  loop->body = s;
  pop_control();
  return loop;
}

stmt*
translator::on_break_statement()
{
  if (!inside_loop())
    throw std::runtime_error("cannot break in this context");
  return new lang_fn::break_stmt();
}

stmt*
translator::on_continue_statement()
{
  if (!inside_loop())
    throw std::runtime_error("cannot continue in this context");
  return new lang_fn::cont_stmt();
}


/// Create a new return statement that trivially initializes the return
/// value of the expression `e`.
stmt*
translator::on_return_statement()
{
  type* t = cxt->get_void_type();
  expr* e = new lang_store::nop_init(t);
  return new lang_fn::ret_stmt(e);
}

/// Create a new return statement that copy-initializes the return value
/// with the expression `e`.
stmt*
translator::on_return_statement(expr* e)
{
  auto* fn = current_function();
  expr* c = copy_initialize(fn->get_return_type(), e);
  return new lang_fn::ret_stmt(c);
}

/// Creates a new assert statement. This creates an expression statement
/// around an assert expression.
stmt*
translator::on_assert_statement(expr* e)
{
  type* t = cxt->get_bool_type();
  return on_expression_statement(new lang_bool::assert_expr(t, e));
}

/// \todo What if `e` is a function?
stmt*
translator::on_print_statement(expr* e)
{
  e = standard_conversion(e, get_object_type(e->get_type()));
  return new lang_io::print_stmt(e);
}

/// \todo What if `e` is a function reference?
stmt*
translator::on_scan_statement(expr* e)
{
  if (!is_reference(e->get_type()))
    throw std::runtime_error("operand is not a reference");
  return new lang_io::scan_stmt(e);
}

/// Returns a new expression statement with a no-op.
stmt*
translator::on_skip_statement()
{
  type* t = cxt->get_void_type();
  return on_expression_statement(new lang_void::nop_expr(t));
}

/// Returns a new expression statement with a trap.
stmt*
translator::on_trap_statement()
{
  type* t = cxt->get_void_type();
  return on_expression_statement(new lang_void::trap_expr(t));
}

/// Create a new declaration statement.
stmt*
translator::on_declaration_statement(decl* d)
{
  return new lang_fn::decl_stmt(d);
}

/// Create a new expression statement.
stmt*
translator::on_expression_statement(expr* e)
{
  return new lang_fn::expr_stmt(e);
}


} // namespace beaker
