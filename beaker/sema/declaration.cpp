#include "translator.hpp"

#include <beaker/ast/context.hpp>
#include <beaker/ast/lang.tu/decls_tu.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>
#include <beaker/ast/print.hpp>

#include <iostream>


namespace beaker {

/// Returns true if d should be added to its parent context. While this is
/// generally true, certain declarations are created before their parent 
/// context is available (e.g., parameters and return values).
static inline bool
should_add_to_parent(decl* d)
{
  return !is_parameter(d);
}

/// \todo If binding fails, delete `d`? 
void
translator::declare(scope* s, named_decl* d)
{
  struct fn {
    void operator()(decl*) { 
      assert(false && "invalid declaration"); 
    }
    void operator()(lang_tu::tu_decl* tu) {
      // FIXME: We should really be adding the declaration to a lookup table.
      tu->decls.push_back(d); 
    }
    void operator()(lang_fn::fn_decl* d) {
      // FIXME: functions should act as a declaration context, right?
    }

    decl* d;
  };

  bind(s, d);
  if (should_add_to_parent(d))
    apply(current_context(), fn{d});
}

void
translator::declare(named_decl* d)
{
  declare(current_scope(), d);
}

void
translator::bind(scope *s, named_decl* d)
{
  if (auto* ent = env.lookup(d->get_name())) {
    // There is already an entry for this name. Note that we cannot declare 
    // multiple names in the same scope.
    if (ent->get_scope() == current_scope()) {
      // FIXME: Handle redeclaration (soon) and overloading (later).
      throw std::runtime_error("redeclaration error");
    }
  }

  // There are no conflicting declarations.
  env.add(s, d);
}

void
translator::bind(named_decl* d)
{
  bind(current_scope(), d);
}

} // namespace beaker
