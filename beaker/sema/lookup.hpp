#pragma once

#include <beaker/common/diagnostics.hpp>
#include <beaker/common/symbols.hpp>


namespace beaker {

/// Represents a failure to match a name to a declaration.
struct unresolved_id_error : translation_error
{
  unresolved_id_error(const symbol* s) : sym(s) { }

  std::string format() const override;

  const symbol* sym;
};

} // namespace beaker
