#include "translator.hpp"

#include <beaker/lex/tokens.hpp>
#include <beaker/ast/context.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>
#include <beaker/ast/lang.fn/stmts_fn.hpp>


namespace beaker {

translator::translator(context& c)
  : cxt(&c), curr(nullptr)
{ }

symbol* 
translator::on_identifier(token* tok)
{
  id_token* id = dynamic_cast<id_token*>(tok);
  return id->sym;
}

/// \todo Are we considered to be translating a function if translating
/// a local scoped declaration. For example:
///
///   def f() -> void {
///     struct S {
///       // <- current_function() == nullptr?
///   }
///
/// We could have a separate function that returned the innermost enclosing
/// function.
lang_fn::fn_decl*
translator::current_function() const
{
  return dynamic_cast<lang_fn::fn_decl*>(curr);
}

stmt*
translator::current_control() const
{
  assert(current_function() && "not inside a function");
  if (ctrl.empty())
    return nullptr;
  else
    return ctrl.back();
}

bool
translator::inside_loop() const
{
  if (stmt* s = current_control()) {
    if (dynamic_cast<lang_fn::while_stmt*>(s))
      return true;
  }
  return false;
}

} // namespace beaker
