#pragma once

#include <beaker/common/diagnostics.hpp>


namespace beaker {

struct type;
struct expr;


/// Represents a failure to convert an expression to a type.
struct standard_conversion_error : translation_error
{
  standard_conversion_error(const expr* e, const type* t) : src(e), dst(t) { }

  std::string format() const override;

  const expr* src;
  const type* dst;
};

/// Represents a failure to find a conversion from a source expression
/// into a function type.
struct function_conversion_error : translation_error
{
  function_conversion_error(const expr *e) : src(e) { }

  std::string format() const override;

  const expr* src;
};

} // namespace beaker
