#pragma once

#include "scope.hpp"

#include <beaker/ast/context.hpp>
#include <beaker/common/symbols.hpp>

#include <stack>
#include <vector>


namespace beaker {

struct token;
struct context;
struct type;
struct expr;
struct decl;
struct module;
struct stmt;
namespace lang_fn { struct fn_decl; }


/// A pair of expressions.
using expr_pair = std::pair<expr*, expr*>;


/// A converted pair is a pair of converted expressions, which have the
/// same type.
struct conv_pair : expr_pair
{
  using expr_pair::expr_pair;

  /// Returns the (common) type of the converted expressions.
  const type* get_type() const;
  type* get_type();
};


/// Represents a translation from concrete to abstract syntax. This class
/// defines and implements the semantic actions of the parser.
///
/// The translator inherits the facilities of a lexical scoping environment.
/// This provides all of the features necessary to support declarations and
/// name lookup.
///
/// \todo Consider making this an abstract class to allow for multiple
/// translation schemes.
struct translator
{
  translator(context&);

  context& get_context() { return *cxt; }

  decl* on_start_translation();
  decl* on_finish_translation(decl*);

  decl* on_function_declaration(symbol*, decl_seq&&, type*);
  decl* on_function_definition(decl*);
  decl* on_function_definition(decl*, stmt*);
  decl* on_function_parameter(token* tok, type* t, symbol* n);
  decl* on_function_parameter(token* tok, type* t);

  decl* on_constant_declaration(type*, symbol*);
  decl* on_constant_initialization(decl*, expr*);
  decl* on_constant_initialization(decl* d, expr_seq&& es);

  decl* on_variable_declaration(type* t, symbol* n);
  decl* on_variable_initialization(decl* d);
  decl* on_variable_initialization(decl* d, expr* e);
  decl* on_variable_initialization(decl* d, expr_seq&& es);

  stmt* on_block_statement(stmt_seq&& ss);
  stmt* on_if_statement(expr* e, stmt* s1, stmt* s2);
  stmt* on_while_initiation(expr* e);
  stmt* on_while_completion(stmt* l, stmt* s);
  stmt* on_break_statement();
  stmt* on_continue_statement();
  stmt* on_return_statement();
  stmt* on_return_statement(expr* e);
  stmt* on_assert_statement(expr* e);
  stmt* on_print_statement(expr* e);
  stmt* on_scan_statement(expr* e);
  stmt* on_skip_statement();
  stmt* on_trap_statement();
  stmt* on_declaration_statement(decl* d);
  stmt* on_expression_statement(expr* e);

  type* on_void_type(token*);
  type* on_bool_type(token*);
  type* on_int_type(token*);
  type* on_nat_type(token*);
  type* on_reference_type(type* t);
  type* on_function_type(type* t);
  type* on_function_type(type_seq&& ts, type* t);
  type* on_array_type(type* t);
  type* on_array_type(type* t, expr_seq&& es);
  expr* on_array_bound();
  expr* on_array_bound(expr* e);

  type* on_tuple_type();
  type* on_tuple_type(type_seq&& ts);

  expr* on_assign_expression(expr* e1, expr* e2);
  expr* on_cond_expression(expr* e1, expr* e2, expr* e3);
  expr* on_logical_and_expression(expr* e1, expr* e2);
  expr* on_logical_or_expression(expr* e1, expr* e2);
  expr* on_or_expression(expr* e1, expr* e2);
  expr* on_xor_expression(expr* e1, expr* e2);
  expr* on_and_expression(expr* e1, expr* e2);
  expr* on_lt_expression(expr* e1, expr* e2);
  expr* on_gt_expression(expr* e1, expr* e2);
  expr* on_le_expression(expr* e1, expr* e2);
  expr* on_ge_expression(expr* e1, expr* e2);
  expr* on_eq_expression(expr* e1, expr* e2);
  expr* on_ne_expression(expr* e1, expr* e2);
  expr* on_add_expression(expr* e1, expr* e2);
  expr* on_sub_expression(expr* e1, expr* e2);
  expr* on_mul_expression(expr* e1, expr* e2);
  expr* on_div_expression(expr* e1, expr* e2);
  expr* on_rem_expression(expr* e1, expr* e2);
  expr* on_neg_expression(expr* e1);
  expr* on_not_expression(expr* e1);
  expr *on_call_expression(expr* e1, expr_seq&& es);
  expr *on_index_expression(expr* e1, expr_seq&& es);
  expr *on_array_index_expression(lang_array::array_type*, expr* e1, expr_seq&& es);
  expr *on_tuple_index_expression(lang_array::tuple_type*, expr* e1, expr* e2);
  expr* on_constant_expression(expr* e);

  expr* on_true_literal(token*);
  expr* on_false_literal(token*);
  expr* on_integer_literal(token*);
  expr* on_character_literal(token*);
  expr* on_string_literal(token*);
  
  expr* on_id_expression(symbol*);
  expr* on_id_expression(decl*);

  expr* on_tuple_expression(expr_seq&& es);

  symbol* on_identifier(token*);

  // Scope

  /// Enters the scope `k`.
  void enter_scope(int sk) { env.enter_scope(sk); }

  /// Leaves the current scope.
  void leave_scope() { env.leave_scope(); }

  /// Returns the current scope.
  const scope* current_scope() const { return env.current_scope(); }
  scope* current_scope() { return env.current_scope(); }

  // Declaration context

  /// Returns the current declaration context.
  decl* current_context() const { return curr; }

  /// Sets the current declaration context.
  void set_current_context(decl* d) { curr = d; }

  /// Returns the current function. This value is nullptr when not translating
  /// a function.
  lang_fn::fn_decl* current_function() const;

  // Declarations

  /// Add a name binding in `s` for the declaration `d`.
  void bind(scope* s, named_decl* d);

  /// Add name binding for `d` in the current scope.
  void bind(named_decl*);
  
  /// Add a name binding in `s` for `d` and register it with the current 
  /// declaration context.
  void declare(scope*, named_decl*);

  /// Add a name binding for `d` in the current scope and register it with the 
  /// current declaration context.
  void declare(named_decl*);

  // Lookup

  /// Returns a set of declarations found by unqualified name lookup.
  decl_seq unqualified_lookup(const symbol*);

  // Constant expressions

  expr* require_constant_expression(expr* e);
  expr* try_constant_expression(expr* e);

  // Conversions

  expr* reference_conversion(expr* e);

  /// Attempt to convert `e` to `t`.
  expr* standard_conversion(expr* e, type* t);

  /// Convert `e` to a function type.
  expr* function_conversion(expr* e);

  /// Attempt to convert `e` to type `bool`.
  expr* boolean_conversion(expr* e);

  /// Attempt to convert both `e1` and `e2` to type `bool`.
  conv_pair boolean_conversion(expr* e1, expr* e2);

  /// Attempt to convert `e` to an arithmetic type.
  expr* arithmetic_conversion(expr* e);
  
  /// Attempt to convert `e1` and `e2` to a common arithmetic type.
  conv_pair arithmetic_conversion(expr* e1, expr* e2);

  /// Attempt to convert `e` to an integral type.
  expr* integral_conversion(expr* e);

  /// Attempt to convert `e1` and `e2` to a common type.
  conv_pair common_conversion(expr* e1, expr* e2);

  // Initialization

  /// Returns the current initialization or nullptr if none.
  type* current_initialization() { return inits.empty() ? nullptr : inits.back(); }

  /// Indicates that we are initializing an object of type `t`.
  void start_initialization(type* t) { inits.push_back(t); }
  void start_initialization(decl* d) { inits.push_back(d->get_type()); }

  /// Indicates that we are finished initializing an object of type `t`.
  void finish_initialization() { inits.pop_back(); }

  expr* trivially_initialize(type* t);
  expr* zero_initialize(type* t);
  expr* default_initialize(type* t);  
  
  expr* copy_initialize(type* t, expr* e);
  expr* reference_initialize(type* t, expr* e);
  
  expr* aggregate_initialize(type* t, expr_seq& e);
  expr* array_initialize(type* t, expr_seq& e);
  expr* tuple_initialize(type* t, expr_seq& e);

  // Flow control

  /// Activate the statement s to enable or disable certain syntactic 
  /// constructs. For example, pushing a while statement enables break
  /// and continue.
  void push_control(stmt* s) { ctrl.push_back(s); }

  /// Pop the innermost control structure.
  void pop_control() { ctrl.pop_back(); }

  /// Returns the innermost control structure, or null if the current control
  /// is invalid.
  stmt* current_control() const;

  /// Returns true if innermost control structure is a loop.
  bool inside_loop() const;

private:
  context* cxt; // Language context
  decl* curr; // Current semantic declaration context

  stmt_seq ctrl; // The control stack.
  type_seq inits; // The initialization context.

  /// Stores name bindings
  environment env;
};


} // namespace beaker
