#pragma once

#include <beaker/common/symbols.hpp>
#include <beaker/ast/decls.hpp>

#include <cassert>
#include <memory>
#include <unordered_map>
#include <vector>


namespace beaker {

/// Kinds of scopes.
enum {
  module_scope,
  function_parameter_scope,
  function_scope,
  block_scope,
};

// -------------------------------------------------------------------------- //
// Scope

/// Represents the sequence declared names within a region of text. A language
/// can derive from this class to provide specialized behaviors related to
/// the declaration and lookup. The kind member is used as a discriminator,
/// allowing those languages to determine properties of the scope.
struct scope : decl_seq
{
  scope(int k) : scope(k, nullptr) { }
  scope(int k, scope* p) : kind(k), parent(p) { }

  virtual ~scope() = default;

  /// Returns the kind of scope.
  int get_kind() const { return kind; }
  
  /// Returns a string describing the scope kind.
  const char* get_scope_name() const;

  /// Returns the parent scope. The parent scope is null if this is the top
  /// scope.
  const scope* get_parent() const { return parent; }
  scope* get_parent() { return parent; }

  /// Adds the declaration `d` to the current scope.
  void add(named_decl* d) { push_back(d); } 

  int kind;
  scope* parent;
};


// -------------------------------------------------------------------------- //
// Environment

/// The (lexical) environment provides a complete mapping of every name to
/// its associated declarations, including declarations that shadow others.
/// For example:
///
///     var int x = 0; // #1
///     {
///       var int x = 1; // shadows #1
///       print(x); // prints 1
///     }
///     print(x); // prints 0
///
///
/// Note that the declaration may in fact be a declaration set or an overload
/// set. The semantics of these types must handled by the source language.
struct environment
{
  /// Represents a declaration within a particular scope.
  struct entry
  {
    /// Returns the scope in which the entry was added.
    scope* get_scope() { return s; }

    /// Returns the bound declaration.
    named_decl* get_declaration() { return d; }
    
    scope* s;
    named_decl* d;
  };

  /// The list of declarations bound to a given name.
  struct bindings : std::vector<entry>
  {
    /// Returns the innermost binding for a name.
    const entry& top() const { return back(); }
    entry& top() { return back(); }

    /// Insert a new innermost binding for a name.
    void push(scope* s, named_decl* d) { push_back({s, d}); }

    /// Removes the innermost binding for a name.
    void pop() { pop_back(); }
  };

  /// The stack of scopes, which maintain the list of declarations in each
  /// context.
  struct stack_type : std::vector<std::unique_ptr<scope>>
  {
    ~stack_type() { assert(empty() && "imbalanced scope stack"); }

    /// Returns the current scope.
    const scope* top() const { return back().get(); }
    scope* top() { return back().get(); }

    /// Push a new scope onto the scope stack such that all subsequent 
    /// declarations will be added in that scope.
    void push(int k);

    /// Remove the top scope from the scope stack along with the bindings
    /// for names declared in that scope.
    void pop();
  };

  using map_type = std::unordered_map<symbol*, bindings>;

  /// Adds a named declaration d in the given scope. This declaration shall
  /// not replace another declaration in the same scope.
  void add(named_decl* d) { add(current_scope(), d); }

  /// Adds a new innermost binding to the named declaration `d`.
  void add(scope*, named_decl*);
  
  /// Removes the innermost binding for n.
  void remove(const symbol* n) { get_bindings(n)->pop(); }

  /// Removes the innermost binding for d's name. This does not remove the
  /// name from the scope.
  void remove(const named_decl* d) { remove(d->get_name()); }

  /// Returns the list of bindings for a name or nullptr if a) the name has 
  /// not been seen, or b) if there are no bindings for it.
  const bindings* get_bindings(const symbol*) const;
  bindings* get_bindings(const symbol*);

  /// Returns the innermost binding for the given name or nullptr if a) the
  /// name has not been seen, or b) if there are no bindings for it.
  const entry* lookup(const symbol*) const;
  entry* lookup(const symbol*);

  /// Enter a new scope (with kind `k`) such that all new bindings will be
  /// added in that scope.
  void enter_scope(int k);

  /// Removes the current scope and all name bindings for declarations in
  /// that scope.
  void leave_scope();
  
  /// Returns the current scope.
  const scope* current_scope() const;
  scope* current_scope();

  map_type map;
  stack_type stack;
};

/// Push a new empty scope stack.
inline void
environment::stack_type::push(int k)
{
  scope* s = empty() ? nullptr : back().get();
  push_back(std::make_unique<scope>(k, s));
}

/// Pop the current scope from the stack and remove all of the bindings
/// in that contour.
inline void
environment::stack_type::pop()
{
  assert(!empty() && "unbalanced scope stack");
  pop_back();
}

inline const scope*
environment::current_scope() const 
{ 
  assert(!stack.empty());
  return stack.top(); 
}

inline scope* 
environment::current_scope() 
{ 
  assert(!stack.empty());
  return stack.top(); 
}


void print_scope(std::ostream&, const scope*);

} // namespace beaker
