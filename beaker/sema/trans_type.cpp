#include "translator.hpp"

#include <beaker/lex/tokens.hpp>

#include <beaker/ast/types.hpp>
#include <beaker/ast/exprs.hpp>
#include <beaker/ast/decls.hpp>
#include <beaker/ast/stmts.hpp>
#include <beaker/ast/context.hpp>
#include <beaker/ast/print.hpp>
#include <beaker/ast/lang.void/exprs_void.hpp>
#include <beaker/ast/lang.array/exprs_array.hpp>

#include <iostream>


namespace beaker {

/// Returns the `void` type.
type*
translator::on_void_type(token*)
{
  return cxt->get_void_type();
}

/// Returns the `bool` type.
type*
translator::on_bool_type(token*)
{
  return cxt->get_bool_type();
}

/// Returns the `int` type.
type*
translator::on_int_type(token*)
{
  return cxt->get_int_type();
}

/// Returns the `nat` type.
type*
translator::on_nat_type(token*)
{
  return cxt->get_nat_type();
}

/// Returns the type `t&`.
type*
translator::on_reference_type(type* t)
{
  if (is_reference(t))
    throw std::runtime_error("reference to a reference");
  if (is_function(t))
    throw std::runtime_error("reference to a function");

  return cxt->get_reference_type(t);
}

/// Returns the function type `()->t`.
type*
translator::on_function_type(type* t) 
{
  type_seq ts;
  return cxt->get_function_type(ts, t);
}

/// Returns the function type `(ts)->t`.
type*
translator::on_function_type(type_seq&& ts, type* t) 
{
  return cxt->get_function_type(std::move(ts), t);
}

/// Returns the type `t[]`.
type*
translator::on_array_type(type* t) 
{
  return cxt->get_buffer_type(t);
}

/// Returns the array type `t[n1, n2, ..., nk]`. Each expression is evaluated 
/// to determine the extent at it its corresponding rank.
///
/// 
type*
translator::on_array_type(type* t, expr_seq&& es) 
{
  bool buf = std::any_of(es.begin(), es.end(), [](const expr* e) {
    // FIXME: Test for a placeholder, not void.
    return is_void(e->get_type());
  });

  if (buf)
    // FIXME: This needs to be a multidimensional buffer.
    return cxt->get_buffer_type(t);
  else
    return cxt->get_array_type(t, std::move(es));
}

/// An empty array bound is represented by a constant nop expression. We
/// wrap the nop expr as a constant expression so that it has an associated
/// void value.
///
/// \todo Generate a placeholder expression -- which we don't support.
expr* 
translator::on_array_bound()
{
  expr* e = new lang_void::nop_expr(cxt->get_void_type());

  // The array bound is a constant expression.
  return require_constant_expression(e);
}

/// An array bound is a constant integer expression.
expr* 
translator::on_array_bound(expr* e)
{
  // The array bound is a converted integer expression.
  e = integral_conversion(e);

  // The array bound is a constant expression.
  e = require_constant_expression(e);
  
  // FIXME: Verify that e won't generate an array too large?
  return e;
}

type*
translator::on_tuple_type()
{
  return cxt->get_tuple_type();
}

/// Returns a tuple type `{t1, t2, ..., tn}`. Each element type shall be an
/// object type.
type*
translator::on_tuple_type(type_seq&& ts)
{
  for (type* t : ts) {
    if (!is_object(t))
      throw std::runtime_error("element type is not an object");
  }
  return cxt->get_tuple_type(std::move(ts));
}


} // namespace beaker
