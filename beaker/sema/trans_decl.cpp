#include "translator.hpp"

#include <beaker/lex/tokens.hpp>
#include <beaker/ast/print.hpp>
#include <beaker/ast/lang.fn/decls_fn.hpp>

#include <iostream>


namespace beaker {

/// Returns the pre-built translation unit.
decl*
translator::on_start_translation()
{
  return cxt->get_tu();
}

/// Finalize the translation unit.
///
/// \todo Perform all deferred processing (we don't have any yet).
decl*
translator::on_finish_translation(decl* d)
{
  return d;
}

/// Create and declare a new function.
decl*
translator::on_function_declaration(symbol* n, decl_seq&& ps, type* t)
{
  scope* parent = current_scope()->get_parent();
  decl* context = current_context();

  // Determine the return value. Scalar returns bind directly to their value,
  // while aggregates require storage (i.e., an output parameter).
  decl* ret;
  if (is_scalar(t) || is_void(t))
    ret = new lang_fn::ret_parm(uid(), dc(), nullptr, t);
  else
    ret = new lang_fn::out_parm(uid(), dc(), nullptr, t);

  // Create the function.
  type* ty = cxt->get_function_type(ps, ret);
  auto* fn = new lang_fn::fn_decl(uid(), context, n, ty, std::move(ps), ret);
  
  // Declare the function in the parent scope.
  declare(parent, fn);

  return fn;
}

/// Finish the function declaration.
decl*
translator::on_function_definition(decl* d)
{
  return d;
}

/// Attach the body to the definition and perform initial checks and lowering
/// (in a separate thread?). 
decl*
translator::on_function_definition(decl* d, stmt* s)
{
  lang_fn::fn_decl* fn = static_cast<lang_fn::fn_decl*>(d);
  fn->body = s;
  return fn;
}

/// Create a new function parameter.
///
/// \todo Allow in parameters with reference type.
decl*
translator::on_function_parameter(token* tok, type* t, symbol* n)
{
  if (is_void(t))
    throw std::runtime_error("parameter of type 'void'");
  if (is_reference(t) && (!tok || tok->get_kind() == in_kw))
    throw std::runtime_error("in parameter reference");

  decl* parm;
  if (!tok || tok->get_kind() == in_kw)
    parm = new lang_fn::in_parm(uid(), dc(), n, t);
  else if (tok->get_kind() == out_kw)
    parm = new lang_fn::out_parm(uid(), dc(), n, t);
  else if (tok->get_kind() == var_kw)
    parm = new lang_fn::var_parm(uid(), dc(), n, t);
  else
    throw std::logic_error("invalid parameter declaration");

  declare(static_cast<named_decl*>(parm));
  return parm;
}


/// Create a new unnamed function parameter.
///
/// \todo Supply an empty name.
decl*
translator::on_function_parameter(token* tok, type* t)
{
  return on_function_parameter(tok, t, nullptr);
}

/// Declares a constant.
///
/// \todo If we're in module or namespace scope, then this would declare a
/// global variable.
///
/// \todo Allow constant references. We should be able to write things like:
///
///     var int x;
///     const int& r = x;
///     r = 3; // error: assignment to constant object.
///
/// We'll need to add const-qualifiers to the type system in order design this.
decl*
translator::on_constant_declaration(type* t, symbol* n)
{
  if (is_void(t))
    throw std::runtime_error("constant of type 'void'");
  if (is_reference(t))
    throw std::runtime_error("constant reference");
  
  // FIXME: Lets can be global.
  auto* owner = current_function();
  
  // Create the variable.
  auto* var = new lang_fn::const_decl(uid(), owner, n, t);

  // Declare the variable.
  declare(var);

  return var;
}

decl*
translator::on_constant_initialization(decl* d, expr* e)
{
  auto* var = static_cast<lang_fn::const_decl*>(d);
  var->init = copy_initialize(var->get_type(), e);
  return var;
}

decl*
translator::on_constant_initialization(decl* d, expr_seq&& es)
{
  // FIXME: There is no object to brace initialize. We need some way of
  // indicating that we're going to a) materialize a temporary and b)
  // apply the brace initializer to that object. Maybe that's just implicit
  // in the eval/cg components.
  throw std::logic_error("brace initialization of a value");

  // auto* var = static_cast<lang_fn::const_decl*>(d);
  // return var;
}


/// Declares a variable.
///
/// \todo If we're in module or namespace scope, then this would declare a
/// global variable.
decl*
translator::on_variable_declaration(type* t, symbol* n)
{
  if (is_void(t))
    throw std::runtime_error("variable of type 'void'");
  
  // FIXME: Variables can be global.
  auto* owner = current_function();
  
  // Create the variable.
  auto* var = new lang_fn::var_decl(uid(), owner, n, t);

  // Declare the variable.
  declare(var);

  return var;
}

/// Default initialize the variable.
decl*
translator::on_variable_initialization(decl* d) 
{
  auto* var = static_cast<lang_fn::var_decl*>(d);
  var->init = default_initialize(var->get_type());
  return var;
}

/// Copy initialize the variable.
decl*
translator::on_variable_initialization(decl* d, expr* e)
{
  auto* var = static_cast<lang_fn::var_decl*>(d);
  var->init = copy_initialize(var->get_type(), e);
  return var;
}

/// \todo Should we be able to brace-initialize a non-aggregate? Yes. Writing
/// this:
///
///   var int x {0};
///
/// should be equivalent to:
///
///   var int x = 0;
///
/// To make this work, we'll need a projection-like expression that simply
/// unwraps a subexpression from a tuple.
decl*
translator::on_variable_initialization(decl* d, expr_seq&& es)
{
  auto* var = static_cast<lang_fn::var_decl*>(d);
  var->init = aggregate_initialize(var->get_type(), es);
  return var;
}

} // namespace beaker
