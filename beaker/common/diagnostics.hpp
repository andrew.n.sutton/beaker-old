#pragma once

#include <stdexcept>
#include <string>


namespace beaker
{


/// The base class of all diagnostic messages.
///
/// This derives from runtime error, which allows its message to be interpreted
/// as a C-string.
struct translation_error : std::runtime_error
{
  translation_error() : std::runtime_error("") { }

  virtual ~translation_error() = default;

  /// Returns a C-string describing the error message.
  const char* what() const noexcept override;

  /// Formats the error into a printable message.
  virtual std::string format() const = 0;

  /// A text buffer used by the diagnostic to render its messaage. This
  /// is only used when called by the format function.
  mutable std::string buf;
};


inline const char*
translation_error::what() const noexcept
{
  if (buf.empty())
    buf = format();
  return buf.c_str();
}

};
