#include <beaker/lex/lexer.hpp>
#include <beaker/syn/parser.hpp>
#include <beaker/ast/context.hpp>
#include <beaker/ast/print.hpp>

#include <beaker/codegen/llvm/generate.hpp>
// #include <beaker/codegen/jvm/generate.hpp>

#include <fstream>
#include <iterator>
#include <iostream>


using namespace beaker;

/// Stores the configuration of the environment.
struct options
{
  enum vm_type { llvm, jvm };

  vm_type vm = llvm;
};


int 
main(int argc, char* argv[])
{
  /// Defines the language's keywords.
  keyword_table keywords;

  /// Stores the language's symbols.
  symbol_table symbols;

  // Stores language facilities.
  context cxt(symbols);

  // Language and compiler options.
  options opts;

  for (int i = 1; i < argc; ++i) {
    std::string arg = argv[i];
    if (arg[0] == '-') {
      if (arg == "-jvm")
        opts.vm = options::jvm;
      else if (arg == "-llvm")
        opts.vm = options::llvm;
    }
  }

  if (argc < 2) {
    std::cerr << "error: no input files given\n";
    return 1;
  }
  
  // Read the input into a string.
  std::ifstream ifs(argv[1]);
  std::string input{std::istreambuf_iterator<char>(ifs),
                    std::istreambuf_iterator<char>()};

  parser p(keywords, symbols, cxt, input);
  decl* tu = p.translation();

  // std::cout << verbose(tu);

  if (opts.vm == options::llvm) {
    // beaker::llvm::generator gen(cxt);
    // generate(gen, tu);
  }
  else {
    std::cerr << "error: jvm not supported\n";
    return 1;
  }

  return 0;
}
